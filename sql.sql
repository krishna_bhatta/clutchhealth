ALTER TABLE  `accepted_payments` ADD  `country` VARCHAR( 250 ) NOT NULL AFTER  `external_account` ,
ADD  `currency` VARCHAR( 250 ) NOT NULL AFTER  `country` ,
ADD  `routing_number` VARCHAR( 250 ) NOT NULL AFTER  `currency` ,
ADD  `account_number` VARCHAR( 250 ) NOT NULL AFTER  `routing_number` ,
ADD  `account_holder_name` VARCHAR( 250 ) NOT NULL AFTER  `account_number` ,
ADD  `account_holder_type` ENUM(  'individual',  'company' ) NOT NULL DEFAULT  'individual' AFTER  `account_holder_name` ;
