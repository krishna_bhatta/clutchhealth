<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="apple-touch-icon-precomposed" sizes="57x57" href="apple-touch-icon-57x57.png" />
      <link rel="apple-touch-icon-precomposed" sizes="114x114" href="apple-touch-icon-114x114.png" />
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="apple-touch-icon-72x72.png" />
      <link rel="apple-touch-icon-precomposed" sizes="144x144" href="apple-touch-icon-144x144.png" />
      <link rel="apple-touch-icon-precomposed" sizes="60x60" href="apple-touch-icon-60x60.png" />
      <link rel="apple-touch-icon-precomposed" sizes="120x120" href="apple-touch-icon-120x120.png" />
      <link rel="apple-touch-icon-precomposed" sizes="76x76" href="apple-touch-icon-76x76.png" />
      <link rel="apple-touch-icon-precomposed" sizes="152x152" href="apple-touch-icon-152x152.png" />
      <link rel="icon" type="image/png" href="favicon-196x196.png" sizes="196x196" />
      <link rel="icon" type="image/png" href="favicon-96x96.png" sizes="96x96" />
      <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
      <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
      <link rel="icon" type="image/png" href="favicon-128.png" sizes="128x128" />
      <meta name="application-name" content="&nbsp;"/>
      <meta name="msapplication-TileColor" content="#FFFFFF" />
      <meta name="msapplication-TileImage" content="mstile-144x144.png" />
      <meta name="msapplication-square70x70logo" content="mstile-70x70.png" />
      <meta name="msapplication-square150x150logo" content="mstile-150x150.png" />
      <meta name="msapplication-wide310x150logo" content="mstile-310x150.png" />
      <meta name="msapplication-square310x310logo" content="mstile-310x310.png" />
      <meta name="theme-color" content="#be1e2d">
      <title>Clutch Health - @yield('htmlheader_title', '') </title>
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
      <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700,500' rel='stylesheet' type='text/css'>
      <!-- Bootstrap core CSS -->
      <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet" type="text/css" />
      <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
      <link href="{{ asset('css/ie10-viewport-bug-workaround.css') }}" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="{{ asset('css/frontend-main.css') }}" rel="stylesheet">
      <!-- Responsive styles for this template -->
      <link href="{{ asset('css/frontend-responsive.css') }}" rel="stylesheet">
      <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
      <!--[if lt IE 9]><script src="{{ asset('js/ie8-responsive-file-warning.js') }}"></script><![endif]-->
      <script src="{{ asset('js/ie-emulation-modes-warning.js') }}"></script>
    <!-- iCheck -->
    <link href="{{ asset('/plugins/iCheck/square/red.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/plugins/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet" type="text/css" />
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <style type="text/css">
      body {
         padding-bottom: 230px
      }
      @media only screen and (max-width : 768px) {
         .loginbox {
            float: right !important;
         }
         .navbar-header {
            float: left;
         }
      }
      @media only screen and (max-width : 480px) {
         #navbar, .loginbox, .navbar-header {
            float: none !important;
         }
         .navbar-header {
            max-width: 255px;
            margin: 0 auto !important;
         }
         .loginbox {
            max-width: 160px;
            margin: 0 auto 10px !important;
         }
      }
      .navbar-brand img {
         height: 60px;
      }
      .terms {
         font-size: 18px;
      }
      .terms a:hover {
         text-decoration: underline;
         color: #be1e2d;
      }
      .has-error .form-control, .has-error .form-control:focus {
         box-shadow: none;
      }
      </style>
      @yield('css')
   </head>
   <body>
      <div class="body-wrapper">
         <!-- Static navbar -->
         <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
               <div class="navbar-header">
                  <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ asset('img/logo.png') }}" class="img-responsive" ></a>
               </div>
               <div id="navbar">
                  <div class="loginbox pull-right">
                  @yield('rhs-nav')
                  </div>
               </div>
            </div>
         </nav>
         <div id="auth-div">
            @yield('content', 'Content here')
         </div>
         <footer class="footer">
            <div class="container">
               <nav class="footer-navbar">
                  <ul>
                     <li><a href="{{ url('/') }}">Home</a></li>
                     <!-- <li><a href="{{ url('faq') }}">FAQ</a></li> -->
                     <li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
                     <li><a href="{{ url('terms-and-conditions') }}">Terms & Conditions</a></li>
                  </ul>
               </nav>
               <span class="copyright-txt">&copy; 2015-{{ date('Y') }} ClutchHealth. <span class="all-rights">All rights reserved.</span></span>
               <div class="footer-social">
                  <ul>
                     <li><a href="https://www.facebook.com/ClutchHealth" class="fb" target="_blank"></a></li>
                     <li><a href="https://www.twitter.com/ClutchHealth" class="tw" target="_blank"></a></li>
                  </ul>
               </div>
            </div>
         </footer>
      </div>
      <!-- Bootstrap core JavaScript
         ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script>window.jQuery || document.write('<script src="/plugins/jQuery/jQuery-2.1.4.min.js"><\/script>')</script>
      <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
      <script src="{{ asset('/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>


      <script src="{{ asset('/plugins/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
      <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
      <script src="{{ asset('js/ie10-viewport-bug-workaround.js') }}"></script>
      @yield('script')
   </body>
</html>