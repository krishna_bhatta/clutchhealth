<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <?php
                    $user_photo = meta('user_metas', 'user_id', Auth::user()->id, 'profile_photo');
                    $user_photo = ($user_photo!="")?"uploads/profile/".$user_photo:"img/avatar.jpg";
                    ?>
                    <img src="{{ asset($user_photo) }}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <!-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
                </div>
            </div>
        @endif

        <!-- search form (Optional) -->
        <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form> -->
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Find your way</li>
            @if(isset($nav))
            <!-- Optionally, you can add icons to the links -->
            <li {{ ($nav=='user') ? ' class=active treeview' : 'class=treeview' }}>
                <a href="{{ url('admin/users') }}"><i class='fa fa-users'></i> <span>Users</span></a>
            </li>
            <li {{ ($nav=='message') ? ' class=active treeview' : 'class=treeview' }}>
                <a href="{{ url('admin/message') }}"><i class='fa fa-envelope'></i> <span>Messages</span><span class="label label-success pull-right">{{ (unread_inbox(Auth::user()->id) > 0) ? unread_inbox(Auth::user()->id) : '' }}</span></a>
            </li>
            <li {{ ($nav=='blog' || $nav=='add-blog') ? ' class=active treeview' : 'class=treeview' }}>
                <a href="#">
                    <i class='fa fa-paper-plane'></i> <span>Blogs</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li {{ ($nav=='blog') ? ' class=active' : '' }}><a href="{{ url('admin/blogs') }}"><i class="fa fa-files-o"></i> All Blogs</a></li>
                  <li {{ ($nav=='add-blog') ? ' class=active' : '' }}><a href="{{ url('admin/blog/add') }}"><i class="fa fa-plus"></i> Add Blog</a></li>
                </ul>
            </li>
            <li {{ ($nav=='article' || $nav=='add-article') ? ' class=active treeview' : 'class=treeview' }}>
                <a href="#">
                    <i class='fa fa-file-o'></i> <span>Pages</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li {{ ($nav=='article') ? ' class=active' : '' }}><a href="{{ url('admin/pages') }}"><i class="fa fa-files"></i> All pages</a></li>
                  <li {{ ($nav=='add-article') ? ' class=active' : '' }}><a href="{{ url('admin/page/add') }}"><i class="fa fa-plus"></i> Add page</a></li>
                </ul>
            </li>

            <li {{ ($nav=='reward' || $nav=='add-reward') ? ' class=active treeview' : 'class=treeview' }}>
                <a href="#">
                    <i class='fa fa-gift'></i> <span>Reward system</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li {{ ($nav=='reward') ? ' class=active' : '' }}><a href="{{ url('admin/rewards') }}"><i class="fa fa-gift"></i> All rewards</a></li>
                  <li {{ ($nav=='add-reward') ? ' class=active' : '' }}><a href="{{ url('admin/reward/add') }}"><i class="fa fa-plus"></i> Add reward system</a></li>
                </ul>
            </li>
            <li {{ ($nav=='faq' || $nav=='add-faq') ? ' class=active treeview' : 'class=treeview' }}>
                <a href="#">
                    <i class='fa fa-question-circle'></i> <span>FAQ</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li {{ ($nav=='faq') ? ' class=active' : '' }}><a href="{{ url('admin/faqs') }}"><i class="fa fa-question"></i> All faqs</a></li>
                  <li {{ ($nav=='add-faq') ? ' class=active' : '' }}><a href="{{ url('admin/faq/add') }}"><i class="fa fa-plus"></i> Add faq</a></li>
                </ul>
            </li>
            <li {{ ($nav=='question' || $nav=='add-question') ? ' class=active treeview' : 'class=treeview' }}>
                <a href="#">
                    <i class='fa fa-question-circle'></i> <span>Questions</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li {{ ($nav=='question') ? ' class=active' : '' }}><a href="{{ url('admin/questions') }}"><i class="fa fa-question"></i> All questions</a></li>
                  <li {{ ($nav=='add-question') ? ' class=active' : '' }}><a href="{{ url('admin/question/add') }}"><i class="fa fa-plus"></i> Add question</a></li>
                </ul>
            </li>
            <li {{ ($nav=='question') ? ' class=active' : '' }}><a href="{{ url('admin/answers') }}"><i class="fa fa-comments"></i> Answers</a></li>
            <li {{ ($nav=='mail') ? ' class=active' : '' }}>
                <a href="{{ url('admin/broadcast-email') }}">
                    <i class='fa fa-envelope'></i> <span>Broadcast Message</span>
                </a>
            </li>
            @endif
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
