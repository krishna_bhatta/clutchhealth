<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="text-center">
        Copyright &copy; 2015 - {{ date('Y') }} <a href="https://thehomebook.com">The Homebook</a>
    </div>
</footer>