<head>
    <meta charset="UTF-8">
    <title> ClutchHealth - @yield('htmlheader_title', 'Your title here') </title>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <meta name="description" content="Do you want to start incorporating exercise into your life? Are you looking to improve in or learn about a sport? Clutch Health will help you find a trainer or a coach to help you accomplish your goals.">
      <meta name="author" content="Hagi Achren">

      <link rel="apple-touch-icon-precomposed" sizes="57x57" href="comapple-touch-icon-57x57.png" />
      <link rel="apple-touch-icon-precomposed" sizes="114x114" href="comapple-touch-icon-114x114.png" />
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="comapple-touch-icon-72x72.png" />
      <link rel="apple-touch-icon-precomposed" sizes="144x144" href="comapple-touch-icon-144x144.png" />
      <link rel="apple-touch-icon-precomposed" sizes="60x60" href="comapple-touch-icon-60x60.png" />
      <link rel="apple-touch-icon-precomposed" sizes="120x120" href="comapple-touch-icon-120x120.png" />
      <link rel="apple-touch-icon-precomposed" sizes="76x76" href="comapple-touch-icon-76x76.png" />
      <link rel="apple-touch-icon-precomposed" sizes="152x152" href="comapple-touch-icon-152x152.png" />
      <link rel="icon" type="image/png" href="comfavicon-196x196.png" sizes="196x196" />
      <link rel="icon" type="image/png" href="comfavicon-96x96.png" sizes="96x96" />
      <link rel="icon" type="image/png" href="comfavicon-32x32.png" sizes="32x32" />
      <link rel="icon" type="image/png" href="comfavicon-16x16.png" sizes="16x16" />
      <link rel="icon" type="image/png" href="comfavicon-128.png" sizes="128x128" />
      <meta name="application-name" content="ClutchHealth"/>
      <meta name="msapplication-TileColor" content="#FFFFFF" />
      <meta name="msapplication-TileImage" content="http://ClutchHealth.commstile-144x144.png" />
      <meta name="msapplication-square70x70logo" content="http://ClutchHealth.commstile-70x70.png" />
      <meta name="msapplication-square150x150logo" content="http://ClutchHealth.commstile-150x150.png" />
      <meta name="msapplication-wide310x150logo" content="http://ClutchHealth.commstile-310x150.png" />
      <meta name="msapplication-square310x310logo" content="http://ClutchHealth.commstile-310x310.png" />
    <!-- Bootstrap 3.3.4 -->
    <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/plugins/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset('/css/AdminLTE.css') }}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="{{ asset('/css/skins/skin-blue.css') }}" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="{{ asset('/plugins/iCheck/square/red.css') }}" rel="stylesheet" type="text/css" />
    <!-- Modified -->
    <link href="{{ asset('/css/modified.css') }}" rel="stylesheet" type="text/css" />

    @yield('added-css')

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
