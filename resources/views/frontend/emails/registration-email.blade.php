@extends('frontend.layouts.email')

@section('title', 'Activate Your Clutch Health Account')

@section('content')
<tr>
  <td class="h3">
    We're glad you're here, {{ $name }}.
  </td>
</tr>
<tr>
  <td style="padding: 20px 0 0 0;" align="center">
    <table class="buttonwrapper" bgcolor="#be1e2d" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td class="button" height="45">
          <a href="{{ url($link) }}" target="_blank">Activate Account</a>
        </td>
      </tr>
    </table>
  </td>
</tr>
<tr>
  <td class="bodycopy" style="padding-top: 10px;" align="center">
    (Just confirming you're you.)
  </td>
</tr>
@endsection