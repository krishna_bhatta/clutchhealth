@extends('frontend.layouts.email')

@section('title', 'Clutch Health missing you')

@section('content')
<tr>
  <td>
    <strong>Hello Clutch Health Coaches!</strong>
    <div>I wanted to take the time to thank all of you for joining the Clutch Health Team.</div>
    <div>We currently reached a milestone of 13 trainer profiles on the website:</div>
    <div>&nbsp;</div>
    <div>Richard Seto</div>
    <div>Margarita</div>
    <div>Jeffrey r hitchin</div>
    <div>Daniel Bernstein</div>
    <div>Jonathan Gordon</div>
    <div>Mell Barrie</div>
    <div>Melissa</div>
    <div>Hafed Almadani</div>
    <div>Leo Pham</div>
    <div>Kaisha Welsh</div>
    <div>Berly Mvika-Kiesse</div>
    <div>Kwadwo Sefa</div>
    <div>Mark Penelton</div>
    <div>&nbsp;</div>
    <div>Our mission is to decentralize the exercise/fitness market by providing and</div>
    <div>promoting your expertise, knowledge and services to the people of Toronto.</div>
    <div>&nbsp;</div>
    <div>I am very excited to announce that my developer and I have completed the</div>
    <div>integration of the payment system for the website.</div>
    <div>&nbsp;</div>
    <div>Please go to your payment settings here: <a href="http://www.clutchhealth.com/add-payment-method" target="_blank">http://www.clutchhealth.com/add-payment-method</a></div>
    <div>&nbsp;</div>
    <div>For those of you who have not filled out your profile, now would be the time to complete it (Description and picture).</div>
    <div>&nbsp;</div>
    <div>Thank you for your patience, and we look forward to connecting you to new customers</div>
    <div>ASAP.</div>
    <div>&nbsp;</div>
    <div>If you have any questions, comments or concerns please do not hesitate to contact me.</div>
    <div>&nbsp;</div>
    <div>Hagi Achren</div>
    <div>Founder of Clutch Health</div>
    <div>hagiachren2@gmail.com</div>
    <div>&nbsp;</div>
    <div>P.S. If any of you are interested in publishing your own content on to the website&nbsp;</div>
    <div>for extra exposure (blog posts, exercise plans, meal guides, book recommendations etc.) that would be amazing and also&nbsp;</div>
    <div>another great tool for you to promote yourself! Get in touch for more information. &nbsp;</div>
    <div>&nbsp;</div>
    <div>You can also share your Profile on Facebook! <a href="http://www.clutchhealth.com/find-trainer" target="_blank">http://www.clutchhealth.com/find-trainer</a></div>
    <div>Click on your profile and there's a Share button at the bottom.</div>
  </td>
</tr>
<tr>
  <!-- <td style="padding: 20px 0 0 0;" align="center">
    <table class="buttonwrapper" bgcolor="#be1e2d" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td class="button" height="45">
          <a href="{{ url('/login') }}" target="_blank">Login</a>
        </td>
      </tr>
    </table>
  </td> -->
</tr>
<tr>
  <!-- <td class="bodycopy" style="padding-top: 10px;" align="center">
    (Just confirming you're you.)
  </td> -->
</tr>
@endsection