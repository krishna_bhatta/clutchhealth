@extends('frontend.layouts.template')

@section('content')
      <div class="jumbotron">
         <div class="container">
            <h1>We Connect You With The Best Fitness Professionals</h1>
            <p>Looking To Train More Clients? Join The Team </p>
            <a href="{{ url('register') }}" class="btn btn-default btn-lg signup-btn">Sign Up</a>
         </div>
      </div>
      <div class="container" id="platform-features">
         <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12 single-feature">
               <img src="{{ asset('img/img01.jpg') }}" class="img-responsive">
               <h2>Do What You Love</h2>
               <p>Hand pick the type of activities you enjoy with your trainer and start feeling better about yourself! </p>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 single-feature">
               <img src="{{ asset('img/img02.jpg') }}" class="img-responsive">
               <h2>Use That Extra Push</h2>
               <p>Everyone needs a little help, and your trainer will make sure you achieve your goals in a fun and safe manner.</p>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 single-feature">
               <img src="{{ asset('img/img03.jpg') }}" class="img-responsive">
               <h2>Share With Friends</h2>
               <p>Whether it's for business or pleasure, share the experience of motivating exercise at a discounted price.</p>
            </div>
         </div>
         <div class="row" id="more-advice">
            <h2>Live a healthy, active lifestyle Exercise with extra motivation </h2>
         </div>
      </div>
@endsection