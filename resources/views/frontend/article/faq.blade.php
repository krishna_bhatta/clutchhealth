@extends('frontend.layouts.template')

@section('content')
<div class="container" id="platform-features">
	<h3>Frequently asked questions</h3>
	@foreach($faq as $row)
		{{ $row->question }}
	@endforeach
</div>
@endsection



@section('htmlheader_title')
Home
@endsection