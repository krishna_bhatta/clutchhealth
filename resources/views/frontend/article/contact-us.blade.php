@extends('frontend.layouts.template')

@section('content')
<div class="container contact-us" id="platform-features">
    <h3 class="page-title">If you have any questions, we're here to answer them.</h3>
    <div class="form-group">
    	<form id="login-frm" action="{{ url('/submit-contact') }}" method="post">
   		<input type="hidden" name="_token" value="{{ csrf_token() }}">
	        <div class="col-md-8">
	            <div class="form-group">
	                <div class="col-md-6">
	                    <input type="text" name="name" required id="name" placeholder="Type your name here" class="form-control" />
	                </div>
	            </div>
	            <div class="form-group">
	                <div class="col-md-6">
	                    <input type="email" name="email" required id="email" placeholder="Type your email address" class="form-control" />
	                </div>
	            </div>
	            <div class="form-group">
	                <div class="col-md-12">
	                    <textarea name="message" required id="message" cols="30" rows="10" placeholder="Share with us any information that might help us to respond to your request" class="form-control"></textarea>
	                </div>
	            </div>
	            <div class="form-group">
	                <div class="col-md-12 text-center">
	                	<button type="submit" class="btn btn-success">Submit</button>
	                	<button type="reset" class="btn btn-danger">Reset</button>
	                </div>
	            </div>
	        </div>
	        <div class="col-md-4 right-side">
	        	<a class="btn btn-info" href="mailto:support@clutchhealth.com" target="_top">support@clutchhealth.com</a>
	        	<!-- <div class="col-md-12">
	        		<p>
		        		156 Crestwood Rd.<br />
						Thornhill, Ontario L4J1A6<br />
						Canada
					</p>
	        	</div>

				<div class="col-md-6 text-left">
					<a class="btn btn-info" href="mailto:support@clutchhealth.com" target="_top">support@clutchhealth.com</a>
				</div>
				<div class="col-md-6 text-right">
					<a class="btn btn-success" href="tel:16472425646"> +1 647-242-5646</a>
				</div> -->
	        </div>
	    </form>
    </div>
</div>
@endsection


@section('meta_title', 'Contact Us')
@section('meta_description', 'Please let us know how we can help. Contact us at support@clutchhealth.com')