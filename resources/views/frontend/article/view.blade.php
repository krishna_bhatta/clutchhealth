@extends('frontend.layouts.template')

@section('content')
<div class="container" id="platform-features">
   <h3>{{ $article->title }}</h3>
   <div class="article-box">
      {!! $article->details !!}
   </div>
</div>
@endsection

<?php
$fi = meta('article_metas', 'article_id', $article->id, 'fb_image');
$ti = meta('article_metas', 'article_id', $article->id, 'twitter_image');
?>


@section('meta_title', meta('article_metas', 'article_id', $article->id, 'meta_title'))
@section('meta_description', meta('article_metas', 'article_id', $article->id, 'meta_description'))
@if($fi != "")
	@section('fi', asset('uploads/media/'.$fi))
@endif
@if($ti != "")
	@section('ti', asset('uploads/media/'.$ti))
@endif