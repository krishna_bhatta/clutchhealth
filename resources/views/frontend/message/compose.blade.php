@extends('frontend.layouts.template-dashboard')

@section('left')
<div class="box box-primary dashboard-left">
	<div class="box-header">
		<h3 class="box-title"><i class="fa fa-envelope" aria-hidden="true"></i> Messages</h3>
	</div>
	<div class="box-body">
	<ul>
		<li><a href="{{ url('/message/inbox') }}"><i class="fa fa-inbox" aria-hidden="true"></i> Inbox</a></li>
		<li><a href="{{ url('/message/sent') }}"><i class="fa fa-paper-plane" aria-hidden="true"></i> Sent</a></li>
		<li><a href="{{ url('/message/trash') }}"><i class="fa fa-trash" aria-hidden="true"></i> Trash</a></li>
	</ul>
	</div>
</div>
<a class="btn btn-info compose-btn" href="{{ url('message/compose') }}"><i class="fa fa-pencil" aria-hidden="true"></i> New Message</a>
@endsection

@section('right')
  <form action="{{ url('admin/submitCompose') }}" method="post">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <h3 class="box-title">Compose New Message</h3>
    <div class="form-group">
      <div class="row">
        <div class="col-md-12">
          @if (count($errors) > 0)
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif
        </div>
      </div>
    </div>
    <div class="form-group">
      <div class="row">
      @if($receipt_id)
        <input type="hidden" name="to" value="{{ $receipt_id }}">
        <div class="col-md-12">
          <input class="form-control" name="subject" placeholder="Subject:">
        </div>
        @else
        <div class="col-md-4">
          <select class="form-control" name="to">
            <option value="">-- Select User --</option>
            @foreach($users as $user)
            <option value="{{ $user->id }}">{{ $user->name }}</option>
            @endforeach
          </select>
        </div>
        <div class="col-md-8">
          <input class="form-control" name="subject" placeholder="Subject:">
        </div>
        @endif
      </div>
    </div>
    <div class="form-group">
          <textarea id="compose-textarea" placeholder="Message" name="message" class="form-control" style="min-height: 300px"></textarea>
    </div>
  <div class="form-group">
    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" class="btn btn-block signup-btn">Send</button>
      </div>
    </div>
  </div>
  <!-- /.box-footer -->
  </form>

@endsection

@section('htmlheader_title')
Messages
@endsection

@section('css')
<!-- iCheck -->
<link href="{{ asset('/plugins/iCheck/flat/blue.css') }}" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link href="{{ asset('/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('script')
<!-- DataTables -->
<script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
  $("#inbox-tbl").DataTable({
    "aoColumns": [
            { "bSortable": false },
            null,
            null,
            null
         ],
      "order": [[ 3, "desc" ]]
  });

  $('input[type="checkbox"]').iCheck({
    checkboxClass: 'icheckbox_flat-blue',
    radioClass: 'iradio_flat-blue'
  });

  //Enable check and uncheck all functionality
  $(".checkbox-toggle").click(function () {
    var clicks = $(this).data('clicks');
    if (clicks) {
      //Uncheck all checkboxes
      $("#inbox-tbl input[type='checkbox']").iCheck("uncheck");
      $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
    } else {
      //Check all checkboxes
      $("#inbox-tbl input[type='checkbox']").iCheck("check");
      $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
    }
    $(this).data("clicks", !clicks);
  });
});
</script>
@endsection
