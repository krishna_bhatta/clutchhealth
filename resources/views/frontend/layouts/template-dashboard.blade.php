@extends('frontend.layouts.template')
@section('content')
<div class="dashboard-container">
   <div class="container">
      <div class="row">
         <div class="col-md-3">
              @yield('left', 'Left Content')
         </div>
         <div class="col-md-9">
           <div class="box box-primary dashboard-right">
             @if (count($errors) > 0)
                 <div class="alert alert-danger">
                     <strong>Whoops!</strong> There were some problems with your input.<br><br>
                     <ul>
                         @foreach ($errors->all() as $error)
                             <li>{{ $error }}</li>
                         @endforeach
                     </ul>
                 </div>
             @endif
            @yield('right', 'Right Content')
           </div>
         </div>
      </div>
   </div>
</div>
@endsection
