@if(Auth::guest())
      <li><a href="{{ url('find-trainer') }}">Find Trainer</a></li>
      <li><a href="{{ url('about-us') }}">About Us</a></li>
      <li><a href="{{ url('exercise') }}">Exercise</a></li>
      <li><a href="{{ url('coach') }}">Coach</a></li>
      <li><a href="{{ url('blog') }}">Blog</a></li>
      <li><a href="{{ url('contact-us') }}">Contact Us</a></li>
@else
   @if(Auth::user()->role_id == 2)
      <li><a href="{{ url('/dashboard') }}">Dashboard</a></li>
      <li><a href="{{ url('/contracts') }}">Contracts</a></li>
      <li><a href="{{ url('/messages') }}">Messages</a></li>
      <li><a href="{{ url('about-us') }}">About Us</a></li>
      <li><a href="{{ url('contact-us') }}">Contact Us</a></li>
   @elseif(Auth::user()->role_id == 3)
      <li><a href="{{ url('/dashboard') }}">Dashboard</a></li>
      <li><a href="{{ url('find-trainer') }}">Find Trainer</a></li>
      <li><a href="{{ url('/messages') }}">Messages</a></li>
      <li><a href="{{ url('about-us') }}">About Us</a></li>
      <li><a href="{{ url('contact-us') }}">Contact Us</a></li>
   @else
      <li><a href="{{ url('find-trainer') }}">Find Trainer</a></li>
      <li><a href="{{ url('about-us') }}">About Us</a></li>
      <li><a href="{{ url('exercise') }}">Exercise</a></li>
      <li><a href="{{ url('coach') }}">Coach</a></li>
      <li><a href="{{ url('blog') }}">Blog</a></li>
      <li><a href="{{ url('contact-us') }}">Contact Us</a></li>
   @endif
@endif