         <!-- /container -->
         <footer id="footer">
            <footer class="footer">
               <div class="container">
                  <nav class="footer-navbar">
                     <ul>
                        <li><a href="{{ url('/') }}">Home</a></li>
                        <!-- <li><a href="{{ url('faq') }}">FAQ</a></li> -->
                        <li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
                        <li><a href="{{ url('terms-and-conditions') }}">Terms & Conditions</a></li>
                     </ul>
                  </nav>
                  <span class="copyright-txt">&copy; 2015-{{ date('Y') }} ClutchHealth. <span class="all-rights">All rights reserved.</span></span>
                  <div class="footer-social">
                     <ul>
                        <li><a href="https://www.facebook.com/ClutchHealth" class="fb" target="_blank"></a></li>
                        <li><a href="https://www.twitter.com/ClutchHealth" class="tw" target="_blank"></a></li>
                     </ul>
                  </div>
               </div>
            </footer>
         </footer>
      </div>

      <?php
      if(Auth::check()):
        $survey = survey_questions(Auth::user()->id);
      else:
        $survey = survey_questions();
      endif;
      if($survey):
      ?>
         <!-- Survey Modal -->
         <div id="surveyModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">

            <form class="form-horizontal" method="post" action="{{ url('answer-action') }}">
               <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <div class="modal-dialog modal-lg" role="document">

                   <!-- Modal content-->
                   <div class="modal-content">
                     <div class="modal-header" style="padding-bottom:0;">
                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                       <h2 style="margin-bottom:0;">Give us Feedback</h2>
                     </div>
                     <div class="modal-body" style="padding-top:0;">
                        @foreach($survey as $question)
                        <input type="hidden" name="questions[]" value="{{ $question->id }}" />
                           <div class="form-group">
                               <div class="col-md-12">
                                   <h3>{{ $question->title }}</h3>
                                   @if($question->type == "radio" && $question->options != "")
                                   <?php foreach(explode('~', $question->options) as $option): ?>
                                   <label><input type="radio" value="{{ $option }}" name="answer_{{ $question->id }}" /> {{ $option }}</label>
                                   <?php endforeach; ?>
                                   @elseif($question->type == "textarea")
                                   <textarea name="answer_{{ $question->id }}" class="form-control"></textarea>
                                   @else
                                   <input type="text" name="answer_{{ $question->id }}" class ="form-control"/>
                                   @endif
                               </div>
                           </div>
                        @endforeach
                     </div>
                     <div class="modal-footer" style="text-align:center;">
                       <button type="submit" class="btn btn-success">Submit</button>
                     </div>
                   </div>

                 </div>

                  </form>
         </div>
      <?php endif; ?>

      <!-- Bootstrap core JavaScript
         ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script>window.jQuery || document.write('<script src="/plugins/jQuery/jQuery-2.1.4.min.js"><\/script>')</script>
      <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
      <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
      <script src="{{ asset('js/ie10-viewport-bug-workaround.js') }}"></script>
      <!-- Pnotify -->
      <script src="{{ asset('plugins/pnotify/pnotify.custom.min.js') }}"></script>
      @yield('script')
@if(session()->has('notify'))
<?php $notify = Session::get('notify'); ?>
      <script type="text/javascript">
           jQuery(document).ready(function($) {
              new PNotify({
                  title: '{{ $notify->title }}',
                  text: '{{ $notify->text }}',
                  type: '{{ $notify->type }}', // info, success, error
                  styling: 'bootstrap3'
              });
           });
      </script>
@endif

   <script type="text/javascript">
   $(document).ready(function($) {
      var isTried = false;
      $(document).mouseleave(function () {
         if(isTried == false) {
            isTried = true;
             $('#surveyModal').modal('show');
          }
      });
   });
   </script>

   <!-- Facebook Pixel Code -->
   <script>
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
   n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
   document,'script','https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '1833534723546233');
   fbq('track', 'PageView');
   </script>
   <noscript><img height="1" width="1" style="display:none"
   src="https://www.facebook.com/tr?id=1833534723546233&ev=PageView&noscript=1"
   /></noscript>
   <!-- DO NOT MODIFY -->
   <!-- End Facebook Pixel Code -->
   
   </body>
</html>