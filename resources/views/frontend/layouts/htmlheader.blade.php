
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title>Clutch Health - @yield('meta_title', 'Personal trainer in Toronto, Sign up to Coach, Start Exercising in Toronto')</title>
      <meta name="description" content="@yield('meta_description', 'We want to help you achieve Clutch status when it matters most. Sign up and start exercising with your favourite coach. Or become a coach and reach out to more people.')">
      <link rel="canonical" href="{{ Request::url() }}" />
      <meta name="allow-search" content="yes" />
      <meta name="revisit-after" content="7 Days" />
      <meta name="rating" content="Fitness, Personal trainer, Sign up to Coach, Exercise" />
      <meta name="copyright" content="copy rights ClutchHealth" />
      <meta name="country" content="Canada" />
      <meta name="category" content="Fitness, Personal trainer, Sign up to Coach, Exercise" />
      <meta name="document-type" content="public" />
      <meta name="classification" content="Fitness, Personal trainer, Sign up to Coach, Exercise" />
      <meta name="resource-type" content="document" />
      <meta name="distribution" content="global" />
      <meta http-equiv="bulletin-text" content="Fitness, Personal trainer, Sign up to Coach, Exercise" />
      <meta content="no-cache" http-equiv="pragma" />
      <meta name="author" content="Hagi Achren">
      <meta name = "yandex-verification" content = "78aa870a0c024ddd" />

      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:site" content="@ClutchHealth" />
      <meta name="twitter:title" content="ClutchHealth - @yield('meta_title', 'Personal trainer in Toronto, Sign up to Coach, Start Exercising in Toronto')"/>
      <?php $tImg = asset('img/ClutchHealth-Twitter.png'); ?><meta name="twitter:image" content="@yield('ti', $tImg)" />
      <meta name='twitter:url' content="{{ Request::url() }}" />
      <meta name="twitter:description" content="@yield('meta_description', 'We want to help you achieve Clutch status when it matters most. Sign up and start exercising with your favourite coach. Or become a coach and reach out to more people.')" />
      <meta property="og:url" content="{{ Request::url() }}" />
      <meta property="og:type"      content="@yield('page_type', 'website')" />
      <meta property="og:title"     content="ClutchHealth - @yield('meta_title', 'Personal trainer in Toronto, Sign up to Coach, Start Exercising in Toronto')" />
      <meta property="og:description"     content="@yield('meta_description', 'We want to help you achieve Clutch status when it matters most. Sign up and start exercising with your favourite coach. Or become a coach and reach out to more people.')" />
      <?php $fImg = asset('img/ClutchHealth-Facebook.png'); ?><meta property="og:image" content="@yield('fi', $fImg)" />
      
      <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{ asset('apple-touch-icon-57x57.png') }}" />
      <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('apple-touch-icon-114x114.png') }}" />
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('apple-touch-icon-72x72.png') }}" />
      <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('apple-touch-icon-144x144.png') }}" />
      <link rel="apple-touch-icon-precomposed" sizes="60x60" href="{{ asset('apple-touch-icon-60x60.png') }}" />
      <link rel="apple-touch-icon-precomposed" sizes="120x120" href="{{ asset('apple-touch-icon-120x120.png') }}" />
      <link rel="apple-touch-icon-precomposed" sizes="76x76" href="{{ asset('apple-touch-icon-76x76.png') }}" />
      <link rel="apple-touch-icon-precomposed" sizes="152x152" href="{{ asset('apple-touch-icon-152x152.png') }}" />
      <link rel="icon" type="image/png" href="{{ asset('favicon-196x196.png') }}" sizes="196x196" />
      <link rel="icon" type="image/png" href="{{ asset('favicon-96x96.png') }}" sizes="96x96" />
      <link rel="icon" type="image/png" href="{{ asset('favicon-32x32.png') }}" sizes="32x32" />
      <link rel="icon" type="image/png" href="{{ asset('favicon-16x16.png') }}" sizes="16x16" />
      <link rel="icon" type="image/png" href="{{ asset('favicon-128.png') }}" sizes="128x128" />
      <meta name="application-name" content="&nbsp;"/>
      <meta name="msapplication-TileColor" content="#FFFFFF" />
      <meta name="msapplication-TileImage" content="{{ asset('mstile-144x144.png') }}" />
      <meta name="msapplication-square70x70logo" content="{{ asset('mstile-70x70.png') }}" />
      <meta name="msapplication-square150x150logo" content="{{ asset('mstile-150x150.png') }}" />
      <meta name="msapplication-wide310x150logo" content="{{ asset('mstile-310x150.png') }}" />
      <meta name="msapplication-square310x310logo" content="{{ asset('mstile-310x310.png') }}" />
      <meta name="theme-color" content="#be1e2d">      