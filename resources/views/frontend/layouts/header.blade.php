<!DOCTYPE html>
<html lang="en">
   <head>
   @include('frontend.layouts.htmlheader')

   <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
   <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700,500' rel='stylesheet' type='text/css'>
   <!-- Bootstrap core CSS -->
   <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet" type="text/css" />
   <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
   <link href="{{ asset('css/ie10-viewport-bug-workaround.css') }}" rel="stylesheet">   
   <!-- Custom styles for pnotify-->
   <link href="{{ asset('plugins/pnotify/pnotify.custom.min.css') }}" rel="stylesheet">
   
   <!-- Custom styles for this template -->
   <link href="{{ asset('css/frontend-main.css') }}" rel="stylesheet">
   <!-- Responsive styles for this template -->
   <link href="{{ asset('css/frontend-responsive.css') }}" rel="stylesheet">
   <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
   @yield('css')
   <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
   <!--[if lt IE 9]><script src="{{ asset('js/ie8-responsive-file-warning.js') }}"></script><![endif]-->
   <script src="{{ asset('js/ie-emulation-modes-warning.js') }}"></script>
   <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
   <!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
   <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
   <![endif]-->
   </head>
   <body>
     <div id="fb-root"></div>
     <script>(function(d, s, id) {
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) return;
       js = d.createElement(s); js.id = id;
       js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.7&appId=1042174819184566";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));</script>
   
      <div class="body-wrapper">
      <!-- Static navbar -->
      <nav class="navbar navbar-default navbar-static-top">
         <div class="container">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               @if(!Auth::guest())
                  @if(Auth::user()->role_id == 2 || Auth::user()->role_id == 3)
                     <a class="navbar-brand" href="{{ url('dashboard') }}"><img src="{{ asset('img/logo.png') }}" class="img-responsive" ></a>
                  @else
                     <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ asset('img/logo.png') }}" class="img-responsive" ></a>
                  @endif
               @else
                  <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ asset('img/logo.png') }}" class="img-responsive" ></a>
               @endif
            </div>
            <div id="navbar" class="navbar-collapse collapse">
               <div class="loginbox pull-right">
               @if(Auth::guest())
                  <i class="fa fa-user"></i> &nbsp; 
                  <a href="{{ url('/login') }}">Login</a> / 
                  <a href="{{ url('/register') }}">Register</a>
               @elseif(Auth::user()->role_id == 1)
               <a href="{{ url('/admin/home') }}">{{ Auth::user()->name }}</a>
               @else
               <div class="dropdown">
                   <a href="#" data-toggle="dropdown" class="dropdown-toggle">{{ Auth::user()->name }} <b class="caret"></b></a>
                   <ul class="dropdown-menu">
                       <li><a href="{{ url('/user/profile') }}"><i class="fa fa-cog" aria-hidden="true"></i> My Profile</a></li>
                       @if(Auth::user()->role_id == 2)
                       <li><a href="{{ url('/payment-settings') }}"><i class="fa fa-usd" aria-hidden="true"></i> Payment settings</a></li>
                       @endif
                       <li><a href="{{ url('/logout') }}"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a></li>
                   </ul>
               </div>
               @endif
               </div>
               <div class="clear"></div>
               <ul class="nav navbar-nav" id="main-nav">
                  @include('frontend.layouts.main-nav')
               </ul>
            </div>
         </div>
      </nav>
      <div class="clear"></div>