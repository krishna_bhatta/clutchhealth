@extends('frontend.layouts.template-dashboard')

@section('left')
<div class="box box-primary dashboard-left">
	<div class="box-header">
		<h3 class="box-title"><i class="fa fa-usd" aria-hidden="true"></i> Payments</h3>
	</div>
	<div class="box-body">
	<ul>
		<li><a href="{{ url('payment-settings') }}"><i class="fa fa-cog" aria-hidden="true"></i> Payment settings</a></li>
        <li><a href="{{ url('add-payment-method') }}"><i class="fa fa-plus" aria-hidden="true"></i> Add new method</a></li>
	</ul>
	</div>
</div>
@endsection

@section('right')
<h3 style="margin-top:0;">Payment settings</h3>
<table width="100%" class="table">
    <tr>
        <th>SN</th>
        <th>Routing Number</th>
        <th>Account Number</th>
        <th>Status</th>
    </tr>
    <?php $i = 1; ?>
    @foreach($paymentMethod as $method)
    <?php $account_details = Stripe\Account::retrieve($method->stripe_payment_method_id)->external_accounts->data; ?>
    <tr>
        <td>{{ $i++ }}</td>
        <td>{{ $account_details[0]['routing_number'] }}</td>
        <td>****{{ $account_details[0]['last4'] }}</td>
        <td>{{ $account_details[0]['status'] }}</td>
    </tr>
    @endforeach
</table>
@endsection

@section('htmlheader_title')
Payment settings
@endsection

@section('css')

@endsection

@section('script')

@endsection