@extends('frontend.layouts.template-dashboard')

@section('left')
<div class="box box-primary dashboard-left">
	<div class="box-header">
		<h3 class="box-title"><i class="fa fa-envelope" aria-hidden="true"></i> Dashboard</h3>
	</div>
	<div class="box-body">
	<ul>
		@if(Auth::user()->role_id == 2)
		<li><a href="{{ url('contracts') }}"><i class="fa fa-user" aria-hidden="true"></i> My Customers</a></li>
		@else
		<li><a href="{{ url('contract/my-trainer') }}"><i class="fa fa-user" aria-hidden="true"></i> My Trainer</a></li>
		@endif
		<li><a href="{{ url('contract/history') }}"><i class="fa fa-history" aria-hidden="true"></i> History</a></li>
	</ul>
	</div>
</div>
@endsection

@section('right')
<div class="col-md-8">
	<h3 style="margin-top:0;">Pay to {{ $contract->trainer->name }}</h3>
</div>
<div class="col-md-4 text-right">
	<strong>${{ $contract->budget." per ".$contract->payment_mode->title }}</strong>
</div>
<form id="payment-frm" action="{{ url('pay-process') }}" method="POST">
<input type="hidden" name="contract_id" value="{{ $contract->id }}" />
<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<div class="col-md-6">
		<input type="text" class="form-control" name="first_name" value="{{ $first_name }}" placeholder="First name" />
	</div>
	<div class="col-md-6">
		<input type="text" class="form-control" name="last_name" value="{{ $last_name }}" placeholder="Last name" />
	</div>
	<div class="col-md-4">
		<input type="text" class="form-control" name="email" value="{{ $email }}" placeholder="Email" />
	</div>
	<div class="col-md-4">
		<input type="text" class="form-control" name="total_mode" id="total_mode" placeholder="Total {{ $contract->payment_mode->title }}/s (Default 1)" />
	</div>
	<input type="hidden" name="total-payment-hidden" id="total-payment-hidden" />
	<div class="col-md-4">
		<span class="form-control total-payment">Total ${{ $contract->budget }}</span>
	</div>
    <div class="col-md-12">
    	<input type="text" data-stripe="number" placeholder="Card number" class="form-control" />
    </div>
    <div class="col-md-4">
    	<input type="text" data-stripe="cvc" placeholder="Security code" class="form-control" />
    </div>
    <div class="col-md-4">
    	<input type="text" data-stripe="exp-month" placeholder="Expiry month" class="form-control" />
    </div>
    <div class="col-md-4">
    	<input type="text" data-stripe="exp-year" placeholder="Expiry year" class="form-control" />
    </div>
    <div class="col-md-6">
    	<button type="button" id="submit-payment" class="btn btn-success pull-right" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Verifying data">Submit</button>
    </div>
    <div class="col-md-6">
    	<button type="reset" class="btn btn-error pull-left">Reset</button>
    </div>
</form>
<div style="clear:both;"></div>
@endsection

@section('htmlheader_title')
Card details
@endsection

@section('css')
<style type="text/css">
	.btn-group button {
		display: inline;
	}
</style>
@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(function($) {
		$("#total_mode").keyup(function(event) {
			var n = parseInt($("#total_mode").val());
			if(n<1 || isNaN(n)) {
				n = 1;
			}
			var perVal = parseInt({{ $contract->budget }})
			var totalAmount = n*perVal;
			console.log(n);
			$(".total-payment").html("Total $"+totalAmount);
			$("#total-payment-hidden").val(totalAmount);
		});
	});
</script>
<script src="{{ asset('plugins/parsley/parsley.js') }}"></script>
<!-- Inlude Stripe.js -->
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
	Stripe.setPublishableKey('{!! env('STRIPE_KEY') !!}');
	$("#submit-payment").click(function(event) {
		$(this).button('loading');
		var $form = $('#payment-frm');
		Stripe.card.createToken($form, stripeResponseHandler);
	});
	function stripeResponseHandler(status, response) {
	    var $form = $('#payment-frm');

	    if (response.error) {
	    	$("#submit-payment").button('reset');
	        // Show the errors on the form
	        new PNotify({
	            title: 'Error',
	            text: response.error.message,
	            type: 'error', // info, success, error
	            styling: 'bootstrap3'
	        });
	        return false;
	    } else {
	        // response contains id and card, which contains additional card details
	        var token = response.id;
	        // Insert the token into the form so it gets submitted to the server
	        $form.append($('<input type="hidden" name="stripeToken" />').val(token));
	        $('#payment-frm').submit();
	    }
	};
</script>
@endsection