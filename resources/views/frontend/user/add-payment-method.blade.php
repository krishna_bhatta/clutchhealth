@extends('frontend.layouts.template-dashboard')

@section('left')
<div class="box box-primary dashboard-left">
	<div class="box-header">
		<h3 class="box-title"><i class="fa fa-usd" aria-hidden="true"></i> Payments</h3>
	</div>
	<div class="box-body">
	<ul>
		<li><a href="{{ url('payment-settings') }}"><i class="fa fa-cog" aria-hidden="true"></i> Payment settings</a></li>
        <li><a href="{{ url('add-payment-method') }}"><i class="fa fa-plus" aria-hidden="true"></i> Add new method</a></li>
	</ul>
	</div>
</div>
@endsection

@section('right')
<div class="account-holder-details">
    <div class="row">
        <div class="col-md-12">
            <label for="account_holder_name">Account Holder Name</label>
            <input type="text" name="account_holder_name" id="account_holder_name" class="account_holder_name form-control" value="{{ $user->name }}" />
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label for="routing-number">Routing Number</label>
            <input type="text" name="routing_number" id="routing-number" class="routing_number form-control" /> <!--  value="11000000"  -->
        </div>
        <div class="col-md-6">
            <label for="account-number">Account Number</label>
            <input type="text" name="account_number" id="account-number" class="account_number form-control" /> <!--  value="000123456789" -->
        </div>
    </div>

    <div class="row text-center">
        <button type="button" class="btn btn-success process" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Verifying data">Proceed</button>
    </div>
</div>

<div class="account-verify-details">
    <form action="{{ url('add-payment-method-process') }}" method="POST" id="bank-account-form" enctype="multipart/form-data">
        <input type='hidden' name='external_account' id='stripe_token_holder' />
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
            <div class="col-md-6">
                <label for="first_name">First name</label>
                <input type="text" name="first_name" id="first_name" value="{{ old('fist_name', $user->first_name) }}" class="form-control" />
            </div>
            <div class="col-md-6">
                <label for="last_name">Last name</label>
                <input type="text" name="last_name" id="last_name" value="{{ old('last_name', $user->last_name) }}" class="form-control" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label for="address1">Address</label>
                <input type="text" class="form-control" name="address1" value="{{ old('address1', $user->address) }}" id="address1" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="city">City</label>
                <input type="text" class="form-control" name="city" value="{{ old('city', $user->city) }}" id="city" />
            </div>
            <div class="col-md-4">
                <label for="postal_code">Postal code</label>
                <input type="text" class="form-control" name="postal_code" value="{{ old('postal', $user->postal) }}" id="postal_code" />
            </div>
            <div class="col-md-4">
                <label for="state">State</label>
                <select id="state" class="form-control" name="state">
                @foreach($canadian_states as $key=>$value)
                    <option value="{{ $key }}">{{ $value }}</option>
                @endforeach
                </select>
            </div>
        </div>    
        <div class="row">
            <div class="col-md-6">
                <label for="personal_document">Personal document(Driver's license or Passport)</label>
                <input type="file" name="personal_document" id="personal_document" class="form-control" />
            </div>
            <div class="col-md-6">
                <label for="personal_id_number">Personal ID number</label>
                <input type="text" name="personal_id_number" value="{{ old('personal_id_number') }}" id="personal_id_number" class="form-control" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label for="date_of_birth">Date of birth(yyyy-mm-dd)</label>
                <input type="text" class="form-control" value="{{ old('date_of_birth', $user->date_of_birth) }}" name="date_of_birth" id="date_of_birth" />
            </div>
        </div>
        <div class="row text-center">
            <button type="submit" id="submit-btn" class="submit-button btn btn-success">Submit</button>
        </div>
    </form>
</div>
<div style="clear:both;"></div>
@endsection

@section('htmlheader_title')
Add payment method
@endsection

@section('css')
<link href="{{ asset('/plugins/datepicker/datepicker3.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/plugins/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
	.btn-group button {
		display: inline;
	}
    #bank-account-form {
        display: none;
    }
</style>
@endsection

@section('script')
<!-- Inlude Stripe.js -->
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script src="/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="/plugins/bootstrapvalidator/js/bootstrapValidator.min.js"></script>

<script type="text/javascript">
Stripe.setPublishableKey('{!! env('STRIPE_KEY') !!}');

$(document).ready(function($) {
    $('#date_of_birth').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });

    $(".process").click(function(event) {
        $(this).button('loading');
        Stripe.bankAccount.createToken({
            country: 'CA',
            currency: 'CAD',
            account_holder_type: 'individual',
            routing_number: $('.routing_number').val(),
            account_number: $('.account_number').val(),
            account_holder_name: $('.account_holder_name').val()
        }, stripeResponseHandler);
    });

    $('#bank-account-form').bootstrapValidator({
        fields: {
            first_name: {
                validators: {
                    notEmpty: {
                        message: 'First name field is required'
                    }
                }
            },
            last_name: {
                validators: {
                    notEmpty: {
                        message: 'Last name field is required'
                    }
                }
            },
            city: {
                validators: {
                    notEmpty: {
                        message: 'City field is required'
                    }
                }
            },
            postal_code: {
                validators: {
                    notEmpty: {
                        message: 'Postal code field is required'
                    }
                }
            },
            state: {
                validators: {
                    notEmpty: {
                        message: 'State field is required'
                    }
                }
            },
            address1: {
                validators: {
                    notEmpty: {
                        message: 'Address field is required'
                    }
                }
            },
            personal_document: {
                validators: {
                    notEmpty: {
                        message: 'Personal document is required'
                    },
                    file: {
                        extension: 'jpeg,jpg,png',
                        type: 'image/jpeg,image/png',
                        maxSize: 5120 * 5120,   // 2MB
                        message: 'Personal document must be either jpg or png with max size 2MB'
                    }
                }
            },
            personal_id_number: {
                validators: {
                    notEmpty: {
                        message: 'Personal id number is required'
                    }
                }
            },
            date_of_birth: {
                validators: {
                    notEmpty: {
                        message: 'Date of bith is required'
                    }
                }
            },
            stripeToken: {
                validators: {
                    notEmpty: {
                        message: 'Stripe bank token is required'
                    }
                }
            }
        },
        onSuccess: function(e, data) {
            return true;
        }
    });
});


function stripeResponseHandler(status, response) {
    if (response.error) {
        $(".process").button('reset');
        new PNotify({
            title: 'Error',
            text: response.error.message,
            type: 'error', // info, success, error
            styling: 'bootstrap3'
        });
    } else {
        $(".account-holder-details").fadeOut('400', function() {
            $("#stripe_token_holder").attr('value', response['id']);
            $("#bank-account-form").fadeIn();
        });
    }
}
</script>
@endsection