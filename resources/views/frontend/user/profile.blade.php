@extends('frontend.layouts.template')
@section('content')
<div class="profile-container">
   <div class="container">
   <form class="form-horizontal" id="edit-frm" method="post" action="{{ url('user/updateProfileAction') }}" enctype="multipart/form-data">
      <div class="row">
         <div class="col-md-4 profile-left">
            <!-- Profile Image -->
            <div class="box box-primary">
               <div class="box-body box-profile">
                  <?php
                     $user_photo = meta('user_metas', 'user_id', $user->id, 'profile_photo');
                     $user_photo = ($user_photo!="")?"uploads/profile/".$user_photo:"img/avatar.jpg";
                     ?>
                  <img class="profile-user-img img-responsive img-circle" src="{{ asset($user_photo) }}" alt="User profile picture">
                  <h3 class="profile-username text-center">{{ $user->name }}</h3>
                  @if($user->role_id == 2)
                  <p class="text-muted text-center">
                    {{ $user->display }}
                  </p>
                  @endif
               </div>
               <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <!-- About Me Box -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">About Me<span id="reward-title"><i class="fa fa-gift" aria-hidden="true"></i> {{ $rewards }}</span></h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
               @if($user->role_id == 3)
                  <strong><i class="fa fa-book margin-r-5"></i> Description</strong>
                  <p class="text-muted">
                     {{ meta('user_metas', 'user_id', $user->id, 'description') }}
                  </p>
                  <hr>
              @endif
                  <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                  <p class="text-muted">{{ $user->address }}</p>
                  <hr>
                  @if($user->role_id == 2)
                    <strong><i class="fa fa-fire" aria-hidden="true"></i> Specifications</strong>
                      <input type="text" name="specifications" value="{{ meta('user_metas', 'user_id', $user->id, 'specifications') }}" id="specifications" class="form-control" />
                    <hr>
                    <strong><i class="fa fa-trophy margin-r-5"></i> Awards</strong>
                      <input type="text" name="awards" value="{{ meta('user_metas', 'user_id', $user->id, 'awards') }}" id="awards" class="form-control" />
                    <hr>
                    <strong><i class="fa fa-calendar margin-r-5"></i> Training started year</strong>
                      <input type="text" name="training_start" placeholder="Eg. 2001" value="{{ meta('user_metas', 'user_id', $user->id, 'training_start') }}" id="training_start" class="form-control" />
                    <hr>
                    <strong><i class="fa fa-certificate margin-r-5"></i> Certificates</strong>
                      <input type="text" name="certificates" value="{{ meta('user_metas', 'user_id', $user->id, 'certificates') }}" id="certificates" class="form-control" />
                    <hr>
                  @endif
               </div>
               <!-- /.box-body -->
            </div>
            <!-- /.box -->
         </div>
         <div class="col-md-8">
         <div class="col-md-12 body-right">
            	        @if (count($errors) > 0)
            	            <div class="alert alert-danger">
            	                <strong>Whoops!</strong> There were some problems with your input.<br><br>
            	                <ul>
            	                    @foreach ($errors->all() as $error)
            	                        <li>{{ $error }}</li>
            	                    @endforeach
            	                </ul>
            	            </div>
            	        @endif
            	          	<input type="hidden" name="_token" value="{{ csrf_token() }}">
            	            <div class="form-group">
            	              <div class="col-sm-6">
            	                <input type="text" class="form-control" value="{{ $user->name }}" name="name" placeholder="Name">
            	              </div>
            	              <div class="col-sm-6">
            	                <input type="email" value="{{ $user->email }}" class="form-control" name="email" placeholder="Email">
            	              </div>
            	            </div>
            	            <div class="form-group">
            	              <div class="col-sm-12">
            	                <textarea style="min-height:100px;resize:none;" class="form-control" name="description" placeholder="Description">{{ meta('user_metas', 'user_id', $user->id, 'description') }}</textarea>
            	              </div>
            	            </div>
            	            <div class="form-group">
            	              <div class="col-sm-6">
            	                <input type="text" class="form-control" value="{{ $user->phone }}" name="phone" placeholder="Phone">
            	              </div>

            	              <div class="col-sm-6">
            	              	<div class="input-group date">
            	              	  <div class="input-group-addon">
            	              	    <i class="fa fa-calendar" style="position: inherit;"></i>
            	              	  </div>
            	              	  <input style="padding-left:5px;" type="text" value="{{ ($user->date_of_birth != '0000-00-00')?date('d F Y', strtotime($user->date_of_birth)):'' }}" name="date_of_birth" placeholder="Date of birth" class="form-control pull-right" id="dob">
            	              	</div>
            	              </div>
            	            </div>
            	            <div class="form-group">
            	              <div class="col-sm-3">
            	                <input type="text" class="form-control" value="{{ $user->suburb }}" name="suburb" placeholder="Suburb">
            	              </div>

            	              <div class="col-sm-3">
            	                <input type="text" class="form-control" value="{{ $user->postal }}" name="postal" placeholder="Postal">
            	              </div>

            	              <div class="col-sm-3">
            	                <input type="text" class="form-control" value="{{ $user->city }}" name="city" placeholder="City">
            	              </div>

            	              <div class="col-sm-3">
            	                <input type="text" class="form-control" value="{{ $user->state }}" name="state" placeholder="State">
            	              </div>
            	            </div>
            	            <div class="form-group">

            	              <div class="col-sm-12">
            	                <div id="google-location-holder" style="width: 100%; height: 400px;"></div>
            	              </div>
            	            </div>
            	            <div class="form-group">
                            <div class="col-sm-8">
                              <input type="text" class="form-control" name="map-address" placeholder="Map Address">
                            </div>
            	                <div class="col-sm-4" style="padding-top:15px;">
                              <div class="checkbox icheck">
                              <label>
                                  <input type="checkbox" name="is_public" {{ ($user->is_public == 'Yes')?' checked':'' }} value="Yes" /> &nbsp; Public Profile
                              </label>
                              </div>
                            </div>
            	            </div>
                          <div class="form-group">

                            <div class="col-sm-4">
                              <input type="text" class="form-control" name="longitude" placeholder="Longitude">
                            </div>

                            <div class="col-sm-4">
                              <input type="text" class="form-control" name="latitude" placeholder="Latitude">
                            </div>

                            <div class="col-sm-4">
                              <input type="text" class="form-control" value="{{ $user->training_area_radius }}" name="training_area_radius" placeholder="Trainer radious in Miles. Eg. 2.5" />
                            </div>
                            <label class="col-sm-12" for="profile_photo" style="cursor:pointer;">Choose new profile picture.</label>

                              <input type="file" style="display:none;" onchange="readURL(this);" class="form-control" accept=".gif,.png,.jpg,.jpeg,.bmp,.svg" name="profile_photo" id="profile_photo" placeholder="Profile Image">
                              
                          </div>
            	            <?php
            	            $user_photo = meta('user_metas', 'user_id', $user->id, 'profile_photo');
            	            if($user_photo!=""):
            	            ?>
	            	        <div class="form-group">
	        	            	<img style="max-width:200px;" class="img-responsive col-sm-12" id="image" src="{{ ($user_photo!="")?asset('/uploads/profile/'.$user_photo):"" }}" alt="Picture">
	        	            </div>
	            	        <?php endif; ?>
                          <div class="form-group">

                            <div class="col-sm-4">
                              <select name="payment_mode_id" id="payment_mode_id" class="form-control">
                              <option value="">-- Choose payment type --</option>
                               @foreach($payment_modes as $mode)
                               <option {{ ($mode->id == $user->payment_mode_id)?' selected':'' }} value="{{ $mode->id }}">{{ $mode->title }}</option>
                               @endforeach
                              </select>
                            </div>

                            <div class="col-sm-8">
                              <input type="text" class="form-control" value="{{ $user->budget }}" name="budget" placeholder="Budget (We will find best trainer based on your budget). Eg. 150">
                            </div>
                              
                          </div>
                          <!-- <div class="form-group">
                            <div class="col-sm-12">
                              <textarea style="min-height:100px;resize:none;" class="form-control" name="requirement" placeholder="What type of {{ ($user->role_id == 2)?'customer':'trainer' }} do you want?">{{ $user->requirement }}</textarea>
                            </div>
                          </div> -->
            	            <div class="form-group">

            		              <div class="col-sm-6">
            		                <input type="password" class="form-control" name="password" placeholder="New password">
            		              </div>

            		              <div class="col-sm-6">
            		                <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm new password">
            		              </div>
            	            </div>
            	            <div class="form-group">
            	              <div class="col-sm-12">
            	                <button type="submit" class="btn btn-block signup-btn">Submit</button>
            	              </div>
            	            </div>
         </div>
         </div>
      </div>

      </form>
   </div>
</div>
@endsection

@section('css')
<!-- bootstrap datepicker -->
<link href="{{ asset('/plugins/datepicker/datepicker3.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/plugins/cropper/cropper.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/plugins/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/plugins/iCheck/square/red.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/plugins/tag-it/css/jquery.tagit.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/plugins/tag-it/css/tagit.ui-zendesk.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('script')
<script type="text/javascript" src='//maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyCZwwE56KfH-wR9XM0PqNcyx9pmW75gF-g&sensor=false'></script>
<script src="{{ asset('/plugins/locationpicker.jquery.min.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="/plugins/cropper/cropper.min.js"></script>
<script src="{{ asset('/plugins/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('/plugins/tag-it/js/tag-it.js') }}" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
  $("#awards, #certificates, #specifications").tagit({
    allowSpaces: true
  });
  $('input').iCheck({
      checkboxClass: 'icheckbox_square-red',
      radioClass: 'iradio_square-red',
      increaseArea: '10%' // optional
  });
});
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#image').attr('src', e.target.result);
            start_resize();
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function start_resize() {
	return true;
	$(".cropper-container").remove();
	var $image = $('#image');

	$image.cropper({
	  aspectRatio: 1,
	  viewMode: 1,	  
	  dragMode: 'move',
	  // autoCropArea: 0.65,
	  restore: false,
	  guides: false,
	  highlight: false,
	  cropBoxMovable: true,
	  cropBoxResizable: false,
	  built: function () {
	    croppable = true;
	  }
	});
}

jQuery(document).ready(function($) {
	start_resize();
   $('#edit-frm').bootstrapValidator({
       /*feedbackIcons: {
           valid: 'glyphicon glyphicon-ok',
           invalid: 'glyphicon glyphicon-remove',
           validating: 'glyphicon glyphicon-refresh'
       },*/
       fields: {
           name: {
               validators: {
                  notEmpty: {
                      message: 'The name is required.'
                  }
              }
           },
           email: {
               validators: {
                  notEmpty: {
                      message: 'The email is required.'
                  },
                  emailAddress: {
                      message: 'The email is not a valid.'
                  }
              }
           },
           password: {
               validators: {
                  stringLength: {
                      min: 5,
                      max: 14,
                      message: 'The password must be between 5 and 14 characters.'
                  }
              }
           },
           password_confirmation: {               
               validators: {
                   identical: {
                       field: 'password',
                       message: 'The confirmation password do not match.'
                   }
               }
           },
           date_of_birth: {           
               validators: {
                   notEmpty: {
                       message: 'The date of field field is required.'
                   }
               }
           }
       }
   });


  $('#dob').datepicker({
    autoclose: true,
    format: 'dd MM yyyy'
  });

	@if($user->latitude != "" && $user->longitude != "")
		searchLocation({{ $user->latitude }}, {{ $user->longitude }});
	@else
		getLocation();
	@endif
	function getLocation() {
	    if (navigator.geolocation) {
	        navigator.geolocation.getCurrentPosition(showPosition, positionNotAllowed);
	    } else {
	    	searchLocation(43.6532, -79.3832);
	        x.innerHTML = "Geolocation is not supported by this browser.";
	    }
	}
	function showPosition(position) {
	    lati = position.coords.latitude;
	    longiti = position.coords.longitude;
	    searchLocation(lati, longiti);
	}
	function positionNotAllowed() {
		searchLocation(43.6532, -79.3832);
	}
	function searchLocation(cLtitude, cLongitude) {
		$('#google-location-holder').locationpicker({
			location: {latitude: cLtitude, longitude: cLongitude},
			radius: false,
			inputBinding: {
				latitudeInput: $('input[name=latitude]'),
				longitudeInput: $('input[name=longitude]'),
				radiusInput: $('#us3-radius'),
				locationNameInput: $('input[name=map-address]')    
		    }
		});
	}
});
</script>
@endsection