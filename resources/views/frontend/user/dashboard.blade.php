@extends('frontend.layouts.template-dashboard')

@section('left')
<div class="box box-primary dashboard-left">
	<div class="box-header">
		<h3 class="box-title"><i class="fa fa-envelope" aria-hidden="true"></i> Dashboard</h3>
	</div>
	<div class="box-body">
	<ul>
		@if(Auth::user()->role_id == 2)
		<li><a href="{{ url('contracts') }}"><i class="fa fa-user" aria-hidden="true"></i> My Customers</a></li>
		@else
		<li><a href="{{ url('contract/my-trainer') }}"><i class="fa fa-user" aria-hidden="true"></i> My Trainer</a></li>
		@endif
		<li><a href="{{ url('contract/history') }}"><i class="fa fa-history" aria-hidden="true"></i> History</a></li>
	</ul>
	</div>
</div>
@endsection

@section('right')
	@if(!$users->isEmpty())
		@foreach($users as $user)
		@if($user->role_id == 2)
			<?php
				$profile_link = url('coach/'.$user->display);
			?>
		@else

			<?php
				$profile_link = url('profile/'.$user->id);
			?>

		@endif
		<div class="row dashboard-single-user">
			<div class="col-md-8">
				<?php
				$desc = meta('user_metas', 'user_id', $user->id, 'description');
				$specific = meta('user_metas', 'user_id', $user->id, 'specifications');
				?>
				<p class="description">{{ substr($desc, 0, 375) }} <?php echo (strlen($desc) > 375)?"<a href='".$profile_link. "'>... more</a>":""; ?></p>
				@if($specific)
					<p><i class="fa fa-fire" aria-hidden="true"></i> {{ $specific }}</p>
				@endif
			</div>
			<div class="col-md-4">		
				<h4>{{ $user->name }}</h4>
				<?php 
				$user_photo = meta('user_metas', 'user_id', $user->id, 'profile_photo');
				?>
				<div class="col-md-12">
					<img class="img-responsive user-image img-circle" src="{{ ($user_photo!="")?asset('/uploads/profile/'.$user_photo):"img/avatar.jpg" }}" alt="Picture">
				</div>
				<div class="col-md-12">
					<div class="col-md-6">
						<button title="View Profile" onclick="location.href='{{ $profile_link }}';" class="btn btn-info btn-xs dashboard-right-btn profile pull-right"><i class="fa fa-user" aria-hidden="true"></i> &nbsp;Profile</button>
					</div>
					<div class="col-md-6">
						<button title="Message" onclick="location.href='{{ url('message/compose/'.$user->id) }}';" class="btn btn-info btn-xs dashboard-right-btn message pull-left"><i class="fa fa-envelope" aria-hidden="true"></i> &nbsp;Message</button>
					</div>
				</div>
			</div>
		</div>
		@endforeach
	@else
	<h5 align="center">{{ (Auth::user()->role_id == 3)?'Trainer':'Customer' }} not found!</h5>
	@endif
@endsection

@section('htmlheader_title')
Dashboard
@endsection