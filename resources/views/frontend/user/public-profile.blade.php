@extends('frontend.layouts.template')
@section('content')
<div class="profile-container">
   <div class="container">
   <form class="form-horizontal" id="edit-frm" method="post" action="{{ url('user/updateProfileAction') }}" enctype="multipart/form-data">
      <div class="row">
         <div class="col-md-4 profile-left">
            <!-- Profile Image -->
            <div class="box box-primary">
               <div class="box-body box-profile">
                  <?php
                     $user_photo = meta('user_metas', 'user_id', $user->id, 'profile_photo');
                     $user_photo = ($user_photo!="")?"uploads/profile/".$user_photo:"img/avatar.jpg";
                     ?>
                  <div class="col-md-6 text-left pull-left"><i class="fa fa-usd" aria-hidden="true"></i><b>{{ $user->budget }}</b> /{{ $user->payment_mode->title }}</div>
                  @if($user->role_id == 2)
                  <div class="col-md-6 text-right pull-right"><a href="{{ url('contract/compose/'.$user->id) }}" style="padding:0 5px;" class="btn btn-info btn-sm">Hire</a></div>
                  @endif
                  <img class="profile-user-img img-responsive img-circle" src="{{ asset($user_photo) }}" alt="User profile picture">
                  <h3 class="profile-username text-center">{{ $user->name }}</h3>
                  <p class="text-muted text-center">{{ $user->role->title }}</p>
               </div>
               <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <!-- About Me Box -->
            <div class="box box-primary">
               <!-- /.box-header -->
               <div class="box-body">
               @if($user->role_id == 3)
                  <strong><i class="fa fa-book margin-r-5"></i> Description</strong>
                  <p class="text-muted">
                     {{ meta('user_metas', 'user_id', $user->id, 'description') }}
                  </p>
                  <hr>
              @endif
                  <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                  <p class="text-muted">{{ $user->address }}</p>
                  <hr>
                  @if($user->role_id == 2)
                    <strong><i class="fa fa-fire" aria-hidden="true"></i> Specifications</strong>
                      <input type="text" readonly="readonly" name="specifications" value="{{ meta('user_metas', 'user_id', $user->id, 'specifications') }}" id="specifications" class="form-control" />
                    <hr>
                    <strong><i class="fa fa-trophy margin-r-5"></i> Awards</strong>
                      <input type="text" readonly="readonly" name="awards" value="{{ meta('user_metas', 'user_id', $user->id, 'awards') }}" id="awards" class="form-control" />
                    <hr>
                    <strong><i class="fa fa-calendar margin-r-5"></i> Training started year</strong>
                      <input type="text" readonly="readonly" name="training_start" placeholder="Eg. 2001" value="{{ meta('user_metas', 'user_id', $user->id, 'training_start') }}" id="training_start" class="form-control" />
                    <hr>
                    <strong><i class="fa fa-certificate margin-r-5"></i> Certificates</strong>
                      <input type="text" readonly="readonly" name="certificates" value="{{ meta('user_metas', 'user_id', $user->id, 'certificates') }}" id="certificates" class="form-control" />
                    <hr>
                    <div class="text-center" style="margin:0 auto 10px;">
                      <div class="fb-share-button" data-href="{{ url('coach/'.$user->display) }}" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ url('coach/'.$user->display) }}&amp;src=sdkpreparse">Share</a></div>
                    </div>
                  @endif
               </div>
               <!-- /.box-body -->
            </div>
            <!-- /.box -->
         </div>
         <div class="col-md-8">
             <div class="col-md-12 body-right">

                 <div class="form-group">

                   <div class="col-sm-12">
                     <div class="map-holder">
                         <div id="google-location-holder" style="width: 100%; height: 400px;"></div>
                     </div>
                   </div>
                   <div class="col-sm-12 text-center">
                     <strong>Available within {{ $user->training_area_radius }} KM</strong>
                   </div>
                 </div>
               <div class="content-box">
                     <div class="form-group">
                          <div class="col-sm-12">
                             <div class="text-box">
                                 <i class="fa fa-book margin-r-5" aria-hidden="true"></i>
                                <p>{{ meta('user_metas', 'user_id', $user->id, 'description') }}</p>
                             </div>
                          </div>
                        </div>
                 </div>
             </div>
         </div>
      </div>

      </form>
   </div>
</div>
@endsection

@section('meta_title', $user->name)
@section('ti', asset($user_photo))
@section('meta_description', meta('user_metas', 'user_id', $user->id, 'description'))
@section('page_type', 'profile')
@section('fi', asset($user_photo))

@section('css')
<!-- bootstrap datepicker -->
<link href="{{ asset('/plugins/datepicker/datepicker3.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/plugins/cropper/cropper.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/plugins/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/plugins/iCheck/square/red.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/plugins/tag-it/css/jquery.tagit.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/plugins/tag-it/css/tagit.ui-zendesk.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('script')
<script type="text/javascript" src='//maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyCZwwE56KfH-wR9XM0PqNcyx9pmW75gF-g&sensor=false'></script>
<script src="{{ asset('/plugins/locationpicker.jquery.min.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="/plugins/cropper/cropper.min.js"></script>
<script src="{{ asset('/plugins/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('/plugins/tag-it/js/tag-it.js') }}" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
  // $("#awards, #certificates, #specifications").tagit({
  //   allowSpaces: true
  // });
  $('input').iCheck({
      checkboxClass: 'icheckbox_square-red',
      radioClass: 'iradio_square-red',
      increaseArea: '10%' // optional
  });
});
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#image').attr('src', e.target.result);
            start_resize();
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function start_resize() {
	return true;
	$(".cropper-container").remove();
	var $image = $('#image');

	$image.cropper({
	  aspectRatio: 1,
	  viewMode: 1,	  
	  dragMode: 'move',
	  // autoCropArea: 0.65,
	  restore: false,
	  guides: false,
	  highlight: false,
	  cropBoxMovable: true,
	  cropBoxResizable: false,
	  built: function () {
	    croppable = true;
	  }
	});
}

jQuery(document).ready(function($) {
	start_resize();
   $('#edit-frm').bootstrapValidator({
       /*feedbackIcons: {
           valid: 'glyphicon glyphicon-ok',
           invalid: 'glyphicon glyphicon-remove',
           validating: 'glyphicon glyphicon-refresh'
       },*/
       fields: {
           name: {
               validators: {
                  notEmpty: {
                      message: 'The name is required.'
                  }
              }
           },
           email: {
               validators: {
                  notEmpty: {
                      message: 'The email is required.'
                  },
                  emailAddress: {
                      message: 'The email is not a valid.'
                  }
              }
           },
           password: {
               validators: {
                  stringLength: {
                      min: 5,
                      max: 14,
                      message: 'The password must be between 5 and 14 characters.'
                  }
              }
           },
           password_confirmation: {               
               validators: {
                   identical: {
                       field: 'password',
                       message: 'The confirmation password do not match.'
                   }
               }
           },
           date_of_birth: {           
               validators: {
                   notEmpty: {
                       message: 'The date of field field is required.'
                   }
               }
           }
       }
   });


  $('#dob').datepicker({
    autoclose: true,
    format: 'dd MM yyyy'
  });

	@if($user->latitude != "" && $user->longitude != "")
		searchLocation({{ $user->latitude }}, {{ $user->longitude }});
	@else
		getLocation();
	@endif
	function getLocation() {
	    if (navigator.geolocation) {
	        navigator.geolocation.getCurrentPosition(showPosition, positionNotAllowed);
	    } else {
	    	searchLocation(43.6532, -79.3832);
	        x.innerHTML = "Geolocation is not supported by this browser.";
	    }
	}
	function showPosition(position) {
	    lati = position.coords.latitude;
	    longiti = position.coords.longitude;
	    searchLocation(lati, longiti);
	}
	function positionNotAllowed() {
		searchLocation(43.6532, -79.3832);
	}
	function searchLocation(cLtitude, cLongitude) {
		$('#google-location-holder').locationpicker({
			location: {latitude: cLtitude, longitude: cLongitude},
			radius: false,
			inputBinding: {
				latitudeInput: $('input[name=latitude]'),
				longitudeInput: $('input[name=longitude]'),
				radiusInput: $('#us3-radius'),
				locationNameInput: $('input[name=map-address]')    
		    }
		});
	}
});
</script>
@endsection