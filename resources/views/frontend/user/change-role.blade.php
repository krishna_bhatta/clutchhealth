@extends('frontend.layouts.template')
@section('content')
<div class="profile-container">
   <div class="container">
   <form class="form-horizontal" id="edit-frm" method="post" action="{{ url('change-role-action') }}">
     <input type="hidden" name="_token" value="{{ csrf_token() }}" />
     <div class="col-md-3"></div>
     <div class="col-md-6">
     	<h4 align="center">Choose role</h4>
     	<div class="form-group">
     	  <div class="col-md-8">
     	  	<select name="role" id="role" class="form-control">
     	  	  <option value="2">Trainer</option>
     	  	  <option value="3" {{ (Auth::user()->role_id == 3)?'selected':'' }}>Customer</option>
     	  	</select>
     	  </div>
     	  <div class="col-md-4">
     	  	<input type="submit" class="form-control btn btn-success" value="Submit" />
     	  </div>
     	</div>
     </div>
     <div class="col-md-3"></div>
   </form>
   </div>
</div>
@endsection