@extends('frontend.layouts.template')

@section('content')
            <header class="heading text-center">
               <h1>A Platform to build your brand</h1>
            </header>
            <main id="main" role="main">
               <div class="container">
                  <div class="article-holder same-height">
                     <div class="col-holder">
                        <div class="col text-box height">
                           <div class="text-wrap">
                              <h2>"Tell Us Your Strengths, <span class="text-right">We'll Do The Rest"</span></h2>
                              <p>Whether you're a basketball coach or a fitness instructor, we'll find you pupils that have been looking for you all along.</p>
                           </div>
                        </div>
                        <div class="img-holder col height">
                           <img src="{{ asset('img/img03.jpg') }}" alt="Tell Us Your Strengths, We'll Do The Rest" width="493" height="266">
                        </div>
                     </div>
                     <div class="col-holder">
                        <div class="col text-box height">
                           <div class="text-wrap">
                              <h2>featured excellence</h2>
                              <p>Get back-to-back Five-Star Ratings from at least five students, we'll feature your profile and customer reviews on our home page.</p>
                           </div>
                        </div>
                        <div class="img-holder col height">
                           <img src="{{ asset('img/img04.jpg') }}" alt="featured excellence" width="493" height="266">
                        </div>
                     </div>
                     <div class="col-holder">
                        <div class="col text-box height">
                           <div class="text-wrap">
                              <h2>"share your experience, <span class="text-right">earn rewards"</span></h2>
                              <p>The more you teach your craft and share on Social Media, the more we'll reward you. Just add #ClutchHealth to your posts, and watch the Clutch Points pile up.</p>
                           </div>
                        </div>
                        <div class="img-holder col height">
                           <img src="{{ asset('img/img05.jpg') }}" alt="share your experience earn rewards" width="493" height="266">
                        </div>
                     </div>
                     <div class="col-holder">
                        <div class="col text-box height">
                           <div class="text-wrap">
                              <h2>earn instantly</h2>
                              <p>After your weekly sessions you'll receive automatic deposits in to your account. We'll help you keep track of your progress, and offer advice along the way. </p>
                           </div>
                        </div>
                        <div class="img-holder col height">
                           <img src="{{ asset('img/img06.jpg') }}" alt="earn instantly" width="493" height="266">
                        </div>
                     </div>
                  </div>
               </div>
            </main>
@endsection

@section('meta_title', 'Coach in Toronto')
@section('meta_description', 'We provide Coaches with a platform to build your brand. Tell us your strengths, and we\'ll market to the pupils that have been looking for you all along.')