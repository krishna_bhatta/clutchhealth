@extends('frontend.layouts.template')

@section('content')
<div class="container" id="platform-features">
   <h3>{{ $blog->title }}</h3>
   <div class="article-box">
      {!! $blog->details !!}
   </div>
</div>
@endsection


<?php
$fi = meta('blog_metas', 'blog_id', $blog->id, 'fb_image');
$ti = meta('blog_metas', 'blog_id', $blog->id, 'twitter_image');
?>


@section('meta_title', meta('blog_metas', 'blog_id', $blog->id, 'meta_title'))
@section('meta_description', meta('blog_metas', 'blog_id', $blog->id, 'meta_description'))
@if($fi != "")
	@section('fi', asset('uploads/media/'.$fi))
@endif
@if($ti != "")
	@section('ti', asset('uploads/media/'.$ti))
@endif