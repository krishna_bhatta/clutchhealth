@extends('frontend.layouts.template')

@section('content')
<div class="container blogs-container" id="platform-features">
	@if($blogs)
		@foreach($blogs as $blog)
			<div class="item">
				<a href="{{ url('blog/'.$blog->alias) }}">
					@if($blog->featured_image != "")
				  		<img src="{{ asset('uploads/media/'.$blog->featured_image) }}" alt="{{ $blog->title }}" width="375" height="195">
				  	@else
				  		<img src="{{ asset('uploads/media/journey-to-health-exercise-food-stronger-mindset-594e71fe3770820b975f9fb9abecd5f3.jpg') }}" alt="{{ $blog->title }}" width="375" height="195">
				  	@endif
				</a>
			  
			  <div class="item-overlay top"></div>
			</div>
			<h3><a href="{{ url('blog/'.$blog->alias) }}">{{ $blog->title }}</a></h3>
			<p>
				{{ $blog->description }}
			</p>
			<p align="right"><a href="{{ url('blog/'.$blog->alias) }}">Read more...</a></p>
			<hr />
		@endforeach
	@else
		<h5 align="center">Blog not available!</h5>
	@endif
</div>
@endsection

@section('css')

<style>
	.footer {
		

		min-height: 76px;
		position: fixed;
		margin-top: 80px;
		bottom: 0;
		left: 0;
		right: 0;
		clear:both;
	}
</style>
@endsection
@section('meta_title', 'Blog Posts')
@section('meta_description', 'Read inspiring stories. We take you through a journey of health, exercise, food and building a stronger mindset.')