@extends('frontend.layouts.template-dashboard')

@section('left')
<div class="box box-primary dashboard-left">
   <div class="box-header">
      <h3 class="box-title"><i class="fa fa-envelope" aria-hidden="true"></i> Filter Trainer</h3>
   </div>
   <div class="box-body text-center">
   <form method="post" action="{{ url('find-trainer-filter') }}">
   <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <ul>
         <li><input type="text" name="name" value="{{ $search['name'] }}" class="form-control" placeholder="Name" /></li>
         <li><select name="payment_type" class="form-control">
            <option value="">Payment mode</option>
            @foreach($payments as $payment)
            <option {{ ($payment->id == $search['payment_type'])?'selected':'' }} value="{{ $payment->id }}">{{ $payment->title }}</option>
            @endforeach
         </select></li>
         <li><input type="text" name="address" value="{{ $search['address'] }}" class="form-control" placeholder="Address" /></li>
      </ul>
      <input type="submit" value="Search" class="btn btn-success submit-filter" />
   </form>
   </div>
</div>
@endsection

@section('right')
   @if(!$users->isEmpty())
      @foreach($users as $user)
	    <?php
	    $desc = meta('user_metas', 'user_id', $user->id, 'description');
	    $specific = meta('user_metas', 'user_id', $user->id, 'specifications');
	    ?>
	    @if($desc != "")
	      <div class="row dashboard-single-user">
	         <div class="col-md-8">
	            <p class="description">{{ substr($desc, 0, 375) }} <?php echo (strlen($desc) > 375)?"<a href='".url('profile/'.$user->id). "'>... more</a>":""; ?></p>
	            @if($specific)
	               <p><i class="fa fa-fire" aria-hidden="true"></i> {{ $specific }}</p>
	            @endif
	         </div>
	         <div class="col-md-4">     
	            <h4>{{ $user->name }}</h4>
	            <?php 
	            $user_photo = meta('user_metas', 'user_id', $user->id, 'profile_photo');
	            ?>
	            <div class="col-md-12">
	               <img class="img-responsive user-image img-circle" src="{{ ($user_photo!="")?asset('/uploads/profile/'.$user_photo):"img/avatar.jpg" }}" alt="Picture">
	            </div>
	            <div class="col-md-12">
	               <div class="col-md-6">
	                  <button title="View Profile" onclick="location.href='{{ url('profile/'.$user->id) }}';" class="btn btn-info btn-xs dashboard-right-btn profile pull-right"><i class="fa fa-user" aria-hidden="true"></i> &nbsp;Profile</button>
	               </div>
	               <div class="col-md-6">
	                  <button title="Message" onclick="location.href='{{ url('message/compose/'.$user->id) }}';" class="btn btn-info btn-xs dashboard-right-btn message pull-left"><i class="fa fa-envelope" aria-hidden="true"></i> &nbsp;Message</button>
	               </div>
	            </div>
	         </div>
	      </div>
      	@endif
      @endforeach
   @else
   <h5 align="center">Trainer not found!</h5>
   @endif
@endsection


@section('meta_title', 'Find your favourite trainer in Toronto')
@section('meta_description', 'We want to help you achieve Clutch status when it matters most. Sign up and start exercising with your favourite coach in Toronto.')