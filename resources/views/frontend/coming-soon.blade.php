<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
   <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>ClutchHealth</title>

    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Responsive View Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Animate.CSS -->
    <link rel="stylesheet" href="{{ asset('coming-soon-assets/css/animate.css') }}">

    <!-- Font Aewsome Style -->
    <link rel="stylesheet" href="{{ asset('coming-soon-assets/css/font-awesome.min.css') }}">

    <!-- Bootstrap Style -->
    <link rel="stylesheet" href="{{ asset('coming-soon-assets/css/bootstrap.min.css') }}">

    <!-- Reset Style -->
    <link rel="stylesheet" href="{{ asset('coming-soon-assets/css/normalize.css') }}">

    <!-- Main Style -->
    <link rel="stylesheet" href="{{ asset('coming-soon-assets/css/main.css') }}">
    
    <!-- Color Switcher Style -->
    <link rel="stylesheet" href="{{ asset('coming-soon-assets/switcher/switcher.css') }}">
    <!-- alternate style start || to use your preferred color, simply remove all style colors below and leave only your preferred color -->   
    <link rel="stylesheet" href="{{ asset('coming-soon-assets/css/colors/red.css') }}">

    <!--[if lt IE 9]>
   <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
   <![endif]-->

    <script src="{{ asset('coming-soon-assets/js/vendor/modernizr-2.8.3.min.js') }}"></script>
</head>
<body>
<!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<!-- screen loader start -->
<div class="screen-loader"></div><!-- screen loader end -->
<!-- preload start -->
<div id="preload">
    <!-- preload status start -->
    <div id="preload-status"></div><!-- preload status end -->
</div><!-- preload end -->

<!-- === Header Section === -->
<div id="container">
   <!-- +++ Intro Section ++++ -->
    <section class="intro">
      <!-- Intro Section Background-image -->
      <div class="intro-image"></div>
      
      <!-- ====+++ Intro Section Content +++==== -->
      <div class="center-content">
         

         <div class="intro-content text-center">   
            <!-- Logo -->
            <div class="logo">
               <h2 class="text-logo">
               <a href="{{ url('/') }}"><img src="{{ asset('img/logo.png') }}" /></a>
               </h2>
            </div><!-- /End logo -->                     
            <!-- ==== Main Heading ==== -->                                   
            <h3 class="intro-title">This feature is in under development</h3>

            <!-- Spin Clock -->
            <i class="fa fa-clock-o fa-spin fa-4x fa-clock-spin"></i>
                  
            <!-- ====+++ Countdown Timer +++==== -->                 
            <ul class="countdown">
                 <li>
                     <span class="days">00</span>
                     <p class="days-ref">days</p>
                 </li>
                 <li class="seperator"><i class="fa fa-clock-o"></i><br><i class="fa fa-clock-o"></i></li>
                 <li>
                     <span class="hours">00</span>
                     <p class="hours-ref">hours</p>
                 </li>
                 <li class="seperator"><i class="fa fa-clock-o"></i><br><i class="fa fa-clock-o"></i></li>
                 <li>
                     <span class="minutes">00</span>
                     <p class="minutes-ref">minutes</p>
                 </li>
                 <li class="seperator"><i class="fa fa-clock-o"></i><br><i class="fa fa-clock-o"></i></li>
                 <li>
                     <span class="seconds">00</span>
                     <p class="seconds-ref">seconds</p>
                 </li>
             </ul>                     
            
            <!-- ==== Intro Section Sub-Heading ==== -->          
            <div class="intro-subtitle">
               <p>We are currently Developing Something Awesome.</p>
            </div>                     
                                 
         </div> <!-- /End Intro Section Content -->
      </div>      
   </section><!-- /end intro section -->

   <!-- +++ Trigger After Content +++ -->
   <section class="content-wrapper">

        <!-- ==== Footer Section ==== -->
        <footer id="footer" class="content-div">
         <div class="container">
            <div class="row">
               <div class="col-sm-12">
                  <div class="copyright">
                     <!-- Copyright Text -->
                     <p class="text-center">Copyright &copy; 2016. All Rights Reserved ClutchHealth</p>
                  </div>
               </div>
            </div>
         </div>               
        </footer> <!-- /End Footer Section -->
   </section> <!-- /End Trigger Content -->
</div> 

<!-- jQuery Library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
<!-- Modernizer -->
<script src="{{ asset('coming-soon-assets/js/modernizr.custom.js') }}"></script>
<!-- Bootstrap JS -->
<script src="{{ asset('coming-soon-assets/js/bootstrap.min.js') }}"></script>
<!-- Classie JS -->
<script src="{{ asset('coming-soon-assets/js/classie.js') }}"></script>       
<!-- Countdown JS -->
<script src="{{ asset('coming-soon-assets/js/jquery.downCount.js') }}"></script>
<!-- Mailchimp JS Plugin -->
<script src="{{ asset('coming-soon-assets/js/jquery.ajaxchimp.min.js') }}"></script>
<!-- Contact Form JS -->
<script src="{{ asset('coming-soon-assets/js/form-contact.js') }}"></script>
<!-- Goole map API -->
<script src="https://maps.google.com/maps/api/js?sensor=true"></script>
<!-- Google map js -->
<script src="{{ asset('coming-soon-assets/js/gmaps.js') }}"></script>
<!-- Nice Scroll JS -->
<script src="{{ asset('coming-soon-assets/js/jquery.nicescroll.3.5.4.js') }}"></script>
<!-- Style Switcher JS -->
<script src="{{ asset('coming-soon-assets/switcher/switcher.js') }}"></script>
<!-- Plugin Initialization js -->
<script src="{{ asset('coming-soon-assets/js/plugins.js') }}"></script>
<!-- Custom JS -->
<script src="{{ asset('coming-soon-assets/js/main.js') }}"></script>
</body>
</html>