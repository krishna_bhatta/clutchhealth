@extends('frontend.layouts.template')

@section('content')
      <div class="top-banner">
               <div class="col img-holder" style="background:url({{ asset('img/img07.jpg') }}) no-repeat 50% 50% / cover;">
               <img src="images/img07.jpg" class="visible-xs" alt="image description" width="1600" height="600">
               </div>
               <div class="col text-box">
                  <div class="text-content">
                     <h2>START EXERCISING</h2>
                     <p>Always the workout that you want The best way to gain confidence</p>
                  </div>
               </div>
            </div>
            <main id="main" role="main">
               <div class="container">
                  <div class="article-holder same-height">
                     <div class="col-holder">
                        <div class="col text-box height">
                           <div class="text-wrap">
                              <h2>"hand pick your own coach"</span></h2>
                              <p>Whether you're a beginner looking to learn the fundamentals or an aspiring athlete, Clutch Health will match you with the best Coach that is prepared to help you reach your goals. </p>
                           </div>
                        </div>
                        <div class="img-holder col height">
                           <img src="{{ asset('img/img08.jpg') }}" alt="hand pick your own coach" width="550" height="300">
                        </div>
                     </div>
                     <div class="col-holder">
                        <div class="col text-box height">
                           <div class="text-wrap">
                              <h2>"Earn clutch points as you go"</h2>
                              <p>Every good deed deserves a reward. The more you exercise with Clutch Health, the more Clutch Points you earn.</p>
                           </div>
                        </div>
                        <div class="img-holder col height">
                           <img src="{{ asset('img/img09.jpg') }}" alt="Earn clutch points as you go" width="493" height="266">
                        </div>
                     </div>
                     <div class="col-holder">
                        <div class="col text-box height">
                           <div class="text-wrap">
                              <h2>5-Star rating system</h2>
                              <p>After your workout, rate your fitness professional and provide anonymous feedback on your workout. Your input helps our Coaches continuously improve their craft. </p>
                           </div>
                        </div>
                        <div class="img-holder col height">
                           <img src="{{ asset('img/img04.jpg') }}" alt="5-Star rating system" width="493" height="266">
                        </div>
                     </div>
                  </div>
               </div>
            </main>
@endsection

@section('meta_title', 'Exercise in Toronto')
@section('meta_description', 'Hand pick your own Coach, earn Clutch Points as you go and get a free session on us! Rate your trainer with our 5-Star Rating System.')