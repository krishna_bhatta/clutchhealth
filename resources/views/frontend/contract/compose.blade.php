@extends('frontend.layouts.template-dashboard')

@section('left')
<div class="box box-primary dashboard-left">
	<div class="box-header">
		<h3 class="box-title"><i class="fa fa-envelope" aria-hidden="true"></i> Dashboard</h3>
	</div>
	<div class="box-body">
	<ul>
		@if(Auth::user()->role_id == 2)
		<li><a href="{{ url('contracts') }}"><i class="fa fa-user" aria-hidden="true"></i> My Customers</a></li>
		@else
		<li><a href="{{ url('contract/my-trainer') }}"><i class="fa fa-user" aria-hidden="true"></i> My Trainer</a></li>
		@endif
		<li><a href="{{ url('contract/history') }}"><i class="fa fa-history" aria-hidden="true"></i> History</a></li>
	</ul>
	</div>
</div>
@endsection

@section('right')
<h3>Start contract with {{ $trainer->name }}</h3>
<form class="form-horizontal" id="edit-frm" method="post" action="{{ url('contract/submitContract') }}" enctype="multipart/form-data">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="hidden" name="trainer_id" value="{{ $trainer->id }}" />
<div class="form-group">
	<div class="col-sm-12">
	  <input type="text" class="form-control" value="{{ old('title') }}" name="title" placeholder="Contract title" />
	</div>
</div>
<div class="form-group">
	<div class="col-sm-6">
	  <input type="text" class="form-control" value="{{ old('start_date') }}" name="start_date" placeholder="Training start date" id="start_date" />
	</div>
	<div class="col-sm-6">
	  <input type="text" class="form-control" value="{{ old('end_date') }}" name="end_date" placeholder="Training end date" id="end_date" />
	</div>
</div>
<div class="form-group">
  <div class="col-sm-12">
    <textarea style="min-height:200px;" class="form-control" name="description" placeholder="Contract details">{{ old('description') }}</textarea>
  </div>
</div>
<div class="form-group">

<div class="col-sm-6">
  <select name="payment_mode_id" id="payment_mode_id" class="form-control">
  <option value="">-- Payment interval --</option>
   @foreach($payment_modes as $mode)
   <option {{ ($mode->id == old('payment_mode_id'))?' selected':'' }} value="{{ $mode->id }}">{{ $mode->title }}</option>
   @endforeach
  </select>
</div>

<div class="col-sm-6">
  <input type="text" class="form-control" name="budget" placeholder="Budget Eg. 150">
</div>
  
</div>
<button type="submit" class="btn btn-block signup-btn">Submit</button>
</form>
@endsection

@section('htmlheader_title')
Offer creation
@endsection

@section('css')
<link href="{{ asset('/plugins/datepicker/datepicker3.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('script')
<script src="/plugins/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('#start_date, #end_date').datepicker({
		  autoclose: true,
		  format: 'dd MM yyyy'
		});
	});
</script>
@endsection