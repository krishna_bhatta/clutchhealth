@extends('frontend.layouts.template-dashboard')

@section('left')
<div class="box box-primary dashboard-left">
	<div class="box-header">
		<h3 class="box-title"><i class="fa fa-envelope" aria-hidden="true"></i> Dashboard</h3>
	</div>
	<div class="box-body">
	<ul>
		@if(Auth::user()->role_id == 2)
		<li><a href="{{ url('contracts') }}"><i class="fa fa-user" aria-hidden="true"></i> My customers</a></li>
		@else
		<li><a href="{{ url('contract/my-trainer') }}"><i class="fa fa-user" aria-hidden="true"></i> My Trainer</a></li>
		@endif
		<li><a href="{{ url('contract/history') }}"><i class="fa fa-history" aria-hidden="true"></i> History</a></li>
	</ul>
	</div>
</div>
@endsection

@section('right')
<h3>Contract lists</h3>
@if($contracts)
<table width="100%" class="table">
	<tr>
		<th>SN</th>
		<th>Title</th>
		<th>Customer</th>
		<th>Start Date</th>
		<th>End Date</th>
		<th>Status</th>
		<th>Action</th>
	</tr>
	<?php $i=1; ?>
	@foreach($contracts as $contract)
	<tr>
		<td>{{ $i++ }}</td>
		<td>{{ $contract->title }}</td>
		<td>{{ $contract->customer->name }}</td>
		<td>{{ $contract->start_date }}</td>
		<td>{{ $contract->end_date }}</td>
		<td>{{ $contract->status }}</td>
		<td><a href="{{ url('contract/view/'.$contract->id) }}" class="btn btn-sm btn-info">View</a></td>
	</tr>
	@endforeach
</table>
@else
Contract not available
@endif

@endsection

@section('htmlheader_title')
Contract lists
@endsection

@section('css')
<link href="{{ asset('/plugins/datepicker/datepicker3.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('script')
<script src="/plugins/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('#start_date, #end_date').datepicker({
		  autoclose: true,
		  format: 'dd MM yyyy'
		});
	});
</script>
@endsection