@extends('frontend.layouts.template-dashboard')

@section('left')
<div class="box box-primary dashboard-left">
	<div class="box-header">
		<h3 class="box-title"><i class="fa fa-envelope" aria-hidden="true"></i> Dashboard</h3>
	</div>
	<div class="box-body">
	<ul>
		@if(Auth::user()->role_id == 2)
		<li><a href="{{ url('contracts') }}"><i class="fa fa-user" aria-hidden="true"></i> My Customers</a></li>
		@else
		<li><a href="{{ url('contract/my-trainer') }}"><i class="fa fa-user" aria-hidden="true"></i> My Trainer</a></li>
		@endif
		<li><a href="{{ url('contract/history') }}"><i class="fa fa-history" aria-hidden="true"></i> History</a></li>
	</ul>
	</div>
</div>
@endsection

@section('right')
<h3 id="contract-view-title">Contract {{ ($contract->status=='Completed' || $contract->status=='Active')?"between ".$contract->trainer->name." and ".$contract->customer->name:"offer from ".$contract->customer->name }}</h3>
<div class="form-group">
	<div class="col-sm-8">
	  <div class="form-control">{{ $contract->title }}</div>
	</div>
	<div class="col-sm-4">
		<span class="form-control">${{ $contract->budget }}/{{ $contract->payment_mode->title }}</span>
	</div>
</div>
<div class="form-group">
  <div class="col-sm-12">
    <span class="form-control">{{ $contract->description }}</span>
  </div>
</div>
<div class="form-group">
	<div class="col-sm-5">
	  <span class="form-control">{{ $contract->start_date }}</span>
	</div>
	<div class="col-sm-2">
		<span class="form-control">to</span>
	</div>
	<div class="col-sm-5">
	  <span class="form-control">{{ $contract->end_date }}</span>
	</div>
</div>
<br style="clear:both"/>
<div class="form-group">
	@if($contract->status == 'Pending' && $contract->trainer_id == Auth::user()->id)
	<div class="col-sm-4 text-right">
		<a href="{{ url('contract/accept/'.$contract->id) }}" class="btn btn-success">Accept</a>
	</div>
	<div class="col-sm-4 text-center">
		<a href="{{ url('contract/reject/'.$contract->id.'/rejected') }}" class="btn btn-danger">Reject</a>
	</div>
	<div class="col-sm-4 text-left">
		<a href="{{ url('contracts') }}" class="btn btn-info">Back</a>
	</div>
	@else
	<div class="col-sm-12 text-center">
		<a href="{{ url('contracts') }}" class="btn btn-info">Back</a>
	</div>
	@endif
</div>
<br style="clear:both"/>
@endsection

@section('htmlheader_title')
Offer creation
@endsection

@section('css')
<link href="{{ asset('/plugins/datepicker/datepicker3.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('script')
<script src="/plugins/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('#start_date, #end_date').datepicker({
		  autoclose: true,
		  format: 'dd MM yyyy'
		});
	});
</script>
@endsection