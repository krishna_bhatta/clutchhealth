@extends('layouts.app')

@section('htmlheader_title')
	Compose Message
@endsection

@section('contentheader_title')
	New Message
@endsection

@section('breadcrumb')
	<li><a href="{{ url('message') }}">Message</a></li><li>Compose Message</li>
@endsection

@section('main-content')
<div class="row">
	@include('admin.message.leftPanel')
  <div class="col-md-9">
    <div class="box box-primary">
      <form action="{{ url('admin/submitCompose') }}" method="post">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="box-header with-border">
        <h3 class="box-title">Compose New Message</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="form-group">
          <div class="row">
            <div class="col-md-12">
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="row">
            <div class="col-md-4">
              <select class="form-control" name="to">
                <option value="">-- Select User --</option>
                @foreach($users as $user)
                <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach
              </select>
            </div>
            <div class="col-md-8">
              <input class="form-control" name="subject" placeholder="Subject:">
            </div>
          </div>
        </div>
        <div class="form-group">
              <textarea id="compose-textarea" name="message" class="form-control" style="min-height: 300px"></textarea>
        </div>
        <!-- <div class="form-group">
          <div class="btn btn-default btn-file">
            <i class="fa fa-paperclip"></i> Attachment
            <input type="file" name="attachment">
          </div>
          <p class="help-block">Max. 5MB</p>
        </div> -->
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <div class="pull-right">
          <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send</button>
        </div>
      </div>
      <!-- /.box-footer -->
      </form>
    </div>
    <!-- /. box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
@endsection


@section('added-css')
<!-- bootstrap wysihtml5 - text editor -->
<link href="{{ asset('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('added-scripts')
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- Page Script -->
<script>
  $(function () {
    //Add text editor
    $("#compose-textarea").wysihtml5();
  });
</script>
@endsection