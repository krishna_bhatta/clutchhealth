@extends('frontend.layouts.email')

@section('title', $title)

@section('content')
<tr>
  <td class="h3">
    {{ $address }}
  </td>
</tr>
@if(isset($link))
<tr>
  <td style="padding: 20px 0 0 0;" align="center">
    <table class="buttonwrapper" bgcolor="#be1e2d" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td class="button" height="45">
          <a href="{{ url($link[0]) }}" target="_blank">{{ $link[1] }}</a>
        </td>
      </tr>
    </table>
  </td>
</tr>
@endif
<tr>
  <td class="bodycopy" style="padding-top: 10px;" {{ (isset($left))?'align="left"':'align="center"' }}>
    <?php echo $body; ?>
  </td>
</tr>
@endsection