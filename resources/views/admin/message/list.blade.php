@extends('layouts.app')

@section('htmlheader_title')
	{{ $title }}
@endsection


@section('contentheader_title')
	{{ $title }}
@endsection

@section('breadcrumb')
	<li>Message</li>
@endsection

@section('main-content')

<div class="row">
	@include('admin.message.leftPanel')
  <div class="col-md-9">
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
          <table id="inbox-tbl" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th class="nosort"><button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></th>
              <th>Name</th>
              <th>Subject</th>
              <th>Date</th>
            </tr>
            </thead>
            <tbody>
              @foreach($messages as $message)
            <tr{{ ($title == 'Inbox' && $message->read == 'No')?' class=unread':'' }}>
              <td><input type="checkbox" /></td>
              <td><a href="{{ url('message/read/'.$message->id) }}">{{ $message->from_details->name }}</a></td>
              <td><a href="{{ url('message/read/'.$message->id) }}">{{ $message->subject }}</a></td>
              <td>{{ date('Y-m-d h:iA', strtotime($message->created_at)) }}</td>
            </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
              <th class="nosort"><button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></th>
              <th>Name</th>
              <th>Subject</th>
              <th>Date</th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
    </div>
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->

@endsection


@section('added-css')
<!-- iCheck -->
<link href="{{ asset('/plugins/iCheck/flat/blue.css') }}" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link href="{{ asset('/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('added-scripts')
<!-- DataTables -->
<script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
  $("#inbox-tbl").DataTable({
    "aoColumns": [
            { "bSortable": false },
            null,
            null,
            null
         ],
      "order": [[ 3, "desc" ]]
  });

  $('input[type="checkbox"]').iCheck({
    checkboxClass: 'icheckbox_flat-blue',
    radioClass: 'iradio_flat-blue'
  });

  //Enable check and uncheck all functionality
  $(".checkbox-toggle").click(function () {
    var clicks = $(this).data('clicks');
    if (clicks) {
      //Uncheck all checkboxes
      $("#inbox-tbl input[type='checkbox']").iCheck("uncheck");
      $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
    } else {
      //Check all checkboxes
      $("#inbox-tbl input[type='checkbox']").iCheck("check");
      $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
    }
    $(this).data("clicks", !clicks);
  });
});
</script>
@endsection