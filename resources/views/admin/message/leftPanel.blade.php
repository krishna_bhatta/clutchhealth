
  <div class="col-md-3">
    <a href="{{ url('message/compose') }}" class="btn btn-primary btn-block margin-bottom">Compose</a>

    <div class="box box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Folders</h3>

        <div class="box-tools">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="box-body no-padding">
        <ul class="nav nav-pills nav-stacked">
          <li{{ ($title=='Inbox') ? ' class=active' : '' }}><a href="{{ url('admin/message') }}"><i class="fa fa-inbox"></i> Inbox
            <!-- <span class="label label-primary pull-right">12</span> --></a></li>
          <li{{ ($title=='Sent') ? ' class=active' : '' }}><a href="{{ url('admin/message/sent') }}"><i class="fa fa-envelope-o"></i> Sent</a></li>
          <li{{ ($title=='Trash') ? ' class=active' : '' }}><a href="{{ url('admin/message/trash') }}"><i class="fa fa-trash-o"></i> Trash</a></li>
        </ul>
      </div>
      <!-- /.box-body -->
    </div>
    <?php /*
    <!-- /. box -->
    <div class="box box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Labels</h3>

        <div class="box-tools">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="box-body no-padding">
        <ul class="nav nav-pills nav-stacked">
          <li><a href="#"><i class="fa fa-circle-o text-red"></i> Important</a></li>
          <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> Promotions</a></li>
          <li><a href="#"><i class="fa fa-circle-o text-light-blue"></i> Social</a></li>
        </ul>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box --> */ ?>
  </div>
  <!-- /.col -->