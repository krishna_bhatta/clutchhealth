@extends('layouts.app')

@section('htmlheader_title')
  Broadcast email
@endsection

@section('contentheader_title')
  Broadcast email
@endsection

@section('breadcrumb')
  <li>Broadcast email</li>
@endsection

@section('main-content')
<div class="row">
  <div class="col-md-12">
    <div class="box box-primary">
      <form action="{{ url('/admin/broadcast-email-action') }}" method="post">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <!-- /.box-header -->
      <div class="box-body">
        <div class="form-group">
          <div class="row">
            <div class="col-md-6">
              <select name="type" class="form-control">
              <option value="Trainers">Trainers</option>
              <option value="Customers">Customers</option>
              <option value="All">All</option>
              </select>
            </div>
            <!-- <div class="col-md-4">
              <select name="type" class="form-control">
              <option value="Blank Description">Blank Description</option>
              <option value="Not Blank Description">Not Blank Description</option>
              <option value="All">All</option>
              </select>
            </div> -->
            <div class="col-md-6">
              <input type="checkbox" name="active" id="active" /> <label for="active">Only for active</label>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="row">
            <div class="col-md-6">
              <input type="text" name="page_title" id="page_title" class="form-control" placeholder="Subject" />
            </div>
            <div class="col-md-6">
              <input type="text" name="address" id="address" class="form-control" placeholder="Address (eg. Hello **fname**)" />
            </div>
          </div>
        </div>
        <div class="form-group">
              <textarea id="compose-textarea" name="message" class="form-control" style="min-height: 300px"></textarea>
        </div>
        <div class="form-group">
          <div class="row">
            <div class="col-md-6">
              <input type="text" name="link_text" id="link_text" class="form-control" placeholder="Link text" />
            </div>
            <div class="col-md-6">
              <input type="url" name="link" id="link" class="form-control" placeholder="Link" />
            </div>
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
          <div class="row">
            <div class="col-md-3">
              <input type="radio" name="is_test" id="is_test_yes" value="Yes" checked="checked" /> <label for="is_test_yes">Test</label>
            </div>
            <div class="col-md-3">
              <input type="radio" name="is_test" id="is_test_no" value="No" /> <label for="is_test_no">Live</label>
            </div>
            <div class="col-md-3">
              <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send</button>
            </div>
            <div class="col-md-3 test-container">
              <input type="text" name="test_email" id="test_email" value="hagiachren2@gmail.com" class="form-control" placeholder="Testing email" />
            </div>
          </div>
      </div>
      <!-- /.box-footer -->
      </form>
    </div>
    <!-- /. box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
@endsection


@section('added-css')
   
 <!-- Custom styles for pnotify-->
<link href="{{ asset('/plugins/pnotify/pnotify.custom.min.css') }}" rel="stylesheet" type="text/css" />
<!-- bootstrap wysihtml5 - text editor -->
<link href="{{ asset('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('added-scripts')
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- Page Script -->
<script>
  $(function () {
    //Add text editor
    $("#compose-textarea").wysihtml5();
  });
  jQuery(document).ready(function($) {
    $("input[name=is_test]").change(function(event) {
      if($("[name=is_test]").val() == "Yes") {
        $(".test-container").show();
      } else {
        $(".test-container").hide();
      }
    });
  });
</script>

<!-- Pnotify -->
<script src="{{ asset('plugins/pnotify/pnotify.custom.min.js') }}"></script>
@if(session()->has('notify'))
<?php $notify = Session::get('notify'); ?>
      <script type="text/javascript">
           jQuery(document).ready(function($) {
              new PNotify({
                  title: '{{ $notify->title }}',
                  text: '{{ $notify->text }}',
                  type: '{{ $notify->type }}', // info, success, error
                  styling: 'bootstrap3'
              });
           });
      </script>
@endif
@endsection