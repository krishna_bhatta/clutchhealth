@extends('layouts.app')

@section('htmlheader_title')
	{{ $blog-> title }}
@endsection


@section('contentheader_title')
	{{ $blog-> title }}
@endsection

@section('breadcrumb')
	<li>Edit blog</li>
@endsection

@section('main-content')
<div class="box">
	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <strong>Whoops!</strong> There were some problems with your input.<br><br>
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
    <!-- /.box-header -->
    <div class="box-body">
    	<form class="form-horizontal" method="post" action="{{ url('/admin/blog/editAction/'.$blog->id) }}" enctype="multipart/form-data">
    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
    	 
       <div class="form-group">
           <div class="col-md-6">
             <label for="title">Title</label>
             <input type="text" name="title" value="{{ $blog->title }}" class="form-control" id="title" placeholder="Title">
           </div>
         <div class="col-md-6">
           <label for="meat_title">Meta title</label>
           <input type="text" name="meta_title" value="{{ meta('blog_metas', 'blog_id', $blog->id, 'meta_title') }}" class="form-control" id="meta_title" placeholder="Meta title">
         </div>
       </div>
       <div class="form-group">
           <div class="col-md-6">
             <label for="description">Description</label>
             <textarea name="description" id="description" style="height:200px;width:100%;display:block;resize:none;">{{ $blog->description }}</textarea>
           </div>
         <div class="col-md-6">
           <label for="meta_description">Meta description</label>
           <textarea name="meta_description" id="meta_description" style="height:200px;width:100%;display:block;resize:none;">{{ meta('blog_metas', 'blog_id', $blog->id, 'meta_description') }}</textarea>
         </div>
       </div>
       <div class="form-group">
           <div class="col-md-4">
              <div class="form-group">
                 <label for="featured_image">Featured image(375px X 195px)</label>
                 <input class="form-control" type="file" name="featured_image" id="featured_image" />
               </div>
               <div class="form-group">
                 <img src="{{ asset('uploads/media/'.$blog->featured_image) }}" height="150" />
               </div>
           </div>
           <div class="col-md-4">
              <div class="form-group">
                 <label for="fb_image">Facebook image(1200px X 630px)</label>
                 <input type="file" name="fb_image" class="form-control" id="fb_image">
               </div>
               <div class="form-group">
                 <img src="{{ asset('uploads/media/'.meta('blog_metas', 'blog_id', $blog->id, 'fb_image')) }}" height="150" />
               </div>
           </div>
           <div class="col-md-4">
              <div class="form-group">
                 <label for="twitter_image">Twitter image(1024px X 512px)</label>
                 <input type="file" name="twitter_image" class="form-control" id="twitter_image">
               </div>
               <div class="form-group">
                 <img src="{{ asset('uploads/media/'.meta('blog_metas', 'blog_id', $blog->id, 'twitter_image')) }}" height="150" />
               </div>
           </div>
       </div>
       <div class="form-group">
         <div class="col-md-6">
             <label for="visibility">Visibility</label>
             <select class="form-control" name="visibility" id="visibility">
                 <option value="Public">Public</option>
                 <option {{ ($blog->visibility == "Private")?'selected':'' }} value="Private">Private</option>
             </select>
         </div>
         <div class="col-md-6">
               <label for="status">Status</label>
               <select class="form-control" name="status" id="status">
                   <option value="Active">Active</option>
                   <option {{ ($blog->status == "Inactive")?'selected':'' }} value="Inactive">Inactive</option>
               </select>
         </div>
       </div>
       <div class="form-group">
         <div class="col-md-12">
           <label for="details">Details</label>
           <textarea id="details" name="details" rows="10" cols="80">{{ $blog->details }}</textarea>
         </div>
       </div>
    </div>
    	  <!-- /.box-body -->

    	  <div class="box-footer">
    	    <button type="submit" class="btn btn-primary">Submit</button>
    	  </div>
    	</form>
</div>
@endsection


@section('added-css')
<!-- DataTables -->
<link href="{{ asset('/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<!-- Tags-input -->
<link href="{{ asset('/plugins/tags-input/bootstrap-tagsinput.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('added-scripts')
<!-- DataTables -->
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<!-- Tags-input -->
<script src="{{ asset('/plugins/tags-input/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('/plugins/tags-input/typeahead.bundle.js') }}"></script> 
<script type="text/javascript">
jQuery(document).ready(function($) {
	CKEDITOR.replace('details');
    $("#title").keyup(function(event) {
        $("#meta_title").val($(this).val());
    });
    $("#description").keyup(function(event) {
        $("#meta_description").val($(this).val());
    });
});
</script>
@endsection
