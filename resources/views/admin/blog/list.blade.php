@extends('layouts.app')

@section('htmlheader_title')
	Blogs
@endsection


@section('contentheader_title')
	Blogs
@endsection

@section('breadcrumb')
	<li>Blogs</li>
@endsection

@section('main-content')
<div class="box">
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-success">
             {{ Session('success') }}
        </div>
    @endif
    <!-- /.box-header -->
    <div class="box-body">
      <table id="user-tbl" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>Title</th>
          <th>Description</th>
          <th>Visibility</th>
          <th>Status</th>
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
        	@foreach($blogs as $row)
	    	<tr>
	    	  <td>{{ $row->title }}</td>
          <td>{{ $row->description }}</td>
	    	  <td>{{ $row->visibility }}</td>
	    	  <td>{{ $row->status }}</td>
	    	  <td>
            <a href="{{ url('admin/blog/edit/'.$row->id) }}" class="btn btn-sm btn-info">Edit</a>
            <a href="{{ url('admin/blog/delete/'.$row->id) }}" onclick="return confirm('Are you sure to delete this blog?')" class="btn btn-sm btn-danger">Delete</a>
	    	  </td>
	    	</tr>
	    	@endforeach
        </tbody>
        <tfoot>
        <tr>
          <th>Title</th>
          <th>Description</th>
          <th>Visibility</th>
          <th>Status</th>
          <th>Action</th>
        </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
</div>
@endsection


@section('added-css')
<!-- DataTables -->
<link href="{{ asset('/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('added-scripts')
<!-- DataTables -->
<script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
	$("#user-tbl").DataTable();
});
</script>
@endsection
