@extends('layouts.app')



@section('htmlheader_title')

  Survey answers

@endsection





@section('contentheader_title')

  Survey answers

@endsection



@section('breadcrumb')

  <li>Survey answers</li>

@endsection



@section('main-content')

<div class="box">

    <!-- /.box-header -->

    <div class="box-body">

      <table id="user-tbl" class="table table-bordered table-striped">

        <thead>

        <tr>

          <th>Question</th>

          <th>Answer</th>

          <th>IP Address</th>

          <th>Timestamp</th>

        </tr>

        </thead>

        <tbody>

          @foreach($answers as $answer)

        <tr>

          <td>{{ $answer->question->title }}</td>

          <td>{{ $answer->answer }}</td>

          <td><a href="http://whatismyipaddress.com/ip/{{ $answer->ip_address }}" target="_blank">{{ $answer->ip_address }}</a></td>

          <td>{{ $answer->created_at }}</td>

        </tr>

        @endforeach

        </tbody>

        <tfoot>

        <tr>

          <th>Question</th>

          <th>Answer</th>

          <th>IP Address</th>

          <th>Timestamp</th>

        </tr>

        </tfoot>

      </table>

    </div>

    <!-- /.box-body -->

</div>

@endsection





@section('added-css')

<!-- DataTables -->

<link href="{{ asset('/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />

@endsection



@section('added-scripts')

<!-- DataTables -->

<script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}"></script>

<script src="{{ asset('/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

<script type="text/javascript">

jQuery(document).ready(function($) {

  $("#user-tbl").DataTable();

});



</script>

@endsection

