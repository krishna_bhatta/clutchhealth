@extends('layouts.app')

@section('htmlheader_title')
	New question
@endsection


@section('contentheader_title')
	New question
@endsection

@section('breadcrumb')
	<li>New question</li>
@endsection

@section('main-content')
<div class="box">
	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <strong>Whoops!</strong> There were some problems with your input.<br><br>
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
    <!-- /.box-header -->
    <div class="box-body">
    	<form class="form-horizontal" method="post" action="{{ url('/admin/question/addAction') }}">
    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
    	  <div class="box-body">
            <div class="form-group">
                <div class="col-md-12">
                  <label for="title">Question</label>
                  <input type="text" name="title" class="form-control" id="title" placeholder="Question">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6">
                    <label for="type">Type</label>
                    <select name="type" id="type" class="form-control">
                        <?php foreach(['checkbox','color','number','radio','text','textarea','url'] as $type): ?>
                            <option value="{{ $type }}">{{ ucfirst($type) }}</option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="status">Status</label>
                    <select name="status" id="status" class="form-control">
                        <option value="Active">Active</option>
                        <option value="Inactive">Inactive</option>
                    </select>
                </div>
            </div>
            <div class="form-group" id="options-container">
                <div class="col-md-12">
                  <label for="options">Options</label>
                  <input type="text" name="options" class="form-control" id="options" placeholde="Options" />
                </div>
            </div>
    	  </div>
    	  <!-- /.box-body -->

    	  <div class="box-footer">
    	    <button type="submit" class="btn btn-primary">Submit</button>
    	  </div>
    	</form>
    </div>
    <!-- /.box-body -->
</div>
@endsection

@section('added-scripts')
<script type="text/javascript">
$(document).ready(function($) {
    $("#type").change(function(event) {
        var val = $(this).find('option:selected').val();
        if(val == "radio" || val == "checkbox") {
            $("#options-container").fadeIn();
        } else {
            $("#options-container").fadeOut();
        }
    });
});
</script>
@endsection
