@extends('layouts.app')



@section('htmlheader_title')

	Rewards

@endsection





@section('contentheader_title')

	Rewards

@endsection



@section('breadcrumb')

	<li>Rewards</li>

@endsection



@section('main-content')

<div class="box">

    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <ul>

                @foreach ($errors as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif

    @if (Session::has('success'))

        <div class="alert alert-success">

             {{ Session('success') }}

        </div>

    @endif

    <!-- /.box-header -->

    <div class="box-body">

      <table id="user-tbl" class="table table-bordered table-striped">

        <thead>

        <tr>
          <th>ID</th>

          <th>Title</th>

          <th>Role</th>

          <th>Points</th>

          <th>Status</th>

          <th>Action</th>

        </tr>

        </thead>

        <tbody>

        	@foreach($rewards as $reward)

	    	<tr>

          <td>{{ $reward->id }}</td>

          <td>{{ $reward->title }}</td>

	    	  <td>{{ $reward->role->title }}</td>

	    	  <td>{{ $reward->point }}</td>

	    	  <td>{{ $reward->status }}</td>

	    	  <td>

            <a href="{{ url('admin/reward/edit/'.$reward->id) }}" class="btn btn-sm btn-info">Edit</a>

            <!-- <a href="{{ url('admin/reward/delete/'.$reward->id) }}" onclick="return confirm('Are you sure to delete this reward system?')" class="btn btn-sm btn-danger">Delete</a> -->

	    	  </td>

	    	</tr>

	    	@endforeach

        </tbody>

        <tfoot>

        <tr>
          <th>ID</th>

          <th>Title</th>

          <th>Role</th>

          <th>Points</th>

          <th>Status</th>

          <th>Action</th>

        </tr>

        </tfoot>

      </table>

    </div>

    <!-- /.box-body -->

</div>

@endsection





@section('added-css')

<!-- DataTables -->

<link href="{{ asset('/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />

@endsection



@section('added-scripts')

<!-- DataTables -->

<script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}"></script>

<script src="{{ asset('/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

<script type="text/javascript">

jQuery(document).ready(function($) {

	$("#user-tbl").DataTable();

});

</script>

@endsection

