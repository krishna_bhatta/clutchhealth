@extends('layouts.app')

@section('htmlheader_title')
	New reward
@endsection


@section('contentheader_title')
	New reward
@endsection

@section('breadcrumb')
	<li>New reward</li>
@endsection

@section('main-content')
<div class="box">
	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <strong>Whoops!</strong> There were some problems with your input.<br><br>
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
    <!-- /.box-header -->
    <div class="box-body">
    	<form class="form-horizontal" method="post" action="{{ url('/admin/reward/addAction') }}">
    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
    	  <div class="box-body">
            <div class="form-group">
                <div class="col-md-12">
                  <label for="title">Title</label>
                  <input type="text" name="title" class="form-control" id="title" placeholder="Title">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-4">
                  <label for="role_id">Role</label>
                  <select name="role_id" id="role_id" class="form-control">
                    @foreach($roles as $role)
                      <option value="{{ $role->id }}">{{ $role->title }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-4">
                  <label for="description">Points</label>
                  <input type="text" name="point" id="point" class="form-control" />
                </div>
        	    <div class="col-md-4">
        	      <label for="status">Status</label>
        	      <select name="status" id="status" class="form-control">
                      <option value="Active">Active</option>
                      <option value="Inactive">Inactive</option>
                  </select>
        	    </div>
            </div>
    	  </div>
    	  <!-- /.box-body -->

    	  <div class="box-footer text-center">
    	    <button type="submit" class="btn btn-primary">Submit</button>
    	  </div>
    	</form>
    </div>
    <!-- /.box-body -->
</div>
@endsection
