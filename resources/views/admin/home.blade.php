@extends('layouts.app')

@section('htmlheader_title')
	Home
@endsection

@section('contentheader_title')
	Dashboard
@endsection

@section('main-content')
	<!-- DONUT CHART -->
	<div class="box box-danger">
	  <div class="box-header with-border" id="dashboard-donut-chart">
	    <h3 class="box-title"><span style="color:#c1c7d1">Offer</span> vs. <span style="color:#3b8bba">Applications</span></h3>

	    <!-- <div class="box-tools pull-right">
	    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
	    </button>
	    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
	      <div class="input-group">
	             <button class="btn btn-success pull-right" id="filter-donute-chart-btn">Submit</button>
	              <button type="button" class="btn btn-default pull-right" id="daterange-btn">
	                <span>
	                  <i class="fa fa-calendar"></i> Date range picker
	                </span>
	                <i class="fa fa-caret-down"></i>
	              </button>
	            </div>
	    </div> -->
	  </div>
	  <div class="box-body">
	      <div class="chart">
	        <canvas id="lineChart" style="height:350px"></canvas>
	      </div>
	    </div>
	  <!-- /.box-body -->
	</div>
	<!-- /.box -->
@endsection

@section('added-css')
<link href="{{ asset('/plugins/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('added-scripts')
<!-- ChartJS 1.0.1 -->
<script src="{{ asset('/plugins/chartjs/Chart.min.js') }}"></script>

<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {

		//Date range as a button
		$('#daterange-btn').daterangepicker(
		    {
		      ranges: {
		        'Today': [moment(), moment()],
		        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		        'This Month': [moment().startOf('month'), moment().endOf('month')],
		        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		      },
		      startDate: moment().subtract(29, 'days'),
		      endDate: moment()
		    },
		    function (start, end) {
		      $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		    }
		);

		var lineChartCanvas = $("#lineChart").get(0).getContext("2d");
		var lineChart = new Chart(lineChartCanvas);
		var lineChartOptions = {
			      //Boolean - If we should show the scale at all
			      showScale: true,
			      //Boolean - Whether grid lines are shown across the chart
			      scaleShowGridLines: false,
			      //String - Colour of the grid lines
			      scaleGridLineColor: "rgba(0,0,0,.05)",
			      //Number - Width of the grid lines
			      scaleGridLineWidth: 1,
			      //Boolean - Whether to show horizontal lines (except X axis)
			      scaleShowHorizontalLines: true,
			      //Boolean - Whether to show vertical lines (except Y axis)
			      scaleShowVerticalLines: true,
			      //Boolean - Whether the line is curved between points
			      bezierCurve: true,
			      //Number - Tension of the bezier curve between points
			      bezierCurveTension: 0.3,
			      //Boolean - Whether to show a dot for each point
			      pointDot: false,
			      //Number - Radius of each point dot in pixels
			      pointDotRadius: 4,
			      //Number - Pixel width of point dot stroke
			      pointDotStrokeWidth: 1,
			      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
			      pointHitDetectionRadius: 20,
			      //Boolean - Whether to show a stroke for datasets
			      datasetStroke: true,
			      //Number - Pixel width of dataset stroke
			      datasetStrokeWidth: 2,
			      //Boolean - Whether to fill the dataset with a color
			      datasetFill: true,
			      //String - A legend template
			      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
			      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
			      maintainAspectRatio: true,
			      //Boolean - whether to make the chart responsive to window resizing
			      responsive: true
			    };
		var areaChartData = {
		      labels: ["January", "February", "March", "April", "May", "June", "July"],
		      datasets: [
		        {
		          label: "Offers",
		          fillColor: "rgba(210, 214, 222, 1)",
		          strokeColor: "rgba(210, 214, 222, 1)",
		          pointColor: "rgba(210, 214, 222, 1)",
		          pointStrokeColor: "#c1c7d1",
		          pointHighlightFill: "#fff",
		          pointHighlightStroke: "rgba(220,220,220,1)",
		          data: [65, 59, 80, 81, 56, 55, 40]
		        },
		        {
		          label: "Applications",
		          fillColor: "rgba(60,141,188,0.9)",
		          strokeColor: "rgba(60,141,188,0.8)",
		          pointColor: "#3b8bba",
		          pointStrokeColor: "rgba(60,141,188,1)",
		          pointHighlightFill: "#fff",
		          pointHighlightStroke: "rgba(60,141,188,1)",
		          data: [100, 90, 90, 50, 70, 150, 200]
		        }
		      ]
		    };
		lineChartOptions.datasetFill = false;
		lineChart.Line(areaChartData, lineChartOptions);
	});
</script>
@endsection
