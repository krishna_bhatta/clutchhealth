@extends('layouts.app')

@section('htmlheader_title')
	New blog
@endsection


@section('contentheader_title')
	New blog
@endsection

@section('breadcrumb')
	<li>New blog</li>
@endsection

@section('main-content')
<div class="box">
	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <strong>Whoops!</strong> There were some problems with your input.<br><br>
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
    <!-- /.box-header -->
    <div class="box-body">
    	<form class="form-horizontal" method="post" action="{{ url('/admin/faq/addAction') }}">
    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
    	  <div class="box-body">
    	    <div class="form-group">
            <div class="col-md-9">
      	      <label for="question">Question</label>
      	      <input type="text" name="question" class="form-control" id="question" placeholder="Question">
            </div>
            <div class="col-md-3">
              <label for="sort_order">Sort order</label>
              <input type="text" name="sort_order" class="form-control" id="sort_order" value="{{ $sort_order }}" placeholder="Sort order">
            </div>
    	    </div>
    	    <div class="form-group">
            <div class="col-md-12">
              <label for="answer">Answer</label>
              <textarea name="answer" id="answer" style="height:200px;width:100%;display:block;resize:none;"></textarea>
            </div>
          </div>
    	    </div>
    	  </div>
    	  <!-- /.box-body -->

    	  <div class="box-footer">
    	    <button type="submit" class="btn btn-primary">Submit</button>
    	  </div>
    	</form>
    </div>
    <!-- /.box-body -->
</div>
@endsection
