
	  <div class="col-md-3">

	    <!-- Profile Image -->
	    <div class="box box-primary">
	      <div class="box-body box-profile">
	      <?php
	      $user_photo = meta('user_metas', 'user_id', $user->id, 'profile_photo');
	      $user_photo = ($user_photo!="")?"uploads/profile/".$user_photo:"img/avatar.jpg";
	      ?>
	        <img class="profile-user-img img-responsive img-circle" src="{{ asset($user_photo) }}" alt="User profile picture">

	        <h3 class="profile-username text-center">{{ $user->name }}</h3>

	        <p class="text-muted text-center">{{ $user->role->title }}</p>

	        <!-- <ul class="list-group list-group-unbordered">
	          <li class="list-group-item">
	            <b>Followers</b> <a class="pull-right">1,322</a>
	          </li>
	          <li class="list-group-item">
	            <b>Following</b> <a class="pull-right">543</a>
	          </li>
	          <li class="list-group-item">
	            <b>Friends</b> <a class="pull-right">13,287</a>
	          </li>
	        </ul>

	        <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a> -->
	      </div>
	      <!-- /.box-body -->
	    </div>
	    <!-- /.box -->

	    <!-- About Me Box -->
	    <div class="box box-primary">
	      <div class="box-header with-border">
	        <h3 class="box-title">About Me</h3>
	      </div>
	      <!-- /.box-header -->
	      <div class="box-body">
	        <strong><i class="fa fa-book margin-r-5"></i> Description</strong>

	        <p class="text-muted">
	          {{ meta('user_metas', 'user_id', $user->id, 'description') }}
	        </p>

	        <hr>

	        <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

	        <p class="text-muted">{{ $user->address }}</p>

	        <hr>

<?php 
$json_award = meta('user_metas', 'user_id', $user->id, 'awards'); 
$awards = json_decode($json_award);
if(is_array($awards)):
	$classes = array('success','primary','default','danger','info','warning');
?>
	        <strong><i class="fa fa-trophy margin-r-5"></i> Awards</strong>

	        <p>
	        <?php
	        	$i=0;
	        	foreach($awards as $award):
	        	$i = ($i>5)?0:$i;
	        ?>
	          <span class="label label-{{ $classes[$i] }}">{{ $award->title }}</span>
	        <?php
	        	$i++;
	        	endforeach;
	        ?>
	        </p>
	        <hr>

<?php endif; ?>

<?php $training_start = meta('user_metas', 'user_id', $user->id, 'training_start'); if($training_start): ?>
	        <strong><i class="fa fa-calendar margin-r-5"></i> Experience</strong>

	        <p>{{ date('Y')-$training_start }} Years</p>
	        <hr>

<?php endif; ?>

<?php 
$json_certificate = meta('user_metas', 'user_id', $user->id, 'certificates'); 
$certificates = json_decode($json_certificate);
if(is_array($certificates)):
	$classes = array('danger','info','warning','success','primary','default');
?>
	        <strong><i class="fa fa-certificate margin-r-5"></i> Certificates</strong>

	        <p>
	        <?php
	        	$i=0;
	        	foreach($certificates as $certificate):
	        	$i = ($i>5)?0:$i;
	        ?>
	          <span class="label label-{{ $classes[$i] }}">{{ $certificate->title }}</span>
	        <?php
	        	$i++;
	        	endforeach;
	        ?>
	        </p>
	        <hr>

<?php endif; ?>
	        <?php /* <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

	        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p> */ ?>
	      </div>
	      <!-- /.box-body -->
	    </div>
	    <!-- /.box -->
	  </div>
	  <!-- /.col -->