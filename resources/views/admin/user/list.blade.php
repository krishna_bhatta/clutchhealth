@extends('layouts.app')

@section('htmlheader_title')
	Users
@endsection


@section('contentheader_title')
	Users
@endsection

@section('breadcrumb')
	<li>Users</li>
@endsection

@section('main-content')
<div class="box">
    <!-- /.box-header -->
    <div class="box-body">
      <table id="user-tbl" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>Name</th>
          <th>Role</th>
          <th>Email</th>
          <th>Phone</th>
          <th>Address</th>
          <th>Approved at</th>
          <th>Status</th>
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
        	@foreach($users as $user)
	    	<tr>
	    	  <td>{{ $user->name }}</td>
	    	  <td>{{ $user->role->title }}</td>
	    	  <td>{{ $user->email }}</td>
	    	  <td>{{ $user->phone }}</td>
	    	  <td>{{ $user->address }}</td>
	    	  <td>{{ ($user->approved_date != "0000-00-00")?date('d F Y', strtotime($user->approved_date)):"-" }}</td>
	    	  <td>{{ $user->status }}</td>
	    	  <td>
            <a href="{{ url('admin/profile/'.$user->id) }}" class="btn btn-sm btn-info">View</a>
            @if($user->status == 'Active')
	    	  	<a href="{{ url('admin/user/triggerStatus/'.$user->id) }}" class="btn btn-sm btn-danger">Ban</a>
            @else
            <a href="{{ url('admin/user/triggerStatus/'.$user->id) }}" class="btn btn-sm btn-success">Active</a>
            @endif
	    	  </td>
	    	</tr>
	    	@endforeach
        </tbody>
        <tfoot>
        <tr>
          <th>Name</th>
          <th>Role</th>
          <th>Email</th>
          <th>Phone</th>
          <th>Address</th>
          <th>Approved at</th>
          <th>Status</th>
          <th>Action</th>
        </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
</div>
@endsection


@section('added-css')
<!-- DataTables -->
<link href="{{ asset('/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('added-scripts')
<!-- DataTables -->
<script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
	$("#user-tbl").DataTable();
});
</script>
@endsection
