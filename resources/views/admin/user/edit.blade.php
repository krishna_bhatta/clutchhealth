@extends('layouts.app')

@section('htmlheader_title')
	Profile
@endsection


@section('contentheader_title')
	Profile
@endsection

@section('breadcrumb')
	<li>Profile</li>
@endsection

@section('main-content')
	<div class="row">
		@include('admin.user.profileSidebar')
	  <div class="col-md-9">
	    <div class="nav-tabs-custom">
	      <?php /* <ul class="nav nav-tabs">
	        <li class="active"><a href="#activity" data-toggle="tab">Activity</a></li>
	        <li><a href="#timeline" data-toggle="tab">Timeline</a></li>
	        <li class="active"><a href="#settings" data-toggle="tab">Settings</a></li>
	      </ul> */ ?>
	      <div class="tab-content">
	        <?php /*<div class="active tab-pane" id="activity">
	          <!-- Post -->
	          <div class="post">
	            <div class="user-block">
	              <img class="img-circle img-bordered-sm" src="../../dist/img/user1-128x128.jpg" alt="user image">
	                  <span class="username">
	                    <a href="#">Jonathan Burke Jr.</a>
	                    <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
	                  </span>
	              <span class="description">Shared publicly - 7:30 PM today</span>
	            </div>
	            <!-- /.user-block -->
	            <p>
	              Lorem ipsum represents a long-held tradition for designers,
	              typographers and the like. Some people hate it and argue for
	              its demise, but others ignore the hate as they create awesome
	              tools to help create filler text for everyone from bacon lovers
	              to Charlie Sheen fans.
	            </p>
	            <ul class="list-inline">
	              <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Share</a></li>
	              <li><a href="#" class="link-black text-sm"><i class="fa fa-thumbs-o-up margin-r-5"></i> Like</a>
	              </li>
	              <li class="pull-right">
	                <a href="#" class="link-black text-sm"><i class="fa fa-comments-o margin-r-5"></i> Comments
	                  (5)</a></li>
	            </ul>

	            <input class="form-control input-sm" type="text" placeholder="Type a comment">
	          </div>
	          <!-- /.post -->

	          <!-- Post -->
	          <div class="post clearfix">
	            <div class="user-block">
	              <img class="img-circle img-bordered-sm" src="../../dist/img/user7-128x128.jpg" alt="User Image">
	                  <span class="username">
	                    <a href="#">Sarah Ross</a>
	                    <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
	                  </span>
	              <span class="description">Sent you a message - 3 days ago</span>
	            </div>
	            <!-- /.user-block -->
	            <p>
	              Lorem ipsum represents a long-held tradition for designers,
	              typographers and the like. Some people hate it and argue for
	              its demise, but others ignore the hate as they create awesome
	              tools to help create filler text for everyone from bacon lovers
	              to Charlie Sheen fans.
	            </p>

	            <form class="form-horizontal">
	              <div class="form-group margin-bottom-none">
	                <div class="col-sm-9">
	                  <input class="form-control input-sm" placeholder="Response">
	                </div>
	                <div class="col-sm-3">
	                  <button type="submit" class="btn btn-danger pull-right btn-block btn-sm">Send</button>
	                </div>
	              </div>
	            </form>
	          </div>
	          <!-- /.post -->

	          <!-- Post -->
	          <div class="post">
	            <div class="user-block">
	              <img class="img-circle img-bordered-sm" src="../../dist/img/user6-128x128.jpg" alt="User Image">
	                  <span class="username">
	                    <a href="#">Adam Jones</a>
	                    <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
	                  </span>
	              <span class="description">Posted 5 photos - 5 days ago</span>
	            </div>
	            <!-- /.user-block -->
	            <div class="row margin-bottom">
	              <div class="col-sm-6">
	                <img class="img-responsive" src="../../dist/img/photo1.png" alt="Photo">
	              </div>
	              <!-- /.col -->
	              <div class="col-sm-6">
	                <div class="row">
	                  <div class="col-sm-6">
	                    <img class="img-responsive" src="../../dist/img/photo2.png" alt="Photo">
	                    <br>
	                    <img class="img-responsive" src="../../dist/img/photo3.jpg" alt="Photo">
	                  </div>
	                  <!-- /.col -->
	                  <div class="col-sm-6">
	                    <img class="img-responsive" src="../../dist/img/photo4.jpg" alt="Photo">
	                    <br>
	                    <img class="img-responsive" src="../../dist/img/photo1.png" alt="Photo">
	                  </div>
	                  <!-- /.col -->
	                </div>
	                <!-- /.row -->
	              </div>
	              <!-- /.col -->
	            </div>
	            <!-- /.row -->

	            <ul class="list-inline">
	              <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Share</a></li>
	              <li><a href="#" class="link-black text-sm"><i class="fa fa-thumbs-o-up margin-r-5"></i> Like</a>
	              </li>
	              <li class="pull-right">
	                <a href="#" class="link-black text-sm"><i class="fa fa-comments-o margin-r-5"></i> Comments
	                  (5)</a></li>
	            </ul>

	            <input class="form-control input-sm" type="text" placeholder="Type a comment">
	          </div>
	          <!-- /.post -->
	        </div>
	        <!-- /.tab-pane -->
	        <div class="tab-pane" id="timeline">
	          <!-- The timeline -->
	          <ul class="timeline timeline-inverse">
	            <!-- timeline time label -->
	            <li class="time-label">
	                  <span class="bg-red">
	                    10 Feb. 2014
	                  </span>
	            </li>
	            <!-- /.timeline-label -->
	            <!-- timeline item -->
	            <li>
	              <i class="fa fa-envelope bg-blue"></i>

	              <div class="timeline-item">
	                <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

	                <h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>

	                <div class="timeline-body">
	                  Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
	                  weebly ning heekya handango imeem plugg dopplr jibjab, movity
	                  jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
	                  quora plaxo ideeli hulu weebly balihoo...
	                </div>
	                <div class="timeline-footer">
	                  <a class="btn btn-primary btn-xs">Read more</a>
	                  <a class="btn btn-danger btn-xs">Delete</a>
	                </div>
	              </div>
	            </li>
	            <!-- END timeline item -->
	            <!-- timeline item -->
	            <li>
	              <i class="fa fa-user bg-aqua"></i>

	              <div class="timeline-item">
	                <span class="time"><i class="fa fa-clock-o"></i> 5 mins ago</span>

	                <h3 class="timeline-header no-border"><a href="#">Sarah Young</a> accepted your friend request
	                </h3>
	              </div>
	            </li>
	            <!-- END timeline item -->
	            <!-- timeline item -->
	            <li>
	              <i class="fa fa-comments bg-yellow"></i>

	              <div class="timeline-item">
	                <span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>

	                <h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>

	                <div class="timeline-body">
	                  Take me to your leader!
	                  Switzerland is small and neutral!
	                  We are more like Germany, ambitious and misunderstood!
	                </div>
	                <div class="timeline-footer">
	                  <a class="btn btn-warning btn-flat btn-xs">View comment</a>
	                </div>
	              </div>
	            </li>
	            <!-- END timeline item -->
	            <!-- timeline time label -->
	            <li class="time-label">
	                  <span class="bg-green">
	                    3 Jan. 2014
	                  </span>
	            </li>
	            <!-- /.timeline-label -->
	            <!-- timeline item -->
	            <li>
	              <i class="fa fa-camera bg-purple"></i>

	              <div class="timeline-item">
	                <span class="time"><i class="fa fa-clock-o"></i> 2 days ago</span>

	                <h3 class="timeline-header"><a href="#">Mina Lee</a> uploaded new photos</h3>

	                <div class="timeline-body">
	                  <img src="http://placehold.it/150x100" alt="..." class="margin">
	                  <img src="http://placehold.it/150x100" alt="..." class="margin">
	                  <img src="http://placehold.it/150x100" alt="..." class="margin">
	                  <img src="http://placehold.it/150x100" alt="..." class="margin">
	                </div>
	              </div>
	            </li>
	            <!-- END timeline item -->
	            <li>
	              <i class="fa fa-clock-o bg-gray"></i>
	            </li>
	          </ul>
	        </div>
	        <!-- /.tab-pane --> */ ?>

	        <div class="active tab-pane" id="settings">
	        @if (count($errors) > 0)
	            <div class="alert alert-danger">
	                <strong>Whoops!</strong> There were some problems with your input.<br><br>
	                <ul>
	                    @foreach ($errors->all() as $error)
	                        <li>{{ $error }}</li>
	                    @endforeach
	                </ul>
	            </div>
	        @endif
	          <form class="form-horizontal" method="post" action="{{ url('admin/profileUpdateAction') }}" enctype="multipart/form-data">
	          	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	            <div class="form-group">
	              <label class="col-sm-2 control-label">Name</label>

	              <div class="col-sm-10">
	                <input type="text" class="form-control" value="{{ $user->name }}" name="name" placeholder="Name">
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-sm-2 control-label">Email</label>

	              <div class="col-sm-10">
	                <input type="email" value="{{ $user->email }}" class="form-control" name="email" placeholder="Email">
	              </div>
	            </div>
	            <div class="form-group">
	              <label for="inputExperience" class="col-sm-2 control-label">Description</label>

	              <div class="col-sm-10">
	                <textarea class="form-control" name="description" placeholder="Description">{{ meta('user_metas', 'user_id', $user->id, 'description') }}</textarea>
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-sm-2 control-label">Phone</label>

	              <div class="col-sm-10">
	                <input type="text" class="form-control" value="{{ $user->phone }}" name="phone" placeholder="Phone">
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-sm-2 control-label">Date of Birth</label>

	              <div class="col-sm-10">
	              	<div class="input-group date">
	              	  <div class="input-group-addon">
	              	    <i class="fa fa-calendar"></i>
	              	  </div>
	              	  <input type="text" value="{{ ($user->date_of_birth != '0000-00-00')?date('d F Y', strtotime($user->date_of_birth)):'' }}" name="date_of_birth" placeholder="Date of birth" class="form-control pull-right" id="dob">
	              	</div>
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-sm-2 control-label">Location Details</label>

	              <div class="col-sm-2">
	                <input type="text" class="form-control" value="{{ $user->suburb }}" name="suburb" placeholder="Suburb">
	              </div>

	              <div class="col-sm-2">
	                <input type="text" class="form-control" value="{{ $user->postal }}" name="postal" placeholder="Postal">
	              </div>

	              <div class="col-sm-3">
	                <input type="text" class="form-control" value="{{ $user->city }}" name="city" placeholder="City">
	              </div>

	              <div class="col-sm-3">
	                <input type="text" class="form-control" value="{{ $user->state }}" name="state" placeholder="State">
	              </div>
	            </div>
	            <div class="form-group">
	              <label for="inputSkills" class="col-sm-2 control-label">Pick location</label>

	              <div class="col-sm-10">
	                <div id="google-location-holder" style="width: 100%; height: 400px;"></div>
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-sm-2 control-label">Map Address</label>

	              <div class="col-sm-10">
	                <input type="text" class="form-control" name="map-address" placeholder="Map Address">
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-sm-2 control-label">GEO Location</label>

	              <div class="col-sm-5">
	                <input type="text" class="form-control" name="longitude" placeholder="Latitude">
	              </div>

	              <div class="col-sm-5">
	                <input type="text" class="form-control" name="latitude" placeholder="Longitude">
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-sm-2 control-label">Profile Image</label>

	              <div class="col-sm-10">
	                <input type="file" onchange="readURL(this);" class="form-control" accept=".gif,.png,.jpg,.jpeg,.bmp,.svg" name="profile_photo" placeholder="Profile Image">
	                <?php
	                $user_photo = meta('user_metas', 'user_id', $user->id, 'profile_photo')
	                ?>
	                <img class="img-responsive" id="image" src="{{ ($user_photo!="")?asset('/uploads/profile/'.$user_photo):"" }}" alt="Picture">
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-sm-2 control-label">New Password</label>

	              <div class="col-sm-10">

		              <div class="col-sm-6">
		                <input type="password" class="form-control" name="password" placeholder="New password">
		              </div>

		              <div class="col-sm-6">
		                <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm new password">
		              </div>
	              </div>
	            </div>
	            <div class="form-group">
	              <div class="col-sm-offset-2 col-sm-10">
	                <button type="submit" class="btn btn-success">Submit</button>
	              </div>
	            </div>
	          </form>
	        </div>
	        <!-- /.tab-pane -->
	      </div>
	      <!-- /.tab-content -->
	    </div>
	    <!-- /.nav-tabs-custom -->
	  </div>
	  <!-- /.col -->
	</div>
	<!-- /.row -->
@endsection


@section('added-css')
<!-- bootstrap datepicker -->
<link href="{{ asset('/plugins/datepicker/datepicker3.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/plugins/cropper/cropper.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('added-scripts')
<script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places'></script>
<script src="{{ asset('/plugins/locationpicker.jquery.min.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="/plugins/cropper/cropper.min.js"></script>
<script type="text/javascript">

function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#image').attr('src', e.target.result);
            start_resize();
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function start_resize() {
	return true;
	$(".cropper-container").remove();
	var $image = $('#image');

	$image.cropper({
	  aspectRatio: 1,
	  viewMode: 1,	  
	  dragMode: 'move',
	  // autoCropArea: 0.65,
	  restore: false,
	  guides: false,
	  highlight: false,
	  cropBoxMovable: true,
	  cropBoxResizable: false,
	  built: function () {
	    croppable = true;
	  }
	});
}

jQuery(document).ready(function($) {
	start_resize();
	$('#dob').datepicker({
	  autoclose: true,
	  format: 'dd MM yyyy'
	});

	@if($user->latitude != "" && $user->longitude != "")
		searchLocation({{ $user->latitude }}, {{ $user->longitude }});
	@else
		getLocation();
	@endif
	function getLocation() {
	    if (navigator.geolocation) {
	        navigator.geolocation.getCurrentPosition(showPosition, positionNotAllowed);
	    } else {
	    	searchLocation(43.6532, -79.3832);
	        x.innerHTML = "Geolocation is not supported by this browser.";
	    }
	}
	function showPosition(position) {
	    lati = position.coords.latitude;
	    longiti = position.coords.longitude;
	    searchLocation(lati, longiti);
	}
	function positionNotAllowed() {
		searchLocation(43.6532, -79.3832);
	}
	function searchLocation(cLtitude, cLongitude) {
		$('#google-location-holder').locationpicker({
			location: {latitude: cLtitude, longitude: cLongitude},
			radius: false,
			inputBinding: {
				latitudeInput: $('input[name=latitude]'),
				longitudeInput: $('input[name=longitude]'),
				radiusInput: $('#us3-radius'),
				locationNameInput: $('input[name=map-address]')    
		    }
		});
	}
});
</script>
@endsection
