@extends('layouts.app')

@section('htmlheader_title')
	New page
@endsection


@section('contentheader_title')
	New page
@endsection

@section('breadcrumb')
	<li>New page</li>
@endsection

@section('main-content')
<div class="box">
	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <strong>Whoops!</strong> There were some problems with your input.<br><br>
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
    <!-- /.box-header -->
    <div class="box-body">
    	<form class="form-horizontal" method="post" action="{{ url('/admin/page/addAction') }}">
    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
    	  <div class="box-body">
            <div class="form-group">
                <div class="col-md-6">
                  <label for="title">Title</label>
                  <input type="text" readonly name="title" class="form-control" id="title" placeholder="Title">
                </div>
        	    <div class="col-md-6">
        	      <label for="meat_title">Meta title</label>
        	      <input type="text" name="meta_title" class="form-control" id="meta_title" placeholder="Meta title">
        	    </div>
            </div>
            <div class="form-group">
                <div class="col-md-6">
                  <label for="description">Description</label>
                  <textarea name="description" id="description" style="height:200px;width:100%;display:block;resize:none;"></textarea>
                </div>
        	    <div class="col-md-6">
        	      <label for="meta_description">Meta description</label>
        	      <textarea name="meta_description" id="meta_description" style="height:200px;width:100%;display:block;resize:none;"></textarea>
        	    </div>
            </div>
            <div class="form-group">
                <div class="col-md-6">
                    <label for="fb_image">Facebook image(1200px X 630px)</label>
                    <input type="file" name="fb_image" class="form-control" id="fb_image">
                </div>
                <div class="col-md-6">
                    <label for="twitter_image">Twitter image(1024px X 512px)</label>
                    <input type="file" name="twitter_image" class="form-control" id="twitter_image">
                </div>
            </div>
            <div class="form-group">
        	    <div class="col-md-12">
        	    	<label for="details">Details</label>
        	    	<textarea id="details" name="details" rows="10" cols="80"></textarea>
        	    </div>
            </div>
    	  </div>
    	  <!-- /.box-body -->

    	  <div class="box-footer">
    	    <button type="submit" class="btn btn-primary">Submit</button>
    	  </div>
    	</form>
    </div>
    <!-- /.box-body -->
</div>
@endsection


@section('added-css')
<!-- DataTables -->
<link href="{{ asset('/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('added-scripts')
<!-- DataTables -->
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
	CKEDITOR.replace('details');
    $("#title").keyup(function(event) {
        $("#meta_title").val($(this).val());
    });
    $("#description").keyup(function(event) {
        $("#meta_description").val($(this).val());
    });
});
</script>
@endsection
