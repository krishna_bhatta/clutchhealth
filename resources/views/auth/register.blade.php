@extends('layouts.auth')

@section('htmlheader_title')
    Membership registration
@endsection

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

        <form id="register-frm" action="{{ url('/register') }}" method="post" enctype='multipart/form-data'>
            <div class="row" id="reg-part1">                
                <h4 class="form-header">Register a new membership</h4>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="Name" name="name" value="{{ old('name') }}"/>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}"/>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Password" name="password"/>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Retype password" name="password_confirmation"/>
                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">                            
                            <div class="checkbox icheck">
                                <label>
                                    <input class="user-type" type="radio" name="role" {{ (old('role') == '2')?' selected':'' }} value="2" /> &nbsp; I am trainer
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6 pull-right col-xs-12">                            
                            <div class="checkbox icheck">
                                <label>
                                    <input class="user-type" type="radio" name="role" {{ (old('role') == '3')?' selected':'' }} value="3" /> &nbsp; I need trainer
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-xs-12">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" name="terms_and_condition" style="margin-right:5px;"> &nbsp; I agree to the <a href="#">terms</a>
                            </label>
                        </div>
                    </div><!-- /.col -->
                    <div class="col-md-4 col-xs-12">
                        <button type="submit" id="reg-btn" class="btn btn-block signup-btn">Register</button>
                    </div><!-- /.col -->
                </div>
            </div>
            <div class="row" id="reg-part2">
               <h4 class="form-header">Tell us something more about you,<br />we can help you to find <span class="find-what">more clients</span>!</h4>
               <input type="hidden" name="latitude" />
               <input type="hidden" name="longitude" />
               <div class="form-group has-feedback">
                   <textarea placeholder="Describe yourself" class="form-control" name="about" id="about">{{ old('about') }}</textarea>
               </div>

               <div class="form-group frm-group-map has-feedback">
                   <div id="google-location-holder" style="width: 100%; height: 200px;"></div>
               </div>
               <div class="form-group has-feedback">
                   <input type="text" class="form-control" name="map-address" placeholder="Address" />
                   <span class="glyphicon glyphicon-screenshot form-control-feedback"></span>
               </div>
               <div class="form-group has-feedback">
                  Profile picture
               </div>
               <div class="form-group has-feedback">
                  <input type="file" accept="image/*" name="profile_photo" id="profile_photo" class="form-control" />
               </div>
               <div class="form-group has-feedback">
                  <div class="col-md-6 col-xs-12" style="padding-left:0;">                            
                      <div class="checkbox icheck">
                          <label style="padding-left:0">
                              <input type="radio" value="1" name="payment_mode_id" /> &nbsp; Per hour
                          </label>
                      </div>
                  </div>
                  <div class="col-md-6 col-xs-12" style="padding-left:0;">                            
                      <div class="checkbox icheck">
                          <label style="padding-left:0">
                              <input type="radio" value="2" name="payment_mode_id" /> &nbsp; Per session
                          </label>
                      </div>
                  </div>
               </div>
                <div class="form-group has-feedback">
                  <input type="text" name="budget" id="budget" class="form-control" placeholder="$" />
                </div>               
               <div class="row">
                   <div class="col-md-12 col-xs-12">
                       <button type="submit" id="reg-btn-final" class="btn btn-block signup-btn">Submit</button>
                   </div><!-- /.col -->
               </div>
            </div>
        </form>
<hr>
<div class="social-holder">
<p>OR</p>
<a href="{{ url('fb-redirect') }}" class="btn btn-primary btn-sm"><i class="fa fa-facebook"></i> Facebook</a>
<a href="{{ url('twitter-redirect') }}" class="btn btn-info btn-sm"><i class="fa fa-twitter"></i> Twitter</a>
<a href="{{ url('google-redirect') }}" class="btn btn-danger btn-sm"><i class="fa fa-google"></i> Google</a>
<a href="{{ url('linkedin-redirect') }}" class="btn btn-info btn-sm"><i class="fa fa-linkedin"></i> Linkedin</a>
</div>
</div>
@endsection

@section('css')
<!-- Custom styles for pnotify-->
<link href="{{ asset('plugins/pnotify/pnotify.custom.min.css') }}" rel="stylesheet">
<style type="text/css">
#auth-div {
    max-width: 450px;
}
.radio, .checkbox {
    float: left;
}
#reg-part2 {
    display: none;
}
.frm-group-map {
    clear: both;
}
</style>
@endsection

@section('script')
<!-- Pnotify -->
<script src="{{ asset('plugins/pnotify/pnotify.custom.min.js') }}"></script>
<script type="text/javascript" src='//maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyCZwwE56KfH-wR9XM0PqNcyx9pmW75gF-g&sensor=false'></script>
<script src="{{ asset('/plugins/locationpicker.jquery.min.js') }}"></script>
    <script>
    var isFormReady = false;
        $(document).ready(function($) {
          $('input[name=role]').on('ifChecked', function(event) {
            if ($('input[name=role]:checked').val() == 2) {
              $(".find-what").html('more clients');
            } else {
              $(".find-what").html('the perfect trainer');
            }
          });
          $('input[name=payment_mode_id]').on('ifChecked', function(event) {
            if ($('input[name=payment_mode_id]:checked').val() == 1) {
              $("#budget").attr('placeholder', 'Enter per hour fee in $');
            } else {
              $("#budget").attr('placeholder', 'Enter per session fee in $');
            }
          });

            function getLocation() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(showPosition, positionNotAllowed);
                } else {
                    searchLocation(43.6532, -79.3832);
                    x.innerHTML = "Geolocation is not supported by this browser.";
                }
            }
            function showPosition(position) {
                lati = position.coords.latitude;
                longiti = position.coords.longitude;
                searchLocation(lati, longiti);
            }
            function positionNotAllowed() {
                searchLocation(43.6532, -79.3832);
            }
            function searchLocation(cLtitude, cLongitude) {
                $('#google-location-holder').locationpicker({
                    location: {latitude: cLtitude, longitude: cLongitude},
                    radius: false,
                    inputBinding: {
                        latitudeInput: $('input[name=latitude]'),
                        longitudeInput: $('input[name=longitude]'),
                        // radiusInput: $('#us3-radius'),
                        locationNameInput: $('input[name=map-address]')    
                    }
                });
            }

            function next_step() {
                $("#reg-btn-final").removeAttr('disabled');
                isFormReady = true;
                $("#reg-part1").fadeOut('500', function() {
                    $("#reg-part2").fadeIn(500,function() {
                        getLocation();
                    });

                });
                $(".social-holder").fadeOut('500');
            }
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-red',
                radioClass: 'iradio_square-red',
                increaseArea: '10%' // optional
            });
            var isValid = $('#register-frm').bootstrapValidator({
                /*feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },*/
                fields: {
                    name: {
                        validators: {
                           notEmpty: {
                               message: 'The name is required.'
                           }
                       }
                    },
                    email: {
                        validators: {
                           notEmpty: {
                               message: 'The email is required.'
                           },
                           emailAddress: {
                               message: 'The email is not a valid.'
                           }
                       }
                    },
                    password: {
                        validators: {
                           notEmpty: {
                               message: 'The password is required.'
                           },
                           stringLength: {
                               min: 5,
                               max: 14,
                               message: 'The password must be between 5 and 14 characters.'
                           }
                       }
                    },
                    password_confirmation: {               
                        validators: {
                            notEmpty: {
                                message: 'The password confirmation is required.'
                            },
                            identical: {
                                field: 'password',
                                message: 'The confirmation password do not match.'
                            }
                        }
                    }
                }
            }).isValid;

          $("#register-frm").submit(function(e){
            if(isFormReady) {
                if(($("#about").val().length) > 50 && isImageUploaded("#profile_photo") && $("input[name='payment_mode_id']:checked").val() && $.isNumeric($("#budget").val())) {
                  return true;
                }
                $(document).ready(function($) {
                   new PNotify({
                       title: 'Payment mode',
                       text: 'Please specify about you(at least 50 characters), profile picture, payment mode and amount.',
                       type: 'info', // info, success, error
                       styling: 'bootstrap3'
                   });
                });
            }
              e.preventDefault();
              if($(this).find('.has-error').length == 0) {
                next_step(); 
              }
          });

        });
        function isImageUploaded(imageField) {
          var ext = $(imageField).val().split('.').pop().toLowerCase();
          if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
              return false;
          } else {
            return true;
          }
        }
    </script>

@endsection

@section('rhs-nav')
<i class="fa fa-user"></i> 
<a href="{{ url('/login') }}">Login</a>
@endsection