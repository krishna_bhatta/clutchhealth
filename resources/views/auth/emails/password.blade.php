{{-- resources/views/emails/password.blade.php --}}
@extends('frontend.layouts.email')

@section('title', 'Activate Your Clutch Health Account')

@section('content')
<tr>
  <td class="h3">
    Somebody requested a new password for your Clutch Health account.
  </td>
</tr>
<tr>
  <td style="padding: 20px 0 0 0;" align="center">
    <table class="buttonwrapper" bgcolor="#be1e2d" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td class="button" height="45">
          <a href="{{ url('password/reset/'.$token) }}" target="_blank">Reset Password</a>
        </td>
      </tr>
    </table>
  </td>
</tr>
<tr>
  <td class="bodycopy" style="padding-top: 15px;" align="center">
    If you haven't requested, just ignore!
  </td>
</tr>
@endsection