@extends('layouts.auth')


@section('htmlheader_title')
Member login area
@endsection

@section('content')
<form id="login-frm" action="{{ url('/login') }}" method="post">
   <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <h4 class="form-header" id="myModalLabel">Login to Your Account</h4>
  @if(session('error'))
    <div class="alert alert-danger">
      <strong>Whoops!</strong> There were some problems with your input.<br><br>
      <ul>
        <li>{{ session('error') }}</li>
      </ul>
    </div>
  @endif
  @if (count($errors) > 0)
      <div class="alert alert-danger">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif
  <div class="form-group">
     <input type="text" class="form-control" name="email" placeholder="Email" >
     <i class="fa fa-envelope"></i>
  </div>
  <div class="form-group" style="margin-bottom:30px;">
     <input type="password" class="form-control login-pw" name="password" placeholder="Password" >
     <i class="fa fa-lock"></i>
  </div>
  <button type="submit" class="log-btn" >Login</button>
  <div class="form-group terms" style="margin-bottom:20px;margin-top:10px;">
     <a href="{{ url('/password/reset') }}">I forgot my password</a><br>
  </div>
</form>
<hr>
<div class="social-holder">
<p>OR</p>
<a href="{{ url('fb-redirect') }}" class="btn btn-primary btn-sm"><i class="fa fa-facebook"></i> Facebook</a>
<a href="{{ url('twitter-redirect') }}" class="btn btn-info btn-sm"><i class="fa fa-twitter"></i> Twitter</a>
<a href="{{ url('google-redirect') }}" class="btn btn-danger btn-sm"><i class="fa fa-google"></i> Google</a>
<a href="{{ url('linkedin-redirect') }}" class="btn btn-info btn-sm"><i class="fa fa-linkedin"></i> Linkedin</a>
</div>
</div>
@endsection

@section('rhs-nav')
<i class="fa fa-user-plus"></i> 
<a href="{{ url('/register') }}">Register</a>
@endsection

@section('script')
<script type="text/javascript">
  $(document).ready(function($) {
      $('input').iCheck({
          checkboxClass: 'icheckbox_square-red',
          radioClass: 'iradio_square-red',
          increaseArea: '10%' // optional
      });
      $('#login-frm').bootstrapValidator({
          /*feedbackIcons: {
              valid: 'glyphicon glyphicon-ok',
              invalid: 'glyphicon glyphicon-remove',
              validating: 'glyphicon glyphicon-refresh'
          },*/
          fields: {
              email: {
                  validators: {
                     notEmpty: {
                         message: 'The email is required.'
                     },
                     emailAddress: {
                         message: 'The email is not a valid.'
                     }
                 }
              },
              password: {
                  validators: {
                     notEmpty: {
                         message: 'The password is required.'
                     },
                     stringLength: {
                         min: 5,
                         max: 14,
                         message: 'The password must be between 5 and 14 characters.'
                     }
                 }
              }
          }
      });
  });
</script>
@endsection

