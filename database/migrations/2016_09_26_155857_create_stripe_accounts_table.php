<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStripeAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stripe_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trainer_id')->unsigned();
            $table->foreign('trainer_id')->references('id')->on('users');
            $table->string('business_name');
            $table->string('email');
            $table->string('default_currency');
            $table->string('stripe_id');
            $table->string('secret');
            $table->string('publishable');
            $table->enum('managed', ['Yes','No'])->default('Yes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stripe_accounts');
    }
}
