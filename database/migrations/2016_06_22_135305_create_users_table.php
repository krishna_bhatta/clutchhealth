<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('role_id')->unsigned();
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            $table->integer('payment_mode_id')->unsigned()->default(1);
            $table->foreign('payment_mode_id')->references('id')->on('payment_modes')->onDelete('cascade');;
            $table->string('email')->unique();
            $table->string('username')->unique();
            $table->string('password');
            $table->date('date_of_birth');
            $table->string('phone');
            $table->string('longitude');
            $table->string('latitude');
            $table->string('postal');
            $table->string('city');
            $table->string('suburb');
            $table->string('state');
            $table->string('country');
            $table->string('address');
            $table->double('budget');
            $table->double('training_area_radius');
            $table->text('requirement');
            $table->enum('is_public', ['Yes','No'])->default('Yes');
            $table->string('confirmation_token');
            $table->enum('status', ['Pending','Process','Reject','Active','Ban'])->default('Pending');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
