-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 15, 2016 at 12:05 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `fitness`
--

-- --------------------------------------------------------

--
-- Table structure for table `allowed_logins`
--

CREATE TABLE IF NOT EXISTS `allowed_logins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `login_type` enum('Manual','Facebook','Twitter','Linkedin','Google') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Manual',
  `social_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `allowed_logins_user_id_foreign` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `allowed_logins`
--

INSERT INTO `allowed_logins` (`id`, `user_id`, `login_type`, `social_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 17, 'Manual', NULL, '2016-08-06 08:46:02', '2016-08-06 08:46:02', NULL),
(3, 20, 'Manual', NULL, '2016-08-06 08:59:47', '2016-08-06 08:59:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `status` enum('Published','Not Published') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `articles_created_by_foreign` (`created_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `title`, `alias`, `description`, `details`, `created_by`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'About Us', 'about-us', 'In sports, clutch means that when the game is on the line, a player makes a winning play to help their team come out on top. Only the best athletes come up clutch, and it takes a ton of preparation to be able to perform at the highest level in a stressful environment. \r\nThis is what we intend to deliver to you. Winning results that help you succeed. ', '<p>We are your go-to internet based exercise platform for trainers and coaches to meet new clients, and vice versa.&nbsp;</p>\r\n\r\n<p><strong>What does &quot;Clutch&quot; mean?</strong></p>\r\n\r\n<p>In sports, clutch means that when the game is on the line, a player makes a winning play to help their team come out on top. Only the best athletes come up clutch, and it takes a ton of preparation to be able to perform at the highest level in a stressful environment.</p>\r\n\r\n<p>This is what we intend to deliver to you. Winning results that help you succeed.</p>\r\n\r\n<p><strong>Why Clutch Health?</strong></p>\r\n\r\n<p>Do you want to start incorporating exercise into your life? Are you looking to improve in or learn about a sport? Clutch Health will help you find a trainer or a coach to help you accomplish your goals.</p>\r\n', 2, 'Published', '2016-08-14 05:00:33', '2016-08-14 05:00:33', NULL),
(2, 'Terms and Conditions', 'terms-and-conditions', 'These terms and conditions outline the rules and regulations for the use of Clutch Health''s Website.', '<h2>These terms and conditions outline the rules and regulations for the use of Clutch Health&#39;s Website.<br />\r\nClutch Health is located at:</h2>\r\n\r\n<p>156 Crestwood Rd. ,<br />\r\nToronto, ON L4J1A6<br />\r\nCanada</p>\r\n\r\n<p>By accessing this website we assume you accept these terms and conditions in full. Do not continue to use Clutch Health&#39;s website if you do not accept all of the terms and conditions stated on this page.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and any or all Agreements: &quot;Client&quot;, &ldquo;You&rdquo; and &ldquo;Your&rdquo; refers to you, the person accessing this website and accepting the Company&rsquo;s terms and conditions. &quot;The Company&quot;, &ldquo;Ourselves&rdquo;, &ldquo;We&rdquo;, &ldquo;Our&rdquo; and &quot;Us&quot;, refers to our Company. &ldquo;Party&rdquo;, &ldquo;Parties&rdquo;, or &ldquo;Us&rdquo;, refers to both the Client and ourselves, or either the Client or ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner, whether by formal meetings of a fixed duration, or any other means, for the express purpose of meeting the Client&rsquo;s needs in respect of provision of the Company&rsquo;s stated services/products, in accordance with and subject to, prevailing law of Canada. Any use of the above terminology or other words in the singular, plural, capitalisation and/or he/she or they, are taken as interchangeable and therefore as referring to same.</p>\r\n\r\n<h2>Cookies</h2>\r\n\r\n<p>We employ the use of cookies. By using <a href="http://clutchhealth.com" onclick="__gaTracker(''send'', ''event'', ''outbound-article'', ''http://clutchhealth.com'', ''Clutch Health'');">Clutch Health</a>&#39;s website you consent to the use of cookies in accordance with Clutch Health&rsquo;s privacy policy.</p>\r\n\r\n<p>Most of the modern day interactive web sites use cookies to enable us to retrieve user details for each visit. Cookies are used in some areas of our site to enable the functionality of this area and ease of use for those people visiting. Some of our affiliate / advertising partners may also use cookies.</p>\r\n\r\n<h2>License</h2>\r\n\r\n<p>Unless otherwise stated, Clutch Health and/or it&rsquo;s licensors own the intellectual property rights for all material on Clutch Health All intellectual property rights are reserved. You may view and/or print pages from http://clutchhealth.com for your own personal use subject to restrictions set in these terms and conditions.</p>\r\n\r\n<p>You must not:</p>\r\n\r\n<ul>\r\n	<li>Republish material from http://clutchhealth.com</li>\r\n	<li>Sell, rent or sub-license material from http://clutchhealth.com</li>\r\n	<li>Reproduce, duplicate or copy material from http://clutchhealth.com</li>\r\n</ul>\r\n\r\n<p>Redistribute content from Clutch Health (unless content is specifically made for redistribution).</p>\r\n\r\n<h3>User Comments</h3>\r\n\r\n<ol>\r\n	<li>This Agreement shall begin on the date hereof.</li>\r\n	<li>Certain parts of this website offer the opportunity for users to post and exchange opinions, information, material and data (&#39;Comments&#39;) in areas of the website. Clutch Health does not screen, edit, publish or review Comments prior to their appearance on the website and Comments do not reflect the views or opinions of Clutch Health, its agents or affiliates. Comments reflect the view and opinion of the person who posts such view or opinion. To the extent permitted by applicable laws Clutch Health shall not be responsible or liable for the Comments or for any loss cost, liability, damages or expenses caused and or suffered as a result of any use of and/or posting of and/or appearance of the Comments on this website.</li>\r\n	<li>Clutch Health reserves the right to monitor all Comments and to remove any Comments which it considers in its absolute discretion to be inappropriate, offensive or otherwise in breach of these Terms and Conditions.</li>\r\n	<li>You warrant and represent that:\r\n	<ol>\r\n		<li>You are entitled to post the Comments on our website and have all necessary licenses and consents to do so;</li>\r\n		<li>The Comments do not infringe any intellectual property right, including without limitation copyright, patent or trademark, or other proprietary right of any third party;</li>\r\n		<li>The Comments do not contain any defamatory, libelous, offensive, indecent or otherwise unlawful material or material which is an invasion of privacy</li>\r\n		<li>The Comments will not be used to solicit or promote business or custom or present commercial activities or unlawful activity.</li>\r\n	</ol>\r\n	</li>\r\n	<li>You hereby grant to <strong>Clutch Health</strong> a non-exclusive royalty-free license to use, reproduce, edit and authorize others to use, reproduce and edit any of your Comments in any and all forms, formats or media.</li>\r\n</ol>\r\n\r\n<h2>Hyperlinking to our Content</h2>\r\n\r\n<ol>\r\n	<li>The following organizations may link to our Web site without prior written approval:\r\n	<ul>\r\n		<li>Government agencies;</li>\r\n		<li>Search engines;</li>\r\n		<li>News organizations;</li>\r\n		<li>Online directory distributors when they list us in the directory may link to our Web site in the same manner as they hyperlink to the Web sites of other listed businesses; and</li>\r\n		<li>Systemwide Accredited Businesses except soliciting non-profit organizations, charity shopping malls, and charity fundraising groups which may not hyperlink to our Web site.</li>\r\n	</ul>\r\n	</li>\r\n</ol>\r\n\r\n<ol>\r\n	<li>These organizations may link to our home page, to publications or to other Web site information so long as the link: (a) is not in any way misleading; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products or services; and (c) fits within the context of the linking party&#39;s site.</li>\r\n	<li>We may consider and approve in our sole discretion other link requests from the following types of organizations:\r\n	<ul>\r\n		<li>commonly-known consumer and/or business information sources such as Chambers of Commerce, American Automobile Association, AARP and Consumers Union;</li>\r\n		<li>dot.com community sites;</li>\r\n		<li>associations or other groups representing charities, including charity giving sites,</li>\r\n		<li>online directory distributors;</li>\r\n		<li>internet portals;</li>\r\n		<li>accounting, law and consulting firms whose primary clients are businesses; and</li>\r\n		<li>educational institutions and trade associations.</li>\r\n	</ul>\r\n	</li>\r\n</ol>\r\n\r\n<p>We will approve link requests from these organizations if we determine that: (a) the link would not reflect unfavorably on us or our accredited businesses (for example, trade associations or other organizations representing inherently suspect types of business, such as work-at-home opportunities, shall not be allowed to link); (b)the organization does not have an unsatisfactory record with us; (c) the benefit to us from the visibility associated with the hyperlink outweighs the absence of Clutch Health; and (d) where the link is in the context of general resource information or is otherwise consistent with editorial content in a newsletter or similar product furthering the mission of the organization.</p>\r\n\r\n<p>These organizations may link to our home page, to publications or to other Web site information so long as the link: (a) is not in any way misleading; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and it products or services; and (c) fits within the context of the linking party&#39;s site.</p>\r\n\r\n<p>If you are among the organizations listed in paragraph 2 above and are interested in linking to our website, you must notify us by sending an e-mail to <a href="mailto:support@clutchhealth.com">support@clutchhealth.com</a>. Please include your name, your organization name, contact information (such as a phone number and/or e-mail address) as well as the URL of your site, a list of any URLs from which you intend to link to our Web site, and a list of the URL(s) on our site to which you would like to link. Allow 2-3 weeks for a response.</p>\r\n\r\n<p>Approved organizations may hyperlink to our Web site as follows:</p>\r\n\r\n<ul>\r\n	<li>By use of our corporate name; or</li>\r\n	<li>By use of the uniform resource locator (Web address) being linked to; or</li>\r\n	<li>By use of any other description of our Web site or material being linked to that makes sense within the context and format of content on the linking party&#39;s site.</li>\r\n</ul>\r\n\r\n<p>No use of (cname)&rsquo;s logo or other artwork will be allowed for linking absent a trademark license agreement.</p>\r\n\r\n<h2>Iframes</h2>\r\n\r\n<p>Without prior approval and express written permission, you may not create frames around our Web pages or use other techniques that alter in any way the visual presentation or appearance of our Web site.</p>\r\n\r\n<h2>Content Liability</h2>\r\n\r\n<p>We shall have no responsibility or liability for any content appearing on your Web site. You agree to indemnify and defend us against all claims arising out of or based upon your Website. No link(s) may appear on any page on your Web site or within any context containing content or materials that may be interpreted as libelous, obscene or criminal, or which infringes, otherwise violates, or advocates the infringement or other violation of, any third party rights.</p>\r\n\r\n<h2>Reservation of Rights</h2>\r\n\r\n<p>We reserve the right at any time and in its sole discretion to request that you remove all links or any particular link to our Web site. You agree to immediately remove all links to our Web site upon such request. We also reserve the right to amend these terms and conditions and its linking policy at any time. By continuing to link to our Web site, you agree to be bound to and abide by these linking terms and conditions.</p>\r\n\r\n<h2>Removal of links from our website</h2>\r\n\r\n<p>If you find any link on our Web site or any linked web site objectionable for any reason, you may contact us about this. We will consider requests to remove links but will have no obligation to do so or to respond directly to you.</p>\r\n\r\n<p>Whilst we endeavour to ensure that the information on this website is correct, we do not warrant its completeness or accuracy; nor do we commit to ensuring that the website remains available or that the material on the website is kept up to date.</p>\r\n\r\n<h2>Disclaimer</h2>\r\n\r\n<p>To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to our website and the use of this website (including, without limitation, any warranties implied by law in respect of satisfactory quality, fitness for purpose and/or the use of reasonable care and skill). Nothing in this disclaimer will:</p>\r\n\r\n<ol>\r\n	<li>limit or exclude our or your liability for death or personal injury resulting from negligence;</li>\r\n	<li>limit or exclude our or your liability for fraud or fraudulent misrepresentation;</li>\r\n	<li>limit any of our or your liabilities in any way that is not permitted under applicable law; or</li>\r\n	<li>exclude any of our or your liabilities that may not be excluded under applicable law.</li>\r\n</ol>\r\n\r\n<p>The limitations and exclusions of liability set out in this Section and elsewhere in this disclaimer: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer or in relation to the subject matter of this disclaimer, including liabilities arising in contract, in tort (including negligence) and for breach of statutory duty.</p>\r\n\r\n<p>To the extent that the website and the information and services on the website are provided free of charge, we will not be liable for any loss or damage of any nature.</p>\r\n', 2, 'Published', '2016-08-14 05:02:17', '2016-08-14 07:25:24', NULL),
(3, 'Privacy Policy', 'privacy-policy', 'This privacy policy has been compiled to better serve those who are concerned with how their ''Personally Identifiable Information'' (PII) is being used online. PII, as described in US privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.', '<p>This privacy policy has been compiled to better serve those who are concerned with how their &#39;Personally Identifiable Information&#39; (PII) is being used online. PII, as described in US privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.</p>\r\n\r\n<p><strong>What personal information do we collect from the people that visit our blog, website or app?</strong></p>\r\n\r\n<p>When ordering or registering on our site, as appropriate, you may be asked to enter your name, email address, credit card information or other details to help you with your experience.</p>\r\n\r\n<p><strong>When do we collect information?</strong></p>\r\n\r\n<p>We collect information from you when you register on our site, place an order or enter information on our site.</p>\r\n\r\n<p><strong>How do we use your information?</strong></p>\r\n\r\n<p>We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:<br />\r\n&nbsp;</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&bull;</strong>&nbsp;To personalize your experience and to allow us to deliver the type of content and product offerings in which you are most interested.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&bull;</strong>&nbsp;To improve our website in order to better serve you.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&bull;</strong>&nbsp;To allow us to better service you in responding to your customer service requests.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&bull;</strong>&nbsp;To administer a contest, promotion, survey or other site feature.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&bull;</strong>&nbsp;To quickly process your transactions.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&bull;</strong>&nbsp;To ask for ratings and reviews of services or products</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&bull;</strong>&nbsp;To follow up with them after correspondence (live chat, email or phone inquiries)</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>How do we protect your information?</strong></p>\r\n\r\n<p>Our website is scanned on a regular basis for security holes and known vulnerabilities in order to make your visit to our site as safe as possible.</p>\r\n\r\n<p>We use regular Malware Scanning.<br />\r\n&nbsp;</p>\r\n\r\n<p>Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems, and are required to keep the information confidential. In addition, all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>We implement a variety of security measures when a user places an order enters, submits, or accesses their information to maintain the safety of your personal information.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>All transactions are processed through a gateway provider and are not stored or processed on our servers.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Do we use &#39;cookies&#39;?</strong></p>\r\n\r\n<p>Yes. Cookies are small files that a site or its service provider transfers to your computer&#39;s hard drive through your Web browser (if you allow) that enables the site&#39;s or service provider&#39;s systems to recognize your browser and capture and remember certain information. For instance, we use cookies to help us remember and process the items in your shopping cart. They are also used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.</p>\r\n\r\n<p><br />\r\n<strong>We use cookies to:</strong></p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&bull;</strong>&nbsp;Help remember and process the items in the shopping cart.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&bull;</strong>&nbsp;Understand and save user&#39;s preferences for future visits.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&bull;</strong>&nbsp;Compile aggregate data about site traffic and site interactions in order to offer better site experiences and tools in the future. We may also use trusted third-party services that track this information on our behalf.</p>\r\n\r\n<p><br />\r\nYou can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser settings. Since browser is a little different, look at your browser&#39;s Help Menu to learn the correct way to modify your cookies.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>If you turn cookies off, some features will be disabled. It won&#39;t affect the user&#39;s experience that make your site experience more efficient and may not function properly.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>However, you will still be able to place orders .</p>\r\n\r\n<p><br />\r\n&nbsp;</p>\r\n\r\n<p><strong>Third-party disclosure</strong></p>\r\n\r\n<p>We do not sell, trade, or otherwise transfer to outside parties your Personally Identifiable Information.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Third-party links</strong></p>\r\n\r\n<p>Occasionally, at our discretion, we may include or offer third-party products or services on our website. These third-party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Google</strong></p>\r\n\r\n<p>Google&#39;s advertising requirements can be summed up by Google&#39;s Advertising Principles. They are put in place to provide a positive experience for users. https://support.google.com/adwordspolicy/answer/1316548?hl=en&nbsp;<br />\r\n&nbsp;</p>\r\n\r\n<p>We use Google AdSense Advertising on our website.</p>\r\n\r\n<p><br />\r\nGoogle, as a third-party vendor, uses cookies to serve ads on our site. Google&#39;s use of the DART cookie enables it to serve ads to our users based on previous visits to our site and other sites on the Internet. Users may opt-out of the use of the DART cookie by visiting the Google Ad and Content Network privacy policy.</p>\r\n\r\n<p><br />\r\n<strong>We have implemented the following:</strong></p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&bull;</strong>&nbsp;Google Display Network Impression Reporting</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&bull;</strong>&nbsp;Demographics and Interests Reporting</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>We, along with third-party vendors such as Google use first-party cookies (such as the Google Analytics cookies) and third-party cookies (such as the DoubleClick cookie) or other third-party identifiers together to compile data regarding user interactions with ad impressions and other ad service functions as they relate to our website.</p>\r\n\r\n<p><br />\r\n<strong>Opting out:</strong><br />\r\nUsers can set preferences for how Google advertises to you using the Google Ad Settings page. Alternatively, you can opt out by visiting the Network Advertising Initiative Opt Out page or by using the Google Analytics Opt Out Browser add on.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><strong><strong>California Online Privacy Protection Act</strong></strong></strong></p>\r\n\r\n<p>CalOPPA is the first state law in the nation to require commercial websites and online services to post a privacy policy. The law&#39;s reach stretches well beyond California to require any person or company in the United States (and conceivably the world) that operates websites collecting Personally Identifiable Information from California consumers to post a conspicuous privacy policy on its website stating exactly the information being collected and those individuals or companies with whom it is being shared. - See more at: http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf</p>\r\n\r\n<p><br />\r\n<strong><strong><strong>According to CalOPPA, we agree to the following:</strong></strong></strong></p>\r\n\r\n<p><strong><strong>Users can visit our site anonymously.</strong></strong></p>\r\n\r\n<p>Once this privacy policy is created, we will add a link to it on our home page or as a minimum, on the first significant page after entering our website.</p>\r\n\r\n<p>Our Privacy Policy link includes the word &#39;Privacy&#39; and can be easily be found on the page specified above.</p>\r\n\r\n<p><br />\r\n<strong><strong>You will be notified of any Privacy Policy changes:</strong></strong></p>\r\n\r\n<p><strong><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&bull;</strong>&nbsp;On our Privacy Policy Page</strong></strong></p>\r\n\r\n<p><strong><strong>Can change your personal information:</strong></strong></p>\r\n\r\n<p><strong><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&bull;</strong>&nbsp;By logging in to your account</strong></strong></p>\r\n\r\n<p><br />\r\n<strong><strong><strong>How does our site handle Do Not Track signals?</strong></strong></strong></p>\r\n\r\n<p>We honor Do Not Track signals and Do Not Track, plant cookies, or use advertising when a Do Not Track (DNT) browser mechanism is in place.</p>\r\n\r\n<p><br />\r\n<strong><strong><strong>Does our site allow third-party behavioral tracking?</strong></strong></strong></p>\r\n\r\n<p>It&#39;s also important to note that we do not allow third-party behavioral tracking</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><strong><strong>COPPA (Children Online Privacy Protection Act)</strong></strong></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>When it comes to the collection of personal information from children under the age of 13 years old, the Children&#39;s Online Privacy Protection Act (COPPA) puts parents in control. The Federal Trade Commission, United States&#39; consumer protection agency, enforces the COPPA Rule, which spells out what operators of websites and online services must do to protect children&#39;s privacy and safety online.<br />\r\n&nbsp;</p>\r\n\r\n<p><strong><strong>We do not specifically market to children under the age of 13 years old.</strong></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><strong><strong>Fair Information Practices</strong></strong></strong></p>\r\n\r\n<p>The Fair Information Practices Principles form the backbone of privacy law in the United States and the concepts they include have played a significant role in the development of data protection laws around the globe. Understanding the Fair Information Practice Principles and how they should be implemented is critical to comply with the various privacy laws that protect personal information.<br />\r\n&nbsp;</p>\r\n\r\n<p><strong><strong><strong>In order to be in line with Fair Information Practices we will take the following responsive action, should a data breach occur:</strong></strong></strong></p>\r\n\r\n<p><strong><strong>We will notify you via email</strong></strong></p>\r\n\r\n<p><strong><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&bull;</strong>&nbsp;Within 7 business days</strong></strong></p>\r\n\r\n<p><strong><strong>We will notify the users via in-site notification</strong></strong></p>\r\n\r\n<p><strong><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&bull;</strong>&nbsp;Within 7 business days</strong></strong></p>\r\n\r\n<p><br />\r\nWe also agree to the Individual Redress Principle which requires that individuals have the right to legally pursue enforceable rights against data collectors and processors who fail to adhere to the law. This principle requires not only that individuals have enforceable rights against data users, but also that individuals have recourse to courts or government agencies to investigate and/or prosecute non-compliance by data processors.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><strong><strong>CAN SPAM Act</strong></strong></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><strong>The CAN-SPAM Act is a law that sets the rules for commercial email, establishes requirements for commercial messages, gives recipients the right to have emails stopped from being sent to them, and spells out tough penalties for violations.</strong></strong><br />\r\n&nbsp;</p>\r\n\r\n<p><strong><strong><strong>We collect your email address in order to:</strong></strong></strong></p>\r\n\r\n<p><strong><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&bull;</strong>&nbsp;</strong></strong>Send information, respond to inquiries, and/or other requests or questions</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&bull;&nbsp;Process orders and to send information and updates pertaining to orders.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&bull;&nbsp;Send you additional information related to your product and/or service</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&bull;&nbsp;Market to our mailing list or continue to send emails to our clients after the original transaction has occurred.</p>\r\n\r\n<p><br />\r\n<strong><strong><strong>To be in accordance with CANSPAM, we agree to the following:</strong></strong></strong></p>\r\n\r\n<p><strong><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&bull;</strong></strong></strong>&nbsp;Not use false or misleading subjects or email addresses.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&bull;&nbsp;Identify the message as an advertisement in some reasonable way.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&bull;&nbsp;Include the physical address of our business or site headquarters.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&bull;&nbsp;Monitor third-party email marketing services for compliance, if one is used.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&bull;&nbsp;Honor opt-out/unsubscribe requests quickly.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&bull;&nbsp;Allow users to unsubscribe by using the link at the bottom of each email.</p>\r\n\r\n<p><br />\r\n<strong><strong><strong>If at any time you would like to unsubscribe from receiving future emails, you can email us at</strong></strong></strong></p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&bull;&nbsp;Follow the instructions at the bottom of each email.</p>\r\n\r\n<p><strong><strong>and we will promptly remove you from&nbsp;<strong>ALL</strong>&nbsp;correspondence.</strong></strong></p>\r\n\r\n<p><br />\r\n&nbsp;</p>\r\n\r\n<p><strong><strong><strong>Contacting Us</strong></strong></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><strong>If there are any questions regarding this privacy policy, you may contact us using the information below.</strong></strong><br />\r\n&nbsp;</p>\r\n\r\n<p><strong><strong>ClutchHealth.com</strong></strong></p>\r\n\r\n<p><strong><strong>156 Crestwood Rd.</strong></strong></p>\r\n\r\n<p><strong><strong>Thornhill, Ontario L4J1A6</strong></strong></p>\r\n\r\n<p><strong><strong>Canada</strong></strong></p>\r\n\r\n<p><strong><strong>hagiachren2@gmail.com</strong></strong></p>\r\n\r\n<p><strong><strong>647-242-5646</strong></strong></p>\r\n\r\n<p><br />\r\n<strong><strong>Last Edited on 2016-08-09</strong></strong></p>\r\n', 2, 'Published', '2016-08-14 05:18:18', '2016-08-14 07:28:42', NULL),
(4, 'Contact Us', 'contact-us', '156 Crestwood Rd.\r\n\r\nThornhill, Ontario L4J1A6\r\n\r\nCanada\r\n\r\nhagiachren2@gmail.com\r\n\r\n647-242-5646', '<p><strong><strong>156 Crestwood Rd.&nbsp;</strong></strong><strong><strong>Thornhill, Ontario L4J1A6&nbsp;</strong></strong><strong><strong>Canada</strong></strong></p>\r\n\r\n<p><strong><strong>info@clutchhealth.com</strong></strong></p>\r\n\r\n<p><strong><strong>647-242-5646</strong></strong></p>\r\n', 2, 'Published', '2016-08-14 05:20:16', '2016-08-14 05:21:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `article_metas`
--

CREATE TABLE IF NOT EXISTS `article_metas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(10) unsigned NOT NULL,
  `meta_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_value` text COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `article_metas_article_id_foreign` (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE IF NOT EXISTS `blogs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `featured_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author` int(10) unsigned NOT NULL,
  `visibility` enum('Private','Public') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Public',
  `status` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `blogs_author_foreign` (`author`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `alias`, `description`, `details`, `featured_image`, `author`, `visibility`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Journey to health, exercise, food, stronger mindset', 'journey-to-health-exercise-food-stronger-mindset', 'I finally realized what I had done to myself when it was my 21st birthday. I gained roughly 60 pounds in less than a year, and it wasn''t the good kind of weight. I started to see stretch marks near my waist and belly, and the photos that were taken of me during my birthday celebration revealed that  I was a person that ate as much food as I wanted, with no self-control. I was mis-using food. I was using food as a source of comfort and pleasure.', '<p>The year was 2010, but I remember it as if was yesterday. I had come home from one of my mundane physiology classes, and no matter how hard I tried I couldn&#39;t even get a D. The anxiety started kicking in, and I couldn&#39;t help but see myself as a failure. My parents were working tirelessly to give me an opportunity that they never had, to be the first from our family to get a University education. All I could think about was how disappointed they would feel once they learned I wasted their money and effort by failing 5 courses in my first two years at school. This led me to turn to the only thing I&#39;ve felt comfort in <em>&mdash; </em>food. Food was always on my mind, no matter what the situation. I knew that I could always turn to that 30-minute euphoric experience and stuff my face with that delicious extra-large pizza, or maybe a couple Big Macs would hit the spot. I started to impulsively eat extremely large amounts of food and the part of my brain that was supposed to care for my well-being and health was completely shut down until further notice.</p>\r\n\r\n<p>I finally realized what I had done to myself when it was my 21st birthday. I gained roughly 60 pounds in less than a year, and it wasn&#39;t the good kind of weight. I started to see stretch marks near my waist and belly, and the photos that were taken of me during my birthday celebration revealed that&nbsp; I was a person that ate as much food as I wanted, with no self-control. I was mis-using food. I was using food as a source of comfort and pleasure.</p>\r\n\r\n<p>After I completed my fourth year of University, a group of friends and I decided to go on a trip to Cuba. They wanted to celebrate their graduation, while I knew that I had to figure out what program I wanted to switch into the next year. For the time being, I decided to forget about school, and just try to have as much fun as possible while in Cuba. When we arrived, the first thing that stood out about Cuba was the people that live there. I saw people that didn&#39;t have much. They would live in stone homes with no windows. Electricity and internet and other essentials we took for granted were luxuries there. But regardless of who I met, everybody was making the most of their situation and staying positive everyday by smiling and being courteous to their fellow neighbors. Failing courses in university was difficult for me to handle, I had never failed so miserably before in my life. This led me to struggle with my problems in a negative manner, like the whole universe was against me. But once I started exploring and meeting people in Varadero, it made me start to wonder, &#39;What&#39;s stopping me from having a positive attitude?&#39;.&nbsp;&nbsp;</p>\r\n\r\n<p>When I got back from Cuba, I was inspired to make a change. I didn&#39;t need to rely on food to help me deal with my anxiety and stress. I made a commitment to begin a regiment of exercising and eating healthier food. Once I started to pair regular exercise with healthy eating, the work started to pay off. My first&nbsp; goal was to get on the treadmill at least every other day for as long as I could. I also stopped eating junk food and started eating 3-4 home-cooked meals per day. After 2 weeks, I hit a small bump in the road. The treadmill was getting mundane and it was harder to convince myself to use it for exercise. I decided that I needed a different activity, so I invested some money into a bench and a dumbbell set. Once I received my equipment, I repeated the same type of dumbbell workouts my basketball trainer had taught me during my high school days. I also searched through YouTube, and found a channel called &quot;Six Pack Shortcuts&quot; hosted by Mike Chang. I don&#39;t have a six pack, yet, but I picked up some other exercises to incorporate into my workout routine.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p>After a few months, I was losing weight, and it was noticeable. I even started receiving compliments from customers at work. By the end of 2014, I barely looked like myself anymore. I took the next step in my path, and I started the Insanity and Insanity Asylum workout videos and I adopted the Paleo diet. That took a lot of will power, and I&#39;m still proud of myself for sticking with it for 4 months.</p>\r\n\r\n<p>The more I think about my experience with obesity, the more I realize how much my mindset has changed. When I was over-eating, I didn&#39;t see it as a problem in the slightest. It was merely something I did to make myself feel better, why would that be bad? But once I realized how unhappy I was about the way I looked and felt, I know that continuing down that path would just become a vicious cycle. Instead, I started forming healthier ways of treating myself, finding joy and stress-relief through exercise and having discipline, and the change happened organically.</p>\r\n\r\n<p>&nbsp;I know that there are many people who suffer from these unhealthy habits. It&#39;s always easier to take the quick exit. I&#39;m here to share my honest experience and opinion through my Clutch Health blog posts. This will be the first of many posts. I welcome your feedback, and I look forward to sharing the rest of my experience and ideas related to health, exercise, food and mindset.</p>\r\n\r\n<p>&nbsp;To close, here are two quotes (among hundreds) that I live by:</p>\r\n\r\n<p><em>&quot;You are today where your thoughts have brought you; you will be tomorrow where your thoughts take you.&quot; James Allen</em></p>\r\n\r\n<p><em>&quot;If it was EASY, everyone would do it&quot; Eric Thomas (ET The Hip-Hop Preacher)</em></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img src="http://fitness.ms/uploads/media/first.jpg" style="height:307px; width:410px" /></p>\r\n\r\n<p>Me during my 21st Birthday Celebration</p>\r\n\r\n<p><img src="http://fitness.ms/uploads/media/second.jpg" style="height:480px; width:288px" /></p>\r\n\r\n<p>My best results at the end of 2014.</p>\r\n', 'aa602df62a297dd3ac845dff70c24c21.jpg', 2, 'Public', 'Active', '2016-08-14 05:39:18', '2016-08-14 05:39:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blog_metas`
--

CREATE TABLE IF NOT EXISTS `blog_metas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `blog_id` int(10) unsigned NOT NULL,
  `meta_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_value` text COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `blog_metas_blog_id_foreign` (`blog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `blog_tags`
--

CREATE TABLE IF NOT EXISTS `blog_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `blog_tag_relations`
--

CREATE TABLE IF NOT EXISTS `blog_tag_relations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `blog_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `blog_tag_relations_blog_id_foreign` (`blog_id`),
  KEY `blog_tag_relations_tag_id_foreign` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contracts`
--

CREATE TABLE IF NOT EXISTS `contracts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `payment_mode_id` int(10) unsigned NOT NULL,
  `budget` int(11) NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `trainer_id` int(11) NOT NULL,
  `status` enum('Draft','Posting','Selected') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Posting',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contracts_payment_mode_id_foreign` (`payment_mode_id`),
  KEY `contracts_customer_id_foreign` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contract_metas`
--

CREATE TABLE IF NOT EXISTS `contract_metas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `contract_id` int(10) unsigned NOT NULL,
  `meta_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_value` text COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contract_metas_contract_id_foreign` (`contract_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE IF NOT EXISTS `faqs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `answer` text COLLATE utf8_unicode_ci NOT NULL,
  `sort_order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from_id` int(10) unsigned NOT NULL,
  `to_id` int(10) unsigned NOT NULL,
  `subject` text COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `is_read` enum('No','Yes') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `deleted_by_from` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `deleted_by_to` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `messages_from_id_foreign` (`from_id`),
  KEY `messages_to_id_foreign` (`to_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `message_metas`
--

CREATE TABLE IF NOT EXISTS `message_metas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message_id` int(10) unsigned NOT NULL,
  `meta_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_value` text COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `message_metas_message_id_foreign` (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_06_22_135159_create_roles_table', 1),
('2016_06_22_135161_create_payment_modes_table', 1),
('2016_06_22_135305_create_users_table', 1),
('2016_06_22_140734_create_user_metas_table', 1),
('2016_06_22_142313_create_messages_table', 1),
('2016_06_22_142716_create_message_metas_table', 1),
('2016_06_22_142931_create_articles_table', 1),
('2016_06_22_143030_create_article_metas_table', 1),
('2016_06_22_143414_create_contracts_table', 1),
('2016_06_22_143427_create_contract_metas_table', 1),
('2016_06_22_143440_create_password_resets_table', 1),
('2016_08_06_143440_create_allowed_logins', 2),
('2016_08_13_020835_create_blog_tags_table', 3),
('2016_08_13_021237_create_blogs_table', 3),
('2016_08_13_021747_create_blog_tag_relations_table', 3),
('2016_08_13_022955_create_blog_metas_table', 3),
('2016_08_13_025450_create_faqs_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_modes`
--

CREATE TABLE IF NOT EXISTS `payment_modes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `interval_value` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `payment_modes`
--

INSERT INTO `payment_modes` (`id`, `title`, `interval_value`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Not Defined', 0, NULL, NULL, NULL),
(2, 'Fixed\r\n', 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Super Admin', 'Active', NULL, NULL, NULL),
(2, 'Trainer', 'Active', NULL, NULL, NULL),
(3, 'Customer', 'Active', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `payment_mode_id` int(10) unsigned NOT NULL DEFAULT '1',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `suburb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `budget` double NOT NULL,
  `training_area_radius` double NOT NULL,
  `requirement` text COLLATE utf8_unicode_ci NOT NULL,
  `is_public` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Yes',
  `status` enum('Pending','Reject','Active','Ban') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Pending',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  KEY `users_payment_mode_id_foreign` (`payment_mode_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `role_id`, `payment_mode_id`, `email`, `password`, `date_of_birth`, `phone`, `longitude`, `latitude`, `postal`, `city`, `suburb`, `state`, `country`, `address`, `budget`, `training_area_radius`, `requirement`, `is_public`, `status`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Krishna Bhatta', 1, 2, 'krishna_bhatta@outlook.com', '$2y$10$bFE3FSdB6D3X1lyQNn4Czu1Sz6iFpSMBqy/sMhFfuw.8k/5n1UBhu', '1989-11-30', '', '83.74180000000001', '28.392889198488714', '', '', '', '', '', '', 500, 0, '', 'Yes', 'Active', '6xWo6R2L4QHjYm3pa2LDWcbxNDAPVdqBINHMliQmMg6j1dqoB6mE0YV2Rlnr', '2016-06-23 06:09:13', '2016-08-14 07:48:25', NULL),
(3, 'Tom Hardy', 3, 2, 'tom@hardy.com', '$2y$10$RtYTYX2Ejhw2bdp3w3zAyuxwN/sgy/QghFhuz96cdaSJ/.S5M.qcK', '1984-06-12', '', '-79.38319999999999', '43.6532', '', '', '', '', '', 'Toronto City Hall, 100 Queen St W, Toronto, ON M5G 1P5, Canada', 500, 6, '', 'Yes', 'Active', 'lMIw6NVdyzjtzhf6Rv3vEM75wvtK0v3rKWBUEpmFnyn6ZzSdIT6CltJFo4mg', '2016-06-23 06:10:08', '2016-08-14 16:12:20', NULL),
(4, 'Ben Affleck', 2, 1, 'ben@yahoo.com', '$2y$10$FUff5DllMFMKNoxZEkcDWOCAnBhdh4w5NQ19HfkJNSD4cOuC9dIJq', '1970-02-03', '', '-79.38319999999999', '43.6532', '', '', '', '', '', 'Toronto City Hall, 100 Queen St W, Toronto, ON M5G 1P5, Canada', 0, 0, '', 'Yes', 'Active', 'GAFGAKEHobYkLEAP5uOqCjBiUZYdBKEA2uhyqCIRz51A07rycBicslplpeb2', '2016-06-23 06:17:28', '2016-06-23 07:49:50', NULL),
(5, 'Jack Harper', 2, 1, 'jack@outlook.com', '$2y$10$eDnAwFwkd6KOeIGKQR.gluA5u6F7.kXEEuDGS9K/ndpEm/veGlTLi', '1970-08-05', '', '83.74180000000001', '28.392889198488714', '', '', '', '', '', 'Kharibot, Banasthali', 0, 0, '', 'Yes', 'Active', '9CtuatBuLfchnOJ2W2HUBvczSuUSuDpALqIY66zEyQ1u2JZPrnkXNUzHcaT7', '2016-07-31 10:25:34', '2016-07-31 10:31:06', NULL),
(17, 'Krishna Bhatta', 2, 1, 'atom@hardy.com', '$2y$10$1.LxSrK1s1gcJ.j0pRM9Ne53iu/W7YyCiPrl75n3uF0KLm6gn1BR2', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, 0, '', 'Yes', 'Pending', NULL, '2016-08-06 08:46:02', '2016-08-06 08:46:02', NULL),
(18, 'Krishna Bhatta', 2, 1, 'tom2@hardy.com', '$2y$10$rF/Wkim4lY3ND2bLulJZVeu3Jy8/02qAffpGdBtvObG/9NAyRPcC2', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, 0, '', 'Yes', 'Pending', 'Q6E209U1OAYvTvoWEzd5n0bYpUDOKN9RrYYNESC4isBRPjPcCUPDZYfzDSBE', '2016-08-06 08:52:57', '2016-08-06 08:54:47', NULL),
(20, 'Test', 2, 1, 'teaast@test.com', '$2y$10$YcE9/Nvt6VKch09AY.AyM.2PTl2HltPq62O/WbVT4AUOnaxrhm1ci', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, 0, '', 'Yes', 'Pending', 'tfxv2aT6l4DbCAaS0MOkvb6Sx2LMRKeEpNw3NhGvY5Lng7IFYRtou7wpi4UH', '2016-08-06 08:59:47', '2016-08-06 09:00:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_metas`
--

CREATE TABLE IF NOT EXISTS `user_metas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `meta_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_value` text COLLATE utf8_unicode_ci,
  `status` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_metas_user_id_foreign` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=24 ;

--
-- Dumping data for table `user_metas`
--

INSERT INTO `user_metas` (`id`, `user_id`, `meta_key`, `meta_value`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 'profile_photo', '3c797634d66b4094d8da5c28e3ccd2b2.jpg', 'Active', NULL, NULL, NULL),
(2, 3, 'description', 'Edward Thomas "Tom" Hardy[1] (born 15 September 1977)[2] is an English actor, screenwriter, and producer. He made his debut in Ridley Scott''s 2001 action film Black Hawk Down. Hardy''s other notable films include the science fiction film Star Trek: Nemesis (2002), the crime film RocknRolla (2008), biographical psychological drama Bronson (2008), science fiction thriller Inception (2010), sports drama Warrior (2011), Cold War espionage film Tinker Tailor Soldier Spy (2011), crime drama Lawless (2012), drama Locke (2013), mobster film The Drop (2014), and the biographical western thriller The Revenant (2015), for which he received an Academy Award nomination for Best Supporting Actor. He also portrayed Bane in the superhero film The Dark Knight Rises (2012), "Mad" Max Rockatansky in the post-apocalyptic film Mad Max: Fury Road (2015), and both Kray twins in the crime thriller Legend (2015).\r\nHardy''s television roles include the HBO war drama miniseries Band of Brothers (2001), the BBC historical drama miniseries The Virgin Queen (2005), ITV''s Wuthering Heights (2008), the Sky 1 drama series The Take (2009), and the BBC British historical crime drama television series Peaky Blinders (2013).\r\nHardy has also performed on British and American stages. He was nominated for the Laurence Olivier Award for Most Promising Newcomer for his role as Skank in the 2003 production of In Arabia We''d All Be Kings, and was awarded the 2003 London Evening Standard Theatre Award for Outstanding Newcomer for his performances in both In Arabia We''d All be Kings and for his role as Luca in Blood. He starred in the 2007 production of The Man of Mode and received positive reviews for his role in the 2010 Philip Seymour Hoffman-directed play The Long Red Road.', 'Active', NULL, NULL, NULL),
(3, 4, 'profile_photo', '8336cb0249e0b0fab3d2259f60f99a12.jpg', 'Active', NULL, NULL, NULL),
(4, 4, 'description', 'Benjamin Geza Affleck-Boldt (born August 15, 1972), better known as Ben Affleck, is an American actor and filmmaker. He began his career as a child actor, starring in the PBS educational series The Voyage of the Mimi (1984, 1988). He later appeared in the coming-of-age comedy Dazed and Confused (1993) and various Kevin Smith films including Chasing Amy (1997) and Dogma (1999). Affleck gained fame when he and childhood friend Matt Damon won the Golden Globe and Academy Award for Best Original Screenplay for Good Will Hunting (1997).', 'Active', NULL, NULL, NULL),
(5, 3, 'awards', 'Best,Nice,Awesome', 'Active', NULL, NULL, NULL),
(6, 4, 'awards', 'Celebrity trainer', 'Active', NULL, NULL, NULL),
(7, 4, 'training_start', '2005', 'Active', NULL, NULL, NULL),
(8, 4, 'certificates', 'Training class', 'Active', NULL, NULL, NULL),
(9, 3, 'training_start', '2008', 'Active', NULL, NULL, NULL),
(10, 3, 'certificates', 'Training for trainer', 'Active', NULL, NULL, NULL),
(11, 3, 'specifications', 'Strangth,Lean Body,Bulk Body', 'Active', NULL, NULL, NULL),
(12, 5, 'profile_photo', '4127734a54370341670c126ad0edebff.jpg', 'Active', NULL, NULL, NULL),
(13, 5, 'description', 'Benjamin Geza Affleck-Boldt (born August 15, 1972), better known as Ben Affleck, is an American actor and filmmaker. He began his career as a child actor, starring in the PBS educational series The Voyage of the Mimi (1984, 1988). He later appeared in the coming-of-age comedy Dazed and Confused (1993) and various Kevin Smith films including Chasing Amy (1997) and Dogma (1999). Affleck gained fame when he and childhood friend Matt Damon won the Golden Globe and Academy Award for Best Original Screenplay for Good Will Hunting (1997).', 'Active', NULL, NULL, NULL),
(14, 5, 'awards', '', 'Active', NULL, NULL, NULL),
(15, 5, 'training_start', '', 'Active', NULL, NULL, NULL),
(16, 5, 'certificates', '', 'Active', NULL, NULL, NULL),
(17, 5, 'specifications', '', 'Active', NULL, NULL, NULL),
(18, 2, 'description', 'Benjamin Geza Affleck-Boldt (born August 15, 1972), better known as Ben Affleck, is an American actor and filmmaker. He began his career as a child actor, starring in the PBS educational series The Voyage of the Mimi (1984, 1988). He later appeared in the coming-of-age comedy Dazed and Confused (1993) and various Kevin Smith films including Chasing Amy (1997) and Dogma (1999). Affleck gained fame when he and childhood friend Matt Damon won the Golden Globe and Academy Award for Best Original Screenplay for Good Will Hunting (1997).', 'Active', NULL, NULL, NULL),
(19, 2, 'awards', NULL, 'Active', NULL, NULL, NULL),
(20, 2, 'training_start', NULL, 'Active', NULL, NULL, NULL),
(21, 2, 'certificates', NULL, 'Active', NULL, NULL, NULL),
(22, 2, 'specifications', NULL, 'Active', NULL, NULL, NULL),
(23, 2, 'profile_photo', '2f2deda515473a40ca006bf563da3dfa.jpg', 'Active', NULL, NULL, NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `allowed_logins`
--
ALTER TABLE `allowed_logins`
  ADD CONSTRAINT `allowed_logins_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `articles_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `article_metas`
--
ALTER TABLE `article_metas`
  ADD CONSTRAINT `article_metas_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`);

--
-- Constraints for table `blogs`
--
ALTER TABLE `blogs`
  ADD CONSTRAINT `blogs_author_foreign` FOREIGN KEY (`author`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blog_metas`
--
ALTER TABLE `blog_metas`
  ADD CONSTRAINT `blog_metas_blog_id_foreign` FOREIGN KEY (`blog_id`) REFERENCES `blogs` (`id`);

--
-- Constraints for table `blog_tag_relations`
--
ALTER TABLE `blog_tag_relations`
  ADD CONSTRAINT `blog_tag_relations_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `blog_tags` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blog_tag_relations_blog_id_foreign` FOREIGN KEY (`blog_id`) REFERENCES `blog_tags` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `contracts`
--
ALTER TABLE `contracts`
  ADD CONSTRAINT `contracts_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `contracts_payment_mode_id_foreign` FOREIGN KEY (`payment_mode_id`) REFERENCES `payment_modes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `contract_metas`
--
ALTER TABLE `contract_metas`
  ADD CONSTRAINT `contract_metas_contract_id_foreign` FOREIGN KEY (`contract_id`) REFERENCES `contracts` (`id`);

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_from_id_foreign` FOREIGN KEY (`from_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `messages_to_id_foreign` FOREIGN KEY (`to_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `message_metas`
--
ALTER TABLE `message_metas`
  ADD CONSTRAINT `message_metas_message_id_foreign` FOREIGN KEY (`message_id`) REFERENCES `messages` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_payment_mode_id_foreign` FOREIGN KEY (`payment_mode_id`) REFERENCES `payment_modes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_metas`
--
ALTER TABLE `user_metas`
  ADD CONSTRAINT `user_metas_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
