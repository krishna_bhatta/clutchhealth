<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;
    
    protected $fillable = [
        'name', 'role_id', 'email', 'password', 'payment_mode_id', 'budget', 'training_area_radius', 'requirement', 'phone', 'is_public', 'longitude', 'latitude', 'postal', 'city', 'suburb', 'state', 'country', 'address', 'approved_date', 'date_of_birth', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
    * Get the role record associated with the user.
    */
   public function role()
   {
       return $this->belongsTo('App\Role', 'role_id', 'id');
   }

    /**
    * Get the payment mode record associated with the user.
    */
   public function payment_mode()
   {
       return $this->belongsTo('App\PaymentMode');
   }

   /**
    * Get the meta value for the user.
    */
   public function meta()
   {
       return $this->hasMany('App\UserMeta');
   }

   /**
    * Get stripe account details
    */
   public function stripe()
   {
       return $this->hasOne('App\StripeAccount', 'id', 'trainer_id');
   }

   /**
    * Get the reward value for the user.
    */
   public function reward()
   {
       return $this->hasMany('App\Reward');
   }
}
