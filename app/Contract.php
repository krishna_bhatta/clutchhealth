<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
   /**
    * Get the meta value for the contract.
    */
   public function meta()
   {
       return $this->hasMany('App\ContractMeta');
   }
   /**
    * Get trainer details of contract
    */
   public function trainer()
   {
       return $this->hasOne('App\User', 'id', 'trainer_id');
   }
   /**
    * Get customer details of contract
    */
   public function customer()
   {
       return $this->hasOne('App\User', 'id', 'customer_id');
   }
   /**
    * Get payment mode
    */
   public function payment_mode()
   {
       return $this->hasOne('App\PaymentMode', 'id', 'payment_mode_id');
   }
}
