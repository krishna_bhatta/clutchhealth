<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{

   /**
    * Get the meta value for the blog.
    */
   public function meta()
   {
       return $this->hasMany('App\BlogMeta');
   }
}
