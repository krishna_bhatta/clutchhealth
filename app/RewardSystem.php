<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RewardSystem extends Model
{
    public function role()
    {
        return $this->belongsTo('App\Role', 'role_id', 'id');
    }
}
