<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    public function rewardSystem()
    {
        return $this->belongsTo('App\RewardSystem', 'reward_system_id', 'id');
    }
}
