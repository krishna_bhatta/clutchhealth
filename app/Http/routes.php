<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::get('/fb-redirect', 'SocialAuthController@fbRedirect');
Route::get('/fb-callback', 'SocialAuthController@fbCallback');

Route::get('/twitter-redirect', 'SocialAuthController@twitterRedirect');
Route::get('/twitter-callback', 'SocialAuthController@twitterCallback');

Route::get('/google-redirect', 'SocialAuthController@googleRedirect');
Route::get('/google-callback', 'SocialAuthController@googleCallback');

Route::get('/linkedin-redirect', 'SocialAuthController@linkedinRedirect');
Route::get('/linkedin-callback', 'SocialAuthController@linkedinCallback');

Route::get('/view/{article_alias}', 'ArticleController@index');

Route::get('/contact-us', 'ArticleController@contact_us');
Route::get('/about-us', 'ArticleController@about_us');
Route::get('/privacy-policy', 'ArticleController@privacy_policy');
Route::get('/terms-and-conditions', 'ArticleController@terms_and_conditions');

Route::post('/submit-contact', 'ArticleController@contact_us');

Route::get('/blog', 'BlogController@index');
Route::get('/blog/{blog_alias}', 'BlogController@index');

Route::get('/find-trainer', 'HomeController@findTrainer');
Route::post('/find-trainer-filter', 'HomeController@findTrainer');

Route::get('/coach', 'HomeController@coach');
Route::get('/exercise', 'HomeController@exercise');

Route::post('/answer-action', 'AnswerController@answer_action');

Route::get('/coming-soon', function() {
    return view('frontend.coming-soon');
});

Route::get('/coach/{display}', 'UserController@profile');
Route::get('/profile/{display}', 'UserController@profile');
Route::get('/confirm-registration', 'UserController@confirm');
Route::get('/create-trainer-accounts', 'StripeController@create_trainer_accounts');
// Route::get('faq', 'ArticleController@faq');

Route::group(
    ['middleware' => 'front'],
    function() {
        Route::get('/user/profile', 'UserController@profile');
        Route::post('/user/updateProfileAction', 'UserController@edit');
        Route::get('/user/preference', 'UserController@preference');  

        Route::get('/dashboard', 'UserController@dashboard');
        Route::get('/contracts', 'ContractController@index');
        Route::get('/contract/view/{contract_id}', 'ContractController@view');
        Route::get('/contract/history', 'ContractController@history');
        Route::get('/contract/my-trainer', 'ContractController@my_trainer');
        Route::get('/contract/compose/{user_id}', 'ContractController@compose');
        Route::post('/contract/submitContract', 'ContractController@submitContract');
        Route::get('/contract/accept/{contract_id}', 'ContractController@accept');
        Route::get('/contract/reject/{contract_id}/{rejected}', 'ContractController@accept');

        Route::get('/messages', 'MessageController@index');
        Route::get('/message/compose', 'MessageController@compose');
        Route::get('/message/compose/{user_id}', 'MessageController@compose');

        Route::get('/change-role', 'UserController@changeRole');
        Route::post('/change-role-action', 'UserController@changeRoleAction');

        Route::get('/payment-settings','StripeController@index');
        Route::get('/add-payment-method','StripeController@add_payment_method');
        Route::post('/add-payment-method-process','StripeController@add_payment_method_process');
        Route::get('/pay', 'StripeController@pay');
        Route::post('/pay-process', 'StripeController@pay_process');
    }
);

Route::group(
    ['middleware' => 'super', 'prefix' => 'admin'],
    function() {
        Route::get('/home', 'Admin\HomeController@index');

        Route::get('/users', 'Admin\UserController@index');
        Route::get('/profile/{id}', 'Admin\UserController@profile');
        Route::get('/edit', 'Admin\UserController@edit');
        Route::post('/profileUpdateAction', 'Admin\UserController@edit');
        Route::get('/user/triggerStatus/{id}', 'Admin\UserController@triggerStatus');

        Route::get('/message/', 'Admin\MessageController@index');
        Route::get('/message/compose', 'Admin\MessageController@compose');
        Route::get('/message/sent', 'Admin\MessageController@sent');
        Route::get('/message/trash', 'Admin\MessageController@trash');
        Route::post('/admin/submitCompose', 'Admin\MessageController@submitCompose');
        Route::get('/message/read/{id}', 'Admin\MessageController@read');

        Route::get('/pages/', 'Admin\ArticleController@index');
        Route::get('/page/add', 'Admin\ArticleController@add');
        Route::post('/page/addAction', 'Admin\ArticleController@addAction');
        Route::get('/page/edit/{id}', 'Admin\ArticleController@edit');
        Route::post('/page/editAction/{id}', 'Admin\ArticleController@editAction');
        Route::get('/page/delete/{id}', 'Admin\ArticleController@delete');

        Route::get('/questions/', 'Admin\QuestionController@index');
        Route::get('/question/add', 'Admin\QuestionController@add');
        Route::post('/question/addAction', 'Admin\QuestionController@addAction');
        Route::get('/question/edit/{id}', 'Admin\QuestionController@edit');
        Route::post('/question/editAction/{id}', 'Admin\QuestionController@editAction');
        Route::get('/question/delete/{id}', 'Admin\QuestionController@delete');

        Route::get('/answers', 'Admin\QuestionController@view_answer');

        Route::get('/rewards/', 'Admin\RewardController@index');
        Route::get('/reward/add', 'Admin\RewardController@add');
        Route::post('/reward/addAction', 'Admin\RewardController@addAction');
        Route::get('/reward/edit/{id}', 'Admin\RewardController@edit');
        Route::post('/reward/editAction/{id}', 'Admin\RewardController@editAction');
        Route::get('/reward/delete/{id}', 'Admin\RewardController@delete');

        Route::get('/blogs/', 'Admin\BlogController@index');
        Route::get('/blog/add', 'Admin\BlogController@add');
        Route::post('/blog/addAction', 'Admin\BlogController@addAction');
        Route::get('/blog/edit/{id}', 'Admin\BlogController@edit');
        Route::post('/blog/editAction/{id}', 'Admin\BlogController@editAction');
        Route::get('/blog/delete/{id}', 'Admin\BlogController@delete');
        Route::get('/blog/tags', 'Admin\BlogController@tags');

        Route::get('/faqs/', 'Admin\FaqController@index');
        Route::get('/faq/add', 'Admin\FaqController@add');
        Route::post('/faq/addAction', 'Admin\FaqController@addAction');
        Route::get('/faq/edit/{id}', 'Admin\FaqController@edit');
        Route::post('/faq/editAction/{id}', 'Admin\FaqController@editAction');
        Route::get('/faq/delete/{id}', 'Admin\FaqController@delete');

        Route::get('/broadcast-email', 'Admin\MessageController@BroadcastMessage');
        Route::post('/broadcast-email-action', 'Admin\MessageController@BroadcastMessage');
    }
);
