<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class FrontMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()):
            if(Auth::user()->role_id == 2 || Auth::user()->role_id == 3):
                return $next($request);
            endif;
        endif;

        if ($request->ajax() || $request->wantsJson()):
            return response('Unauthorized.', 401);
        else:
            $request->session()->flash('warning', 'This area is not accessable to you.');
            return redirect()->guest('login');
        endif;
    }
}
