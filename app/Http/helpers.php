<?php

/**
 * Getting meta value by given param
 * @param  string $table       
 * @param  string $primary_key 
 * @param  integer $primary_id  
 * @param  string $meta_key    
 * @return string              
 */
function meta($table, $primary_key, $primary_id, $meta_key, $default = FALSE) {
    $row = DB::table($table)
                 ->select('meta_value')
                 ->where($primary_key, '=', $primary_id)
                 ->where('meta_key', '=', $meta_key)
                 ->first();
    if($row)
        return $row->meta_value;

    return $default;
}


/**
 * Update meta value by given param
 * @param  string $table       
 * @param  string $primary_key 
 * @param  integer $primary_id  
 * @param  string $meta_key    
 * @param  string $meta_value
 * @return string              
 */
function insert_meta($table, $primary_key, $primary_id, $meta_key, $meta_value) {
	$row = DB::table($table)
                 ->select('meta_value')
                 ->where($primary_key, '=', $primary_id)
                 ->where('meta_key', '=', $meta_key)
                 ->first();
    if($row):
        DB::table($table)
             ->select('meta_value')
             ->where($primary_key, '=', $primary_id)
             ->where('meta_key', '=', $meta_key)
             ->update(['meta_value' => $meta_value]);
    else:
        DB::table($table)->insert(
            array($primary_key => $primary_id,
                  'meta_key' => $meta_key,
                  'meta_value' => $meta_value)
        );
    endif;

    return false;
}

function unread_inbox($user_id) {
	return DB::table('messages')
	                ->where('to_id', $user_id)
	                ->where('is_read', 'No')
	                ->count();
}

function addPoint($user_id, $system_id, $status, $remarks) {    
    $query = DB::table('users')
                 ->select('role_id')
                 ->where('id', $user_id)
                 ->where('status', 'Active');
    if(!$query->exists())
        return FALSE;

    $role_id = $query->first()->role_id;

    $query = DB::table('reward_systems')
                 ->select('point')
                 ->where('id', $system_id)
                 ->where('role_id', $role_id)
                 ->where('status', 'Active');
    if(!$query->exists())
        return FALSE;

    $reward_system = $query->first();

    return DB::table('rewards')->insert(
        ['user_id' => $user_id, 'reward_system_id' => $system_id, 'points' => $reward_system->point, 'remarks' => $remarks, 'status' => $status, 'created_at' => date('Y-m-d H:i:s')]
    );

}

function survey_questions($user_id = FALSE) {
    $query = DB::table('answers')
                 ->where('ip_address', $_SERVER['REMOTE_ADDR']);
    if($query->exists())
        return FALSE;

    if ($user_id):
        $query = DB::table('answers')
                ->where('user_id', '!=', $user_id);
        if($query->exists())
            return FALSE;
    endif;

    $query = DB::table('questions')
                    ->where('status', 'Active');

    if(!$query->exists())
        return FALSE;

    return $query->get();                    
}