<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.2/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

use App\Message;

use App\User;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class MessageController extends Controller
{

    /**
     * Show the message dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $where = ['to_id' => Auth::id(), 'deleted_by_to' => 'No'];
        $data['messages'] = Message::where($where)->get();
        $data['nav'] = 'home';
        return view('frontend.message.index', $data);
    }

    public function compose($user_id = FALSE) {
        $data['users'] = User::where('id', '!=', Auth::id())->get();
        $data['nav'] = 'message';
        $data['title'] = 'Inbox';
        $data['receipt_id'] = $user_id;
        return view('frontend/message/compose', $data);
    }

    public function submitCompose(Request $request) {

        $messages = [
            'to.required' => 'Recipient field is required.',
        ];

        $validator = Validator::make($request->all(), [
            'to' => 'required|numeric',
            'subject' => 'required|string',
            'message' => 'required|string'
        ], $messages);

        if ($validator->fails()) {
            return redirect('message/compose')
                        ->withErrors($validator)
                        ->withInput();
        }

       $flight = new Message;
       $flight->to_id = $request->to;
       $flight->from_id = Auth::id();
       $flight->subject = $request->subject;
       $flight->message = $request->message;
       $flight->save();

       return redirect('message')->with('success', 'Message successfully sent.');
    }
}