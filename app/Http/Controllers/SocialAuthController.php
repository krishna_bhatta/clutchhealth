<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\AllowedLogin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Socialite;
use Image;

use Auth;

class SocialAuthController extends Controller
{
	public function __construct(Socialite $socialite){
       $this->socialite = $socialite;
   }

   private function socialRegistration($data,$login_type) {
    $allowedLogin = AllowedLogin::where('social_id', $data['id'])->where('login_type', $login_type);
    if($allowedLogin->exists()):
        $user = User::where('id', $allowedLogin->first()->user_id);
        Auth::loginUsingId($allowedLogin->first()->user_id);
        if(Auth::user()->status == 'Process'):
            return TRUE;
        endif;

        return FALSE;
    else:
        $user = User::where('email', $data['email']);
        if($user->exists()):
            $allowedLogins = new AllowedLogin;
            $allowedLogins->user_id = $user->first()->id;
            $allowedLogins->login_type = $login_type;
            $allowedLogins->social_id = $data['id'];
            $allowedLogins->save();
            Auth::loginUsingId($user->first()->id);
            return FALSE;
        else:
            $fileName = md5($_SERVER['REMOTE_ADDR'].time()).'.png';
            // Image::make($data['picture'])->resize(160, 160)->save(public_path('uploads/profile/' . $fileName));
            Image::make($data['picture'])->resize(160, 160)->save('../public_html/uploads/profile/' . $fileName);
            $user = new User;
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->role_id = 2;
            $user->status = 'Process';
            $user->save();

            addPoint($user->id, 3, 'Active', 'Profile verified');

            // insert_meta('user_metas', 'user_id', $user->id, 'profile_photo', $fileName);

            $allowedLogins = new AllowedLogin;
            $allowedLogins->user_id = $user->id;
            $allowedLogins->login_type = $login_type;
            $allowedLogins->social_id = $data['id'];
            $allowedLogins->save();
            Auth::loginUsingId($user->id);
            return TRUE;
        endif;
    endif;
   }

    public function fbRedirect() {
        return Socialite::driver('facebook')->redirect();
    }

    public function fbCallback() {
        $providerUser = Socialite::driver('facebook')->user();
        $data = $providerUser->user;
        $picture_array = explode('?', $providerUser->avatar_original);
        $data['picture'] = $picture_array[0].'?width=160';

        if(!isset($data['email'])):
            return redirect('register')->withInput($data);
        endif;

        if($data['email'] == ""):
            return redirect('register')->withInput($data);
        endif;

        $isNew = $this->socialRegistration($data,'Facebook');

        if($isNew):
            return redirect()->intended('change-role');
        endif;

        return redirect()->intended('dashboard');
    }

    public function twitterRedirect() {
        return Socialite::driver('twitter')->redirect();
    }

    public function twitterCallback() {
        $providerUser = Socialite::driver('twitter')->user();

        $data = array(
            'name' => $providerUser->name,
            'id' => $providerUser->id,
            'picture' => $providerUser->avatar_original
            );
        if($providerUser->email == ""):
            return redirect('register')->withInput($data);
        else:
            $data['email'] = $providerUser->email;
        endif;

        $isNew = $this->socialRegistration($data,'Twitter');
        if($isNew):
            return redirect()->intended('change-role');
        else:
            return redirect()->intended('dashboard');
        endif;
    }

    public function googleRedirect() {
        return Socialite::driver('google')->redirect();
    }

    public function googleCallback() {
        $providerUser = Socialite::driver('google')->user();
        $picture_array = explode('?', $providerUser->avatar);

        $user = $providerUser->user;
        $data = array(
            'name' => $user['displayName'],
            'email' => $user['emails'][0]['value'],
            'id' => $user['id'],
            'picture' => $picture_array[0].'?width=160'
            );
        $isNew = $this->socialRegistration($data,'Google');
        if($isNew):
            return redirect()->intended('change-role');
        else:
            return redirect()->intended('dashboard');
        endif;
    }

    public function linkedinRedirect() {
        return Socialite::driver('linkedin')->redirect();
    }

    public function linkedinCallback() {
    	$providerUser = Socialite::driver('linkedin')->user();

        $user = $providerUser->user;
        $data = array(
            'name' => $user['formattedName'],
            'email' => $user['emailAddress'],
            'id' => $user['id'],
            'picture' => $user['pictureUrls']['values'][0]
            );
        $isNew = $this->socialRegistration($data,'LinkedIn');
        if($isNew):
            return redirect()->intended('change-role');
        else:
            return redirect()->intended('dashboard');
        endif;
    }
}