<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\PayPalLib;

use Session;

class PayPalController extends Controller {
    private $config = array(
    	"environment" => "sandbox", # or live
        "userid" => "hagiachren-facilitator_api1.hotmail.com", 
        "password" => "MUR7SELTYK2G7FQ2", 
        "signature" => "AFcWxV21C7fd0v3bYYYRCpSSRl31AYbuJg1aAqI0JAebtwx3huyH45-Y"
    // "appid" => "", # You can set this when you go live
        );
    
    public function index() {
        
        $paypal = new PayPalLib($this->config);
        
        $result = $paypal->call(array(
            'actionType' => 'PAY',
            'currencyCode' => 'USD',
            'feesPayer' => 'EACHRECEIVER',
            'memo' => 'Payment for trainer',
            
            'cancelUrl' => url('canclePayment'),
            'returnUrl' => url('successPayment'),
            
            'receiverList' => array(
                'receiver' => array(
                    array(
                        'amount' => '200.00',
                        'email' => 'hagiachren-facilitator@hotmail.com',
                        'primary' => 'true'
                    ),
                    array(
                        'amount' => '50.00',
                        'email' => 'us-provider@commercefactory.org',
                        'primary' => 'false'
                    )
                )
            )
        ), 'Pay');
        
        if ($result['responseEnvelope']['ack'] == 'Success' && @$result["payKey"]) {
            Session::set('payKey', $result["payKey"]);
            // if(@$result["payKey"]):
            // 	$redirect_url = sprintf("%s?cmd=_ap-payment&paykey=%s", $paypal->redirect_url(), $result["payKey"]);
            // else:
            // 	$redirect_url = sprintf("%s?cmd=_ap-preapproval&preapprovalkey=%s", $paypal->redirect_url(), $result["preapprovalKey"]);
            // endif;

            $redirect_url = sprintf("%s?cmd=_ap-payment&paykey=%s", $paypal->redirect_url(), $result["payKey"]);
            return redirect()->away($redirect_url);

        } else {
            echo $result['error'][0]['message'];
        }
    }

    public function success() {
        sleep(10);
        $payKey = Session::get('payKey');
        if($payKey != ""):
            Session::forget('payKey');
            $paypal = new PayPalLib($this->config);
            $data = array(
                'actionType'  => 'Pay',
                'payKey'  => $payKey,
              );
            $result = $paypal->call($data, "PaymentDetails");

            if ($result['responseEnvelope']['ack'] == "Success" && $result['status'] == "COMPLETED"):
                echo "<pre>";
                print_r($result);
                echo serialize($result);
            else:
                echo 'Handle payment execution failure';
            endif;
        else:
            echo 'Handle payment execution failure';
        endif;
    }

    public function cancle() {
    	echo "User cancled payment.";
    }
}