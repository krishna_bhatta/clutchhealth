<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Answer;

use Auth;

class AnswerController extends Controller
{
    function answer_action(Request $request) {
    	if(sizeof($request->questions) == 0):
    		$notify = (object) array();
    		$notify->title = 'Error';
    		$notify->type = 'error';
    		$notify->text = 'Unable to receive your answer.';

    		return redirect('/')->with('notify', $notify);
    	endif;

    	foreach($request->questions as $question_id):
			$answer = new Answer;
			$ans_var = "answer_".$question_id;
			if($request->$ans_var != ""):
                if (Auth::check()):
                    $answer->user_id = Auth::user()->id;
                endif;
		    	$answer->ip_address = $request->ip();
		    	$answer->question_id = $question_id;
		    	$answer->answer = $request->$ans_var;
		    	$answer->save();
	    	endif;
    	endforeach;

    	$notify = (object) array();
    	$notify->title = 'Success';
    	$notify->type = 'success';
    	$notify->text = 'Thank you for participating on our survery.';

    	return redirect('/')->with('notify', $notify);
    }
}
