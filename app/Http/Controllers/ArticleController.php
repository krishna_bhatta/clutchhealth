<?php

namespace App\Http\Controllers;

use App\Article;
use App\Faq;
use Validator;
use Auth;
use Session;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    public function index($article_alias) {
    	$article = Article::where('alias', $article_alias);
    	if($article->exists()):
    		$data['article'] = $article->first();
    		return view('frontend/article/view', $data);
    	else:
    		abort(404);
    	endif;
    }

    public function contact_us() {
        return view('frontend.article.contact-us');
    }

    public function about_us() {
        return $this->index('about-us');
    }

    public function faq() {
    	$faq = Faq::where("id", "!=", "");
    	if($faq->exists()):
    		$data['faq'] = Faq::all();
    		return view('frontend/article/faq', $data);
    	else:
    		abort(404);
    	endif;
    }

    public function privacy_policy() {
        return $this->index('privacy-policy');
    }

    public function terms_and_conditions() {
        return $this->index('terms-and-conditions');
    }
}
