<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Purchase;

use App\User;
use App\Contract;
use App\StripeAccount;
use App\Transaction;

use Auth;

use Exception;

use File;

use App\AcceptedPayment;

use Validator;

use Mail;

class StripeController extends Controller {

    public function index() {        
        if(Auth::user()->role_id != 2):
            abort(404);
        endif;
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $data['user'] = Auth::user();
        $data['paymentMethod'] = AcceptedPayment::where('user_id', Auth::user()->id)->get();
        return view('frontend/user/payment-settings', $data);
    }

    public function add_payment_method() {
        if(Auth::user()->role_id != 2):
            abort(404);
        endif;
        $user = Auth::user();
        $name_array = explode(' ', $user->name);
        $user->first_name = (isset($name_array[0]))?$name_array[0]:"";
        $user->last_name = (isset($name_array[1]))?$name_array[1]:"";
        if($user->date_of_birth == "0000-00-00"):
            $user->date_of_birth = "";
        endif;
        $data['user'] = $user;
        $data['canadian_states'] = array( 
            "BC" => "British Columbia", 
            "ON" => "Ontario", 
            "NL" => "Newfoundland and Labrador", 
            "NS" => "Nova Scotia", 
            "PE" => "Prince Edward Island", 
            "NB" => "New Brunswick", 
            "QC" => "Quebec", 
            "MB" => "Manitoba", 
            "SK" => "Saskatchewan", 
            "AB" => "Alberta", 
            "NT" => "Northwest Territories", 
            "NU" => "Nunavut",
            "YT" => "Yukon Territory"
        );
        return view('frontend/user/add-payment-method', $data);
    }

    public function add_payment_method_process(Request $request) {
        $validation =  [
            'external_account' => 'required|max:255',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'address1' => 'required|max:255',
            'city' => 'required|max:255',
            'postal_code' => 'required|max:255',
            'state' => 'required|max:255',
            'personal_id_number' => 'required|max:255',
            'personal_document' => 'image|mimes:jpg,jpeg,png',
            'date_of_birth' => 'required|max:255'
        ];


        $validator = Validator::make($request->all(),$validation);

        if($validator->fails()):
            return redirect('add-payment-method')->with('errors', $validator->errors());
        else:

            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

            try {
                $user = Auth::user();
                $query = StripeAccount::where('trainer_id', '=', $user->id);
                if(!$query->exists()):

                    $name_array = explode(' ', $user->name);

                    $stripe_account = \Stripe\Account::create([
                        "country" => "CA", 
                        "managed" => true,
                        "business_name" => $user->name,
                        "email" => $user->email,
                        "default_currency" => 'cad'
                        ]);
                    $stripe_account_details = new StripeAccount;
                    $stripe_account_details->trainer_id = $user->id;
                    $stripe_account_details->stripe_id = $stripe_account->id;
                    $stripe_account_details->email = $stripe_account->email;
                    $stripe_account_details->business_name = $stripe_account->business_name;
                    $stripe_account_details->default_currency = $stripe_account->default_currency;
                    $stripe_account_details->secret = $stripe_account->keys->secret;
                    $stripe_account_details->publishable = $stripe_account->keys->publishable;
                    $stripe_account_details->managed = 'Yes';
                    $stripe_account_details->save();
                else:
                    $stripe_account_details = StripeAccount::where('trainer_id', Auth::user()->id)->first();                    
                endif;

                $identity_document = \Stripe\FileUpload::create(
                  array(
                    "purpose" => "identity_document",
                    "file" => fopen($_FILES['personal_document']['tmp_name'], 'r')
                  ),
                  array("stripe_account" => $stripe_account_details->stripe_id)
                );
            } catch(Exception  $e) {
                $notify = (object) array();
                $notify->title = 'Error';
                $notify->type = 'error';
                $notify->text = $e->getMessage();
                return redirect('add-payment-method')->with('notify', $notify);
            }


            try {
                $date_of_birth_array = explode('-', $request->date_of_birth);
                $account = \Stripe\Account::retrieve($stripe_account_details->stripe_id);
                $account->external_account = $request->external_account;
                $account->legal_entity->address->city = $request->city;
                $account->legal_entity->address->line1 = $request->address1;
                $account->legal_entity->address->postal_code = $request->postal_code;
                $account->legal_entity->address->state = $request->state;
                $account->legal_entity->dob->day = $date_of_birth_array[2];
                $account->legal_entity->dob->month = $date_of_birth_array[1];
                $account->legal_entity->dob->year = $date_of_birth_array[0];
                $account->legal_entity->first_name = $request->first_name;
                $account->legal_entity->last_name = $request->last_name;
                $account->legal_entity->verification->document = $identity_document->id;
                $account->legal_entity->type = 'individual';
                $account->tos_acceptance->date = time();
                // $account->tos_acceptance->ip = $request->ip;
                $account->tos_acceptance->ip = '23.16.0.1';
                $account->save();

                $accepted_payment = new AcceptedPayment;
                $accepted_payment->user_id = Auth::user()->id;
                $accepted_payment->stripe_account_id = $stripe_account_details->id;
                $accepted_payment->external_account = $request->external_account;
                $accepted_payment->stripe_payment_method_id = $account->id;
                $accepted_payment->save();

                $email_data = array(
                    'email' => Auth::user()->email,
                    'name' => Auth::user()->name,
                    'title' => 'Payment method added',
                    'address' => 'Hello '.Auth::user()->name.',',
                    'link' => array(url('payment-settings'), 'View settings'),
                    'body' => 'You have successfully added a new payment method.'
                    );

                Mail::send('frontend.emails.template', $email_data, function($message) use ($email_data) {
                    $message->from('mail@clutchhealth.com' , 'Clutch Health Team' );
                    $message->to($email_data['email'], $email_data['name'])->subject('Payment method added');
                });

                $notify = (object) array();
                $notify->title = 'Success';
                $notify->type = 'success';
                $notify->text = 'Payment method successfully requested.';
                return redirect('/payment-settings')->with('notify', $notify);

            } catch(Exception $e) {
                $notify = (object) array();
                $notify->title = 'Error';
                $notify->type = 'error';
                $notify->text = $e->getMessage();
                return redirect('add-payment-method')->with('notify', $notify);
            }
        endif;
    }

    public function pay(Request $request) {
        $query = Contract::where('id', '=', $request->contract_id)->where('customer_id', '=', Auth::user()->id);
        if(!$query->exists()):
            $notify = (object) array();
            $notify->title = 'Error';
            $notify->type = 'error';
            $notify->text = 'Contract does not exists.';
            return redirect('/dashboard')->with('notify', $notify);
        endif;

        $name_array = explode(' ', Auth::user()->name);
        $data['first_name'] = (isset($name_array[0]))?$name_array[0]:"";
        $data['last_name'] = (isset($name_array[1]))?$name_array[1]:"";
        $data['email'] = Auth::user()->email;
        $data['contract'] = $query->first();

        return view('frontend/user/card-details', $data);
    }

    public function pay_process(Request $request) {
        $contract_id = $request->input('contract_id');
        $query = Contract::where('id', '=', $contract_id)->where('customer_id', '=', Auth::user()->id);
        if(!$query->exists()):
            $notify = (object) array();
            $notify->title = 'Error';
            $notify->type = 'error';
            $notify->text = 'Contract does not exists.';
            return redirect('/dashboard')->with('notify', $notify);
        endif;
        $contract = $query->first();

        $token = $request->input('stripeToken');
        $total_mode = (is_numeric($request->input('total_mode')) && $request->input('total_mode') > 0)?$request->input('total_mode'):1;

        $amount = $total_mode*$contract->budget;
        $application_fee = (env('APPLICATION_FEE')/$amount)*100;

        $stripe_class = \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        $query = StripeAccount::where('trainer_id',$contract->trainer_id);
        if(!$query->exists()):
            $notify = (object) array();
            $notify->title = 'Error';
            $notify->type = 'error';
            $notify->text = $contract->trainer->name." has not setup payment details.";
            return redirect('/dashboard')->with('notify', $notify);
        endif;
        $stripe_account = $query->first();

        try {
            $charge = \Stripe\Charge::create([
                "amount" => round($amount*1.13*100),
                "currency" => "cad", 
                "source" => $token, 
                "description" => "Coach fee from ".Auth::user()->name,
                "application_fee" => round($application_fee*100)
                ],[
                "stripe_account" => $stripe_account->stripe_id
                ]);

            $customer_body = 'Here is your payment summary for the purchase of trainer fee to '.$contract->trainer->name.'.<br /> <br />';
            
            $customer_body .= 'Price of trainer charge = $'.$amount.'<br />';
            $customer_body .= 'Price of HST Ontario services tax (13%) = $'.round($amount*0.13, 2).'<br />';
            $customer_body .= 'Total = $'.round($amount*1.13, 2).'<br /> <br />';

            $customer_body .= 'Thank you for you purchase. If you have any questions, you can contact us at <a href="mailto:support@clutchhealth.com"  target="_top">support@clutchhealth.com</a>.<br /><br />';

            $customer_body .= 'Thank you for joining The Clutch Health Team!<br />';
            $customer_body .= 'Clutch Health';

            $email_data = array(
                'email' => $contract->customer->email,
                'name' => $contract->customer->name,
                'title' => 'Payment summary',
                'address' => 'Hello '.$contract->customer->name.',',
                'body' => nl2br($customer_body),
                'replace' => true,
                'left' => true
                );

            Mail::send('frontend.emails.template', $email_data, function($message) use ($email_data) {
                $message->from('mail@clutchhealth.com' , 'Clutch Health Team' );
                $message->to($email_data['email'], $email_data['name'])->subject('Payment summary');
            });

            $trainer_body = 'Here is the breakdown of your recent payment from '.$contract->customer->name.'.<br /> <br />';
            
            $trainer_body .= '80% of '.$amount.' = $'.round($amount*0.80, 2).'<br />';
            $trainer_body .= '13% of '.round($amount*0.80, 2).' = $'.round($amount*0.80*0.13, 2).' (HST Ontario services tax)<br />';
            $trainer_body .= 'Total = $'.round($amount*0.80*1.13, 2).' <br /> <br />';

            $trainer_body .= 'We highly recommend that you set aside the $5.20 for income tax purposes. <br /> <br />';

            $trainer_body .= 'If you have any questions, contact us at <a href="mailto:support@clutchhealth.com"  target="_top">support@clutchhealth.com</a>.<br /> <br />Thank you for being a major part of The Clutch Health Team! <br />';
            $trainer_body .= 'Clutch Health';

            $email_data = array(
                'email' => $contract->trainer->email,
                'name' => $contract->trainer->name,
                'title' => 'Payment summary',
                'address' => 'Hello '.$contract->trainer->name.',',
                'body' => nl2br($trainer_body),
                'replace' => true,
                'left' => true
                );

            Mail::send('frontend.emails.template', $email_data, function($message) use ($email_data) {
                $message->from('mail@clutchhealth.com' , 'Clutch Health Team' );
                $message->to($email_data['email'], $email_data['name'])->subject('Payment summary');
            });
        } catch (\Stripe\Error\Card $e) {
            $notify = (object) array();
            $notify->title = 'Error';
            $notify->type = 'error';
            $notify->text = $e->getMessage();
            return redirect('/dashboard')->with('notify', $notify);
        }

        $transaction = new Transaction;
        $transaction->customer_id = $contract->customer_id;
        $transaction->trainer_id = $contract->trainer_id;
        $transaction->contract_id = $contract->id;
        $transaction->currency = $charge->currency;
        $transaction->amount = $charge->amount;
        $transaction->details = serialize($transaction);
        $transaction->save();


        $notify = (object) array();
        $notify->title = 'Success';
        $notify->type = 'success';
        $notify->text = 'Trainer fee successfully paid.';
        return redirect('/dashboard')->with('notify', $notify);
    }

    public function update_stripe_account(Request $request) {
        if(Auth::user()->rold_id != 2):
            abort(404);
        endif;
        $query = StripeAccount::where('trainer_id', Auth::user()->id);
        if(!$query->exists()):
            $notify = (object) array();
            $notify->title = 'Error';
            $notify->type = 'error';
            $notify->text = 'Your transaction account yet to be build on our system.';
            return redirect('/dashboard')->with('notify', $notify);
        endif;
        $stripe_id = $query->first()->stripe_id;
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $account = \Stripe\Account::retrieve($stripe_id);

    }

    public function create_trainer_accounts() {
        $where['role_id'] = 2;
        $where['status'] = 'Active';
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $i = 0;
        $j = 0;
        foreach(User::where($where)->get() as $user):
            $i++;
            $query = StripeAccount::where('trainer_id', '=', $user->id);
            if(!$query->exists()):
                $j++;

                $name_array = explode(' ', $user->name);

                $stripe_account = \Stripe\Account::create([
                    "country" => "CA", 
                    "managed" => true,
                    "business_name" => $user->name,
                    "email" => $user->email,
                    "default_currency" => 'cad'
                    ]);
                $account = new StripeAccount;
                $account->trainer_id = $user->id;
                $account->stripe_id = $stripe_account->id;
                $account->email = $stripe_account->email;
                $account->business_name = $stripe_account->business_name;
                $account->default_currency = $stripe_account->default_currency;
                $account->secret = $stripe_account->keys->secret;
                $account->publishable = $stripe_account->keys->publishable;
                $account->managed = 'Yes';
                $account->save();
            endif;
        endforeach;
        return "Out of ".$i." trainers ".$j." stripe accounts has been created.";
    }
}