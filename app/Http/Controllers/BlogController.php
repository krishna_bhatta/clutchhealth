<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    public function index($blog_alias = FALSE) {
    	if($blog_alias):
    		$blog = Blog::where('alias', $blog_alias);
    		if($blog->exists()):
    			$data['blog'] = $blog->first();
    			return view('frontend/blog/single', $data);
    		else:
    			abort(404);
    		endif;
    	else:
    		$data['blogs'] = Blog::all();
    		return view('frontend/blog/index', $data);
    	endif;

    }
}
