<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.2/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

use App\User;
use App\Role;
use App\PaymentMode;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $data['nav'] = 'home';
        return view('frontend.home', $data);
    }

    public function coach() {
        $data['nav'] = 'coach';
        return view('frontend.coach', $data);
    }

    public function exercise() {
        $data['nav'] = 'exercise';
        return view('frontend.exercise', $data);
    }

    public function findTrainer(Request $request) {
        $data['search'] = array(
            'name' => $request->input('name'),
            'payment_type' => $request->input('payment_type'),
            'address' => $request->input('address')
            );
        $users = User::where('role_id', 2)->where('status', 'Active');

        $name = $request->input('name');
        if($name != ""):
            $users->where('name', 'like', '%'.$name.'%');
        endif;


        $data['users'] = $users->get();
        $data['payments'] = PaymentMode::all();
        return view('frontend.find-trainer', $data);
    }
}