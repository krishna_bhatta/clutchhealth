<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\Role;

use App\User;
use App\PaymentMode;
use App\Contract;
use App\Reward;
use App\StripeAccount;

use Auth;

use Validator;

use Intervention\Image\ImageManager;
use Illuminate\Support\Facades\Input;
use Mail;

class UserController extends Controller
{

    /**
     * Show the user edit page.
     *
     * @return Response
     */
    public function index()
    {
        $data['users'] = User::all();
        $data['nav'] = 'user';

        return view('admin/user/list', $data);
    }

    public function confirm() {
        $token = Input::get('token');
        $query = User::where('id', '=', Input::get('cId'));
        if($query->exists()):
            $user = User::find(Input::get('cId'));
            if($token == $user->confirmation_token):
                $user->confirmation_token = md5(time().$user->id);
                $user->status = 'Active';
                $user->save();

                addPoint($user->id, 3, 'Active', 'Profile verified');

                if($user->role_id == 2):
                    $message_body = 'Once you\'re done, you can start getting paid!<br />';
                    
                    $message_body .= 'If you have any questions, you can contact us at <a href="mailto:support@clutchhealth.com"  target="_top">support@clutchhealth.com</a>.<br /><br />';

                    $message_body .= 'The Clutch Health Team';

                    $email_data = array(
                        'email' => $user->email,
                        'name' => $user->name,
                        'title' => 'Thank you for verifying account',
                        'address' => 'Thank you for activating your account '.$user->name.',',
                        'body' => nl2br($message_body),
                        'link' => array(url('add-payment-method'), 'Add payment method'),
                        'replace' => true,
                        'left' => true
                        );

                    Mail::send('frontend.emails.template', $email_data, function($message) use ($email_data) {
                        $message->from('mail@clutchhealth.com' , 'Clutch Health Team' );
                        $message->to($email_data['email'], $email_data['name'])->subject('Thank you for activating your account');
                    });
                endif;

                $notify = (object) array();
                $notify->title = 'Success';
                $notify->type = 'success';
                $notify->text = 'Profile successfully activated.';

                return redirect('user/profile')->with('notify', $notify);
            else:
                $notify = (object) array();
                $notify->title = 'Error';
                $notify->type = 'error';
                $notify->text = 'Invalid token.';
            endif;
        else:
            $notify = (object) array();
            $notify->title = 'Error';
            $notify->type = 'error';
            $notify->text = 'Broken link.';
        endif;
        return redirect('/')->with('notify', $notify);
    }

    /**
     * Show the user profile page.
     *
     * @return Response
     */
    public function profile($display = FALSE) {
        if(!$display):
            $data['user'] = User::findOrFail(Auth::user()->id);
            $data['payment_modes'] = PaymentMode::all();
            $data['nav'] = 'user';
            $data['rewards'] = Auth::user()->reward->sum('points');
            return view('frontend/user/profile', $data);
        else:
            $user_query = User::where('display',$display);

            if(!$user_query->exists()):
                $user_query = User::where('id',$display);
                if($user_query->exists()):
                    if($user_query->first()->role_id == 2):
                        return redirect('coach/'.$user_query->first()->display);
                    endif;
                else:
                    abort(404);
                endif;
            else:
                if($user_query->first()->display != $display && $user_query->first()->role_id == 2):
                    return redirect('coach/'.$user_query->first()->display);
                endif;
            endif;


            $data['user'] = $user_query->first();
            $data['payment_modes'] = PaymentMode::all();
            $data['nav'] = 'user';
            return view('frontend/user/public-profile', $data);
        endif;
    }

    /**
     * Manage user dashboard
     */
    public function dashboard() {
        $data['nav'] = 'contracts';
        if(Auth::user()->role_id == 2):
            $where = ['role_id' => 3];
        else:
            $where = ['role_id' => 2];
        endif;

        $where['is_public'] = 'Yes';
        $where['status'] = 'Active';
        
        $data['users'] = User::where($where)->orderBy('budget', 'desc')->get();
        return view('frontend.user.dashboard', $data);
    }

    /**
     * Change customer to trainer and trainer to customer view
     * @return void
     */
    public function changeRole(Request $request) {
        return view('frontend.user.change-role');
    }

    /**
     * Change customer to trainer and trainer to customer action
     * @return void
     */
    public function changeRoleAction(Request $request) {
        $messages = [
            
        ];

        $validation =  [
            'role' => 'required|between:1,4'
        ];
        $validator = Validator::make($request->all(),$validation,$messages);

        if($validator->fails()):
            return redirect('change-role')->with('errors', $validator->errors());
        endif;

        $user = User::find(Auth::user()->id);

        if($request->input('role') == 2 && Auth::user()->username == ''):
            $uesrname_array = $this->generateUserName(Auth::user()->name);
            $user->display = $uesrname_array['display'];
            $user->username = $uesrname_array['username'];
        endif;

        $user->role_id = $request->input('role');
        $user->status = 'Active';


        $user->save();

        $notify = (object) array();
        $notify->title = 'Success';
        $notify->type = 'success';
        $notify->text = 'Your role successfully changed.';

        return redirect('dashboard')->with('notify', $notify);
    }

    private function generateUserName($full_name) {

        $display = str_replace(' ', '', $full_name);
        $username = strtolower($display);

        if(!User::where('username',$username)->exists()):
            return ['display' => $display, 'username' => $username];
        endif;

        $display = $display.rand(0,1000);
        $username = strtolower($display);

        while (!User::where('username',$username)->exists()) {            
            return ['display' => $display, 'username' => $username];

        }
    }

    

    /**
     * Show the user edit page.
     *
     * @return Response
     */
    public function edit(Request $request)
    {
        $data['user'] = Auth::user();
        $data['nav'] = 'user';

        $name = $request->input('name');

        if($name != "") {
            $email = $request->input('email');

            $messages = [
                
            ];

            $validation =  [
                'name' => 'required|max:255',
                'email' => 'required|email',
                'date_of_birth' => 'required|max:255',
                'phone' => 'max:20',
                'longitude' => 'required|max:255',
                'latitude' => 'required|max:255',
                'profile_photo' => 'image',
                'password' => 'confirmed|min:5',
            ];

            if(Auth::user()->email != $email):
                $validation['email'] = 'required|email|max:255|unique:users';
            endif;



            $validator = Validator::make($request->all(),$validation,$messages);

            if($validator->fails()):
                return redirect('user/profile')->with('errors', $validator->errors());
            else:
                $user = User::find(Auth::user()->id);

                $user->name = $request->input('name');
                $user->email = $request->input('email');
                $user->date_of_birth = date('Y-m-d', strtotime($request->input('date_of_birth')));
                $user->phone = $request->input('phone');
                $user->longitude = $request->input('longitude');
                $user->latitude = $request->input('latitude');
                $user->address = $request->input('map-address');
                $user->training_area_radius = $request->input('training_area_radius');
                $user->budget = $request->input('budget');
                $user->payment_mode_id = $request->input('payment_mode_id');
                $user->is_public = ($request->input('is_public') != "")?$request->input('is_public'):"No";;

                if($request->input('password') != ""):
                    $user->password = bcrypt($request->input('password'));
                endif;


                $user->save();

                if ($request->hasFile('profile_photo')):
                    $destinationPath = 'uploads/profile'; // upload path
                    $extension = $request->file('profile_photo')->getClientOriginalExtension(); // getting image extension
                    $fileName = md5(time().rand(11111,99999)).'.'.$extension; // renameing image
                    // $request->file('profile_photo')->move($destinationPath, $fileName);

                    $image_manager = new ImageManager;

                    $img = $image_manager->make($_FILES['profile_photo']['tmp_name']);

                    // resize image
                    $img->fit(200, 200);

                    // save image
                    $img->save($destinationPath.'/'.$fileName);


                    insert_meta('user_metas', 'user_id', Auth::user()->id, 'profile_photo', $fileName);
                endif;

                insert_meta('user_metas', 'user_id', Auth::user()->id, 'description', $request->input('description'));

                insert_meta('user_metas', 'user_id', Auth::user()->id, 'awards', $request->input('awards'));

                insert_meta('user_metas', 'user_id', Auth::user()->id, 'training_start', $request->input('training_start'));

                insert_meta('user_metas', 'user_id', Auth::user()->id, 'certificates', $request->input('certificates'));

                insert_meta('user_metas', 'user_id', Auth::user()->id, 'specifications', $request->input('specifications'));

                $notify = (object) array();
                $notify->title = 'Success';
                $notify->type = 'success';
                $notify->text = 'Profile successfully updated.';

                return redirect('user/profile')->with('notify', $notify);
            endif;
        }


        return view('user/profile', $data);
    }

    public function check_username(Request $request) {
        $username = $request->input('username');

        $username_rule = 'required|min:5|max:18|alpha_num';
        if(Auth::user()->username != $username):
            $username_rule .= '|unique:users';
        endif;

        $validation =  [
                'username' => $username_rule
            ];
        $validator = Validator::make($request->all(),$validation);

        if($validator->fails()):
            return $validator->errors();
        else:
            return TRUE;
        endif;
    }
}
