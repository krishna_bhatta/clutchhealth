<?php

namespace App\Http\Controllers\Admin;

use App\Faq;
use App\faqTag;
use Validator;
use Auth;
use Session;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Intervention\Image\ImageManager;

class FaqController extends Controller
{
    public function index() {
        $data['faqs'] = faq::all();
        $data['nav'] = 'faq';

        return view('admin/faq/list', $data);
    }

    public function add(Request $request) {
        $data['nav'] = 'add-faq';
        $data['sort_order'] = Faq::max('sort_order')+1;

        return view('admin/faq/add', $data);
    }

    public function addAction(Request $request) {
		$validation =  [
	        'question' => 'required|max:255',
	        'answer' => 'required',
	        'sort_order' => 'required'
	    ];
	    $validator = Validator::make($request->all(),$validation);
	    if($validator->fails()):
	    	return redirect('admin/faq/add')->with('errors', $validator->errors());
	    else:
	    	$faq = new Faq;
	    	$faq->question = $request->input('question');
	    	$faq->answer = $request->input('answer');
	    	$faq->sort_order = $request->input('sort_order');
	    	$faq->save();
	    	return redirect('admin/faq/add');
	    endif;
    }

    public function edit($faq_id) {
        $data['nav'] = 'faq';
    	$data['faq'] = faq::findOrFail($faq_id);
    	$data['faq_id'] = $faq_id;
    	return view('admin/faq/edit', $data);
    }

    public function editAction(Request $request, $faq_id) {
		$validation =  [
	        'question' => 'required|max:255',
	        'answer' => 'required',
	        'sort_order' => 'required'
	    ];
	    $validator = Validator::make($request->all(),$validation);
	    if($validator->fails()):
	    	return redirect('admin/faq/edit/'.$faq_id)->with('errors', $validator->errors());
	    else:
	    	$faq = faq::findOrFail($faq_id);
	    	$faq->question = $request->input('question');
	    	$faq->answer = $request->input('answer');
	    	$faq->sort_order = $request->input('sort_order');
	    	$faq->save();
	    	$faq->save();
	    	return redirect('admin/faqs');
	    endif;
    }

    public function delete(Request $request, $faq_id) {
    	$faq = Faq::where('id', $faq_id);
    	if($faq->exists()):
    		$faq->delete();
    		Session::flash('message', 'FAQ successfully deleted.');
    		return redirect('admin/faqs');
    	else:
    		$error = array('faq' => 'Unable to delete FAQ.');
    		return redirect('admin/faqs/')->with('errors', $error);
    	endif;
    }
}
