<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\User;

use App\Message;

use Auth;

use Validator;
use Mail;

class MessageController extends Controller
{
    public function index() {
      $where = ['to_id' => Auth::id(), 'deleted_by_to' => 'No'];
      $data['messages'] = Message::where($where)->get();
      $data['nav'] = 'message';
      $data['title'] = 'Inbox';

      return view('admin/message/list', $data);
    }
    public function sent() {
      $where = ['from_id' => Auth::id(), 'deleted_by_from' => 'No'];
      $data['messages'] = Message::where($where)->get();
      $data['nav'] = 'message';
      $data['title'] = 'Sent';

      return view('admin/message/list', $data);
    }
    public function trash() {
      $where = ['from_id' => Auth::id(), 'deleted_by_from' => 'Yes'];
      $orWhere = ['to_id' => Auth::id(), 'deleted_by_to' => 'Yes'];
      $data['messages'] = Message::where($where)->orWhere($orWhere)->get();
      $data['nav'] = 'message';
      $data['title'] = 'Trash';

      return view('admin/message/list', $data);
    }

    public function read($message_id) {
    	$data['message'] = Message::findOrFail($message_id);
      $data['nav'] = 'message';
      $data['title'] = 'Inbox';

      $flight = Message::where('id', $message_id)
          ->where('to_id', Auth::id())
          ->update(['read' => 'Yes']);

      return view('admin/message/read', $data);
    }

    public function compose() {
    	$data['users'] = User::where('id', '!=', Auth::id())->get();
      $data['nav'] = 'message';
      $data['title'] = 'Inbox';
        return view('admin/message/compose', $data);
    }

    public function submitCompose(Request $request) {

    	$messages = [
    	    'to.required' => 'Recipient field is required.',
    	];

    	$validator = Validator::make($request->all(), [
    	    'to' => 'required|numeric',
    	    'subject' => 'required|string',
    	    'message' => 'required|string'
    	], $messages);

    	if ($validator->fails()) {
            return redirect('admin/message/compose')
                        ->withErrors($validator)
                        ->withInput();
        }

       $flight = new Message;
       $flight->to_id = $request->to;
       $flight->from_id = Auth::id();
       $flight->subject = $request->subject;
       $flight->message = $request->message;
       $flight->save();

       return redirect('admin/message')->with('success', 'Message successfully sent.');
    }

    public function BroadcastMessage(Request $request) {
      if ($request->has('address')):
        $query = User::where('id','!=','');

        if($request->type == "Trainers"):
          $query->where('role_id',2);
        elseif($request->type == "Customers"):
          $query->where('role_id', 3);
        endif;

        if($request->active != ""):
          $query->where('status', 'Active');
        endif;
        $users = $query->get();

        // echo "<pre>"; print_r($email_data); die; 
        foreach($users as $user):
          $email_data = array(
            'title' => $this->replace_body($request->page_title, $user),
            'address' => $this->replace_body($request->address, $user),
            'body' => $this->replace_body($request->message, $user)
            );

        if($request->link != "" && $request->link_text):
          $email_data['link'] = array($request->link, $request->link_text);
        endif;
        $email_data['current_email'] = $user->email;
        $email_data['current_name'] = $user->name;
        if($request->is_test == "Yes"):
          $email_data['current_email'] = $request->test_email;
        endif;
        Mail::send('admin.message.broadcast-template', $email_data, function($message) use ($email_data) {
            $message->from('mail@clutchhealth.com' , 'Clutch Health Team' );
            $message->sender('mail@clutchhealth.com' , 'Clutch Health Team' );
            $message->to($email_data['current_email'], $email_data['current_name'])->subject($email_data['title']);
        });
        if($request->is_test == "Yes"):
          $notify = (object) array();
          $notify->title = 'Success';
          $notify->type = 'success';
          $notify->text = "Test email successfully send.";
          return redirect('/admin/broadcast-email')->with('notify', $notify);
        endif;
      endforeach;

        $notify = (object) array();
        $notify->title = 'Success';
        $notify->type = 'success';
        $notify->text = "Email successfully send.";
        return redirect('/admin/broadcast-email')->with('notify', $notify);
      endif;
      $data['nav'] = 'mail';
      $data['title'] = 'Broadcast message';
      return view('admin/message/broadcast', $data);
    }

    private function replace_body($body, $user) {      
      $name_array = explode(' ', $user->name);
      $first_name = (isset($name_array[0]))?$name_array[0]:"";
      $last_name = (isset($name_array[1]))?$name_array[1]:"";

      $body = str_replace('**fname**', $first_name, $body);
      $body = str_replace('**lname**', $last_name, $body);
      return $body;
    }
}
