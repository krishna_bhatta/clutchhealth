<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Question;
use App\Answer;
use Validator;

class QuestionController extends Controller
{
        public function index() {
            $data['questions'] = Question::all();
            $data['nav'] = 'question';

            return view('admin/question/list', $data);
        }
        public function view_answer() {
            $data['answers'] = Answer::all();
            $data['nav'] = 'answer';

            return view('admin/question/answer', $data);
        }

        public function add(Request $request) {
            $data['nav'] = 'add-question';

            return view('admin/question/add', $data);
        }

        public function addAction(Request $request) {
    		$validation =  [
    	        'title' => 'required|max:255',
    	        'type' => 'required',
    	        'status' => 'required'
    	    ];
    	    $validator = Validator::make($request->all(),$validation);
    	    if($validator->fails()):
    	    	return redirect('admin/question/add')->with('errors', $validator->errors());
    	    else:
    	    	$question = new Question;
    	    	$question->title = $request->input('title');
    	    	$question->type = $request->input('type');
    	    	$question->options = $request->input('options');
    	    	$question->status = $request->input('status');
    	    	$question->save();

    	    	return redirect('admin/question/add');
    	    endif;
        }

        public function edit($question_id) {
            $data['nav'] = 'question';
        	$data['question'] = Question::findOrFail($question_id);
        	$data['question_id'] = $question_id;
        	return view('admin/question/edit', $data);
        }

        public function editAction(Request $request, $question_id) {
    		$validation =  [
    	        'title' => 'required|max:255',
    	        'type' => 'required',
    	        'status' => 'required'
    	    ];
    	    $validator = Validator::make($request->all(),$validation);
    	    if($validator->fails()):
    	    	return redirect('admin/question/edit/'.$question_id)->with('errors', $validator->errors());
    	    else:
    	    	$question = Question::findOrFail($question_id);
    	    	$question->title = $request->input('title');
    	    	$question->type = $request->input('type');
    	    	$question->options = $request->input('options');
    	    	$question->status = $request->input('status');
    	    	$question->save();

    	    	return redirect('admin/questions');
    	    endif;
        }

        public function delete(Request $request, $question_id) {
        	$question = Question::where('id', $question_id);
        	if($question->exists()):
        		$question->delete();
        		Session::flash('message', 'Question successfully deleted.');
        		return redirect('admin/questions');
        	else:
        		$error = array('question' => 'Unable to delete question.');
        		return redirect('admin/questions/')->with('errors', $error);
        	endif;
        }
}
