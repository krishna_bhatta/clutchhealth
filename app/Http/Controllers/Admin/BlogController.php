<?php

namespace App\Http\Controllers\Admin;

use App\Blog;
use App\BlogMeta;
use App\BlogTag;
use Validator;
use Auth;
use Session;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Intervention\Image\ImageManager;

class BlogController extends Controller
{
    public function index() {
        $data['blogs'] = Blog::all();
        $data['nav'] = 'blog';

        return view('admin/blog/list', $data);
    }

    public function add(Request $request) {
        $data['nav'] = 'add-blog';

        return view('admin/blog/add', $data);
    }

    public function tags() {
    	return response()->json(BlogTag::all('id', 'title'));
    }

    public function addAction(Request $request) {
		$validation =  [
	        'title' => 'required|max:255',
	        'description' => 'required',
	        'visibility' => 'required',
	        'details' => 'required',
	        'status' => 'required'
	    ];
	    $validator = Validator::make($request->all(),$validation);
	    if($validator->fails()):
	    	return redirect('admin/blog/add')->with('errors', $validator->errors());
	    else:
	    	$blog = new Blog;
	    	$blog->title = $request->input('title');
	    	$blog->alias = str_slug($request->input('title'), '-');
	    	$blog->description = $request->input('description');
	    	$blog->visibility = $request->input('visibility');
	    	$blog->details = $request->input('details');
	    	$blog->status = $request->input('status');
	    	$blog->author = Auth::user()->id;

	    	if ($request->hasFile('featured_image')):
	    	    $destinationPath = 'uploads/media'; // upload path
	    	    $extension = $request->file('featured_image')->getClientOriginalExtension(); // getting image extension
	    	    $fileName = str_slug($request->input('title'), '-').'-'.md5(time().rand(11111,99999)).'.'.$extension; // renameing image
	    	    // $request->file('featured_image')->move($destinationPath, $fileName);

	    	    $image_manager = new ImageManager;

	    	    $img = $image_manager->make($_FILES['featured_image']['tmp_name']);

	    	    // resize image
	    	    $img->fit(375, 195);

	    	    // save image
	    	    $img->save($destinationPath.'/'.$fileName);


	    	    $blog->featured_image = $fileName;
	    	endif;

	    	$blog->save();



			if($request->input('meta_title') != ""):
		    	insert_meta('blog_metas', 'blog_id', $blog->id, 'meta_title', $request->input('meta_title'));
		    endif;
		    if($request->input('meta_description') != ""):
		    	insert_meta('blog_metas', 'blog_id', $blog->id, 'meta_description', $request->input('meta_description'));
		    endif;
	    	if ($request->hasFile('fb_image')):
	    	    $destinationPath = 'uploads/media'; // upload path
	    	    $extension = $request->file('fb_image')->getClientOriginalExtension(); // getting image extension
	    	    $fileName = str_slug($request->input('title'), '-').'-'.md5(time().rand(11111,99999)).'.'.$extension; // renameing image

	    	    $image_manager = new ImageManager;

	    	    $img = $image_manager->make($_FILES['fb_image']['tmp_name']);

	    	    // resize image
	    	    $img->fit(1200, 630);

	    	    // save image
	    	    $img->save($destinationPath.'/'.$fileName);

	    	    insert_meta('blog_metas', 'blog_id', $blog->id, 'fb_image', $fileName);
	    	endif;
	    	if ($request->hasFile('twitter_image')):
	    	    $destinationPath = 'uploads/media'; // upload path
	    	    $extension = $request->file('twitter_image')->getClientOriginalExtension(); // getting image extension
	    	    $fileName = str_slug($request->input('title'), '-').'-'.md5(time().rand(11111,99999)).'.'.$extension; // renameing image

	    	    $image_manager = new ImageManager;

	    	    $img = $image_manager->make($_FILES['twitter_image']['tmp_name']);

	    	    // resize image
	    	    $img->fit(1024, 512);

	    	    // save image
	    	    $img->save($destinationPath.'/'.$fileName);

	    	    insert_meta('blog_metas', 'blog_id', $blog->id, 'twitter_image', $fileName);
	    	endif;

	    	return redirect('admin/blog/add');
	    endif;
    }

    public function edit($blog_id) {
        $data['nav'] = 'blog';
    	$data['blog'] = Blog::findOrFail($blog_id);
    	$data['blog_id'] = $blog_id;
    	return view('admin/blog/edit', $data);
    }

    public function editAction(Request $request, $blog_id) {
		$validation =  [
	        'title' => 'required|max:255',
	        'description' => 'required',
	        'visibility' => 'required',
	        'details' => 'required',
	        'status' => 'required'
	    ];
	    $validator = Validator::make($request->all(),$validation);
	    if($validator->fails()):
	    	return redirect('admin/page/edit/'.$blog_id)->with('errors', $validator->errors());
	    else:
	    	$blog = Blog::findOrFail($blog_id);

	    	
	    	$blog->title = $request->input('title');
	    	$blog->alias = str_slug($request->input('title'), '-');
	    	$blog->description = $request->input('description');
	    	$blog->visibility = $request->input('visibility');
	    	$blog->details = $request->input('details');
	    	$blog->status = $request->input('status');
	    	$blog->author = Auth::user()->id;

	    	if ($request->hasFile('featured_image')):
	    	    $destinationPath = 'uploads/media'; // upload path
	    	    $extension = $request->file('featured_image')->getClientOriginalExtension(); // getting image extension
	    	    $fileName = str_slug($request->input('title'), '-').'-'.md5(time().rand(11111,99999)).'.'.$extension; // renameing image
	    	    // $request->file('featured_image')->move($destinationPath, $fileName);

	    	    $image_manager = new ImageManager;

	    	    $img = $image_manager->make($_FILES['featured_image']['tmp_name']);

	    	    // resize image
	    	    $img->fit(375, 195);

	    	    // save image
	    	    $img->save($destinationPath.'/'.$fileName);


	    	    $blog->featured_image = $fileName;
	    	endif;

	    	$blog->save();



			if($request->input('meta_title') != ""):
		    	insert_meta('blog_metas', 'blog_id', $blog->id, 'meta_title', $request->input('meta_title'));
		    endif;
		    if($request->input('meta_description') != ""):
		    	insert_meta('blog_metas', 'blog_id', $blog->id, 'meta_description', $request->input('meta_description'));
		    endif;
	    	if ($request->hasFile('fb_image')):
	    	    $destinationPath = 'uploads/media'; // upload path
	    	    $extension = $request->file('fb_image')->getClientOriginalExtension(); // getting image extension
	    	    $fileName = str_slug($request->input('title'), '-').'-'.md5(time().rand(11111,99999)).'.'.$extension; // renameing image

	    	    $image_manager = new ImageManager;

	    	    $img = $image_manager->make($_FILES['fb_image']['tmp_name']);

	    	    // resize image
	    	    $img->fit(1200, 630);

	    	    // save image
	    	    $img->save($destinationPath.'/'.$fileName);

	    	    insert_meta('blog_metas', 'blog_id', $blog->id, 'fb_image', $fileName);
	    	endif;
	    	if ($request->hasFile('twitter_image')):
	    	    $destinationPath = 'uploads/media'; // upload path
	    	    $extension = $request->file('twitter_image')->getClientOriginalExtension(); // getting image extension
	    	    $fileName = str_slug($request->input('title'), '-').'-'.md5(time().rand(11111,99999)).'.'.$extension; // renameing image

	    	    $image_manager = new ImageManager;

	    	    $img = $image_manager->make($_FILES['twitter_image']['tmp_name']);

	    	    // resize image
	    	    $img->fit(1024, 512);

	    	    // save image
	    	    $img->save($destinationPath.'/'.$fileName);

	    	    insert_meta('blog_metas', 'blog_id', $blog->id, 'twitter_image', $fileName);
	    	endif;
	    	
	    	return redirect('admin/blogs');
	    endif;
    }

    public function delete(Request $request, $blog_id) {
    	$blog = Blog::where('id', $blog_id);
    	if($blog->exists()):
    		$blog->delete();
    		Session::flash('message', 'Blog successfully deleted.');
    		return redirect('admin/blogs');
    	else:
    		$error = array('blog' => 'Unable to delete blog.');
    		return redirect('admin/blogs/')->with('errors', $error);
    	endif;
    }
}
