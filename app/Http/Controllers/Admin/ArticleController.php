<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\ArticleMeta;
use Validator;
use Auth;
use Session;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Intervention\Image\ImageManager;

class ArticleController extends Controller
{
    public function index() {
        $data['articles'] = Article::all();
        $data['nav'] = 'article';

        return view('admin/article/list', $data);
    }

    public function add(Request $request) {
        $data['nav'] = 'add-article';

        return view('admin/article/add', $data);
    }

    public function addAction(Request $request) {
		$validation =  [
	        'title' => 'required|max:255',
	        'description' => 'required',
	        'details' => 'required'
	    ];
	    $validator = Validator::make($request->all(),$validation);
	    if($validator->fails()):
	    	return redirect('admin/page/add')->with('errors', $validator->errors());
	    else:
	    	$article = new Article;
	    	$article->title = $request->input('title');
	    	$article->alias = str_slug($request->input('title'), '-');
	    	$article->description = $request->input('description');
	    	$article->details = $request->input('details');
	    	$article->created_by = Auth::user()->id;
	    	$article->status = 'Published';
	    	$article->save();

			if($request->input('meta_title') != ""):
		    	insert_meta('article_metas', 'article_id', $article->id, 'meta_title', $request->input('meta_title'));
		    endif;
		    if($request->input('meta_description') != ""):
		    	insert_meta('article_metas', 'article_id', $article->id, 'meta_description', $request->input('meta_description'));
		    endif;
	    	if ($request->hasFile('fb_image')):
	    	    $destinationPath = 'uploads/media'; // upload path
	    	    $extension = $request->file('fb_image')->getClientOriginalExtension(); // getting image extension
	    	    $fileName = str_slug($request->input('title'), '-').'-'.md5(time().rand(11111,99999)).'.'.$extension; // renameing image

	    	    $image_manager = new ImageManager;

	    	    $img = $image_manager->make($_FILES['fb_image']['tmp_name']);

	    	    // resize image
	    	    $img->fit(1200, 630);

	    	    // save image
	    	    $img->save($destinationPath.'/'.$fileName);

	    	    insert_meta('article_metas', 'article_id', $article->id, 'fb_image', $fileName);
	    	endif;
	    	if ($request->hasFile('twitter_image')):
	    	    $destinationPath = 'uploads/media'; // upload path
	    	    $extension = $request->file('twitter_image')->getClientOriginalExtension(); // getting image extension
	    	    $fileName = str_slug($request->input('title'), '-').'-'.md5(time().rand(11111,99999)).'.'.$extension; // renameing image

	    	    $image_manager = new ImageManager;

	    	    $img = $image_manager->make($_FILES['twitter_image']['tmp_name']);

	    	    // resize image
	    	    $img->fit(1024, 512);

	    	    // save image
	    	    $img->save($destinationPath.'/'.$fileName);

	    	    insert_meta('article_metas', 'article_id', $article->id, 'twitter_image', $fileName);
	    	endif;

	    	return redirect('admin/page/add');
	    endif;
    }

    public function edit($article_id) {
        $data['nav'] = 'article';
    	$data['article'] = Article::findOrFail($article_id);
    	$data['article_id'] = $article_id;
    	return view('admin/article/edit', $data);
    }

    public function editAction(Request $request, $article_id) {
		$validation =  [
	        'title' => 'required|max:255',
	        'description' => 'required',
	        'details' => 'required'
	    ];
	    $validator = Validator::make($request->all(),$validation);
	    if($validator->fails()):
	    	return redirect('admin/page/edit/'.$article_id)->with('errors', $validator->errors());
	    else:
	    	$article = Article::findOrFail($article_id);
	    	$article->title = $request->input('title');
	    	// $article->alias = str_slug($request->input('title'), '-');
	    	$article->description = $request->input('description');
	    	$article->details = $request->input('details');
	    	$article->save();

			if($request->input('meta_title') != ""):
		    	insert_meta('article_metas', 'article_id', $article->id, 'meta_title', $request->input('meta_title'));
		    endif;
		    if($request->input('meta_description') != ""):
		    	insert_meta('article_metas', 'article_id', $article->id, 'meta_description', $request->input('meta_description'));
		    endif;
	    	if ($request->hasFile('fb_image')):
	    	    $destinationPath = 'uploads/media'; // upload path
	    	    $extension = $request->file('fb_image')->getClientOriginalExtension(); // getting image extension
	    	    $fileName = str_slug($request->input('title'), '-').'-'.md5(time().rand(11111,99999)).'.'.$extension; // renameing image

	    	    $image_manager = new ImageManager;

	    	    $img = $image_manager->make($_FILES['fb_image']['tmp_name']);

	    	    // resize image
	    	    $img->fit(1200, 630);

	    	    // save image
	    	    $img->save($destinationPath.'/'.$fileName);

	    	    insert_meta('article_metas', 'article_id', $article->id, 'fb_image', $fileName);
	    	endif;
	    	if ($request->hasFile('twitter_image')):
	    	    $destinationPath = 'uploads/media'; // upload path
	    	    $extension = $request->file('twitter_image')->getClientOriginalExtension(); // getting image extension
	    	    $fileName = str_slug($request->input('title'), '-').'-'.md5(time().rand(11111,99999)).'.'.$extension; // renameing image

	    	    $image_manager = new ImageManager;

	    	    $img = $image_manager->make($_FILES['twitter_image']['tmp_name']);

	    	    // resize image
	    	    $img->fit(1024, 512);

	    	    // save image
	    	    $img->save($destinationPath.'/'.$fileName);

	    	    insert_meta('article_metas', 'article_id', $article->id, 'twitter_image', $fileName);
	    	endif;

	    	return redirect('admin/pages');
	    endif;
    }

    public function delete(Request $request, $article_id) {
    	$article = Article::where('id', $article_id);
    	if($article->exists()):
    		$article->delete();
    		Session::flash('message', 'Page successfully deleted.');
    		return redirect('admin/pages');
    	else:
    		$error = array('article' => 'Unable to delete article.');
    		return redirect('admin/pages/')->with('errors', $error);
    	endif;
    }
}
