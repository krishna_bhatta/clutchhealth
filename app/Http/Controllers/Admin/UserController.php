<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\Role;

use App\User;

use Auth;

use Validator;

use Intervention\Image\ImageManager;

class UserController extends Controller
{

    /**
     * Show the user edit page.
     *
     * @return Response
     */
    public function index()
    {
        $data['users'] = User::all();
        $data['nav'] = 'user';

        return view('admin/user/list', $data);
    }

    /**
     * Show the user profile page.
     *
     * @return Response
     */
    public function profile($user_id) {

        $data['user'] = User::findOrFail($user_id);
        $data['nav'] = 'user';
        return view('admin/user/profile', $data);
    }

    /**
     * Show the user edit page.
     *
     * @return Response
     */
    public function edit(Request $request)
    {
        $data['user'] = Auth::user();
        $data['nav'] = 'user';

        $name = $request->input('name');

        if($name != "") {
            $email = $request->input('email');

            $messages = [
                
            ];

            $validation =  [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255',
                'date_of_birth' => 'required|max:255',
                'phone' => 'max:20',
                'longitude' => 'required|max:255',
                'latitude' => 'required|max:255',
                'profile_photo' => 'image',
                'password' => 'confirmed|min:5',
            ];

            if(Auth::user()->email != $email):
                $validation['email'] = 'required|email|max:255|unique:users';
            endif;



            $validator = Validator::make($request->all(),$validation,$messages);

            if($validator->fails()):
                return redirect('admin/edit')->with('errors', $validator->errors());
            else:
                $user = User::find(Auth::user()->id);

                $user->name = $request->input('name');
                $user->email = $request->input('email');
                $user->date_of_birth = date('Y-m-d', strtotime($request->input('date_of_birth')));
                $user->phone = $request->input('phone');
                $user->longitude = $request->input('longitude');
                $user->latitude = $request->input('latitude');
                $user->address = $request->input('map-address');

                if($request->input('password') != ""):
                    $user->password = bcrypt($request->input('password'));
                endif;


                $user->save();

                if ($request->hasFile('profile_photo')):
                    $destinationPath = 'uploads/profile'; // upload path
                    $extension = $request->file('profile_photo')->getClientOriginalExtension(); // getting image extension
                    $fileName = md5(time().rand(11111,99999)).'.'.$extension; // renameing image
                    // $request->file('profile_photo')->move($destinationPath, $fileName);

                    $image_manager = new ImageManager;

                    $img = $image_manager->make($_FILES['profile_photo']['tmp_name']);

                    // resize image
                    $img->fit(200, 200);

                    // save image
                    $img->save($destinationPath.'/'.$fileName);


                    insert_meta('user_metas', 'user_id', Auth::user()->id, 'profile_photo', $fileName);
                endif;

                insert_meta('user_metas', 'user_id', Auth::user()->id, 'description', $request->input('description'));

                return redirect('admin/edit')->with('success', 'User successfully updated.');
            endif;
        }


        return view('admin/user/edit', $data);
    }

    /**
     * Trigger ban/unban user
     *
     * @return Response
     */
    public function triggerStatus($user_id)
    {
        $user = User::findOrFail($user_id);
        if(!$user)
            return redirect('home')->with('error', 'Unable to find user.');

        if($user->status == 'Active'):
            $user->status = 'Ban';
        else:
            $user->status = 'Active';
        endif;

        $user->save();
        return redirect('users')->with('success', 'User status successfully updated.');
    }
}
