<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\RewardSystem;
use App\Role;

use Auth;

use Validator;
use Session;

class RewardController extends Controller
{
   public function index() {
           $data['rewards'] = RewardSystem::all();
           $data['nav'] = 'reward';

           return view('admin/reward/list', $data);
       }

       public function add(Request $request) {
           $data['nav'] = 'add-reward';
           $data['roles'] = Role::where('status','Active')->where('id', '!=', '1')->get();

           return view('admin/reward/add', $data);
       }

       public function addAction(Request $request) {
            $validation =  [
                'title' => 'required',
                'point' => 'required',
                'role_id' => 'required',
                'status' => 'required'
            ];
            $validator = Validator::make($request->all(),$validation);
            if($validator->fails()):
                return redirect('admin/reward/add')->with('errors', $validator->errors());
            endif;
            $reward = new RewardSystem;
            $reward->title = $request->input('title');
            $reward->role_id = $request->input('role_id');
            $reward->point = $request->input('point');
            $reward->status = $request->input('status');
            $reward->save();
            return redirect('admin/reward/add');
       }

       public function edit($reward_id) {
           $data['nav'] = 'reward';
           $data['roles'] = Role::where('status','Active')->get();
            $data['reward'] = RewardSystem::findOrFail($reward_id);
            $data['reward_id'] = $reward_id;
            return view('admin/reward/edit', $data);
       }

       public function editAction(Request $request, $reward_id) {
            $validation =  [
                'title' => 'required',
                'point' => 'required',
                'role_id' => 'required',
                'status' => 'required'
            ];
            $validator = Validator::make($request->all(),$validation);
            if($validator->fails()):
                return redirect('admin/reward/add')->with('errors', $validator->errors());
            endif;
            $reward = RewardSystem::findOrFail($reward_id);
            $reward->title = $request->input('title');
            $reward->role_id = $request->input('role_id');
            $reward->point = $request->input('point');
            $reward->status = $request->input('status');
            $reward->save();

            return redirect('admin/rewards');
       }

       /*public function delete(Request $request, $reward_id) {
        $reward = RewardSystem::where('id', $reward_id);
        if($reward->exists()):
            $reward->delete();
            Session::flash('message', 'reward successfully deleted.');
            return redirect('admin/rewards');
        else:
            $error = array('reward' => 'Unable to delete reward.');
            return redirect('admin/rewards/')->with('errors', $error);
        endif;
       }*/

}
