<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;

use Validator;

use App\Contract;

use App\PaymentMode;
use App\User;
use Mail;

class ContractController extends Controller
{

    public function index() {
        $data['contracts'] = Contract::where('trainer_id', Auth::user()->id)
                                        ->where('status', 'Pending')
                                        ->orWhere('status', 'Active')
                                        ->get();
        return view('frontend.contract.list', $data);
    }

    public function accept($contract_id, $status = 'accepted') {
        $query = Contract::where('trainer_id', Auth::user()->id)->where('id', $contract_id)->where('status','Pending');
        if($query->exists()):
            $contract = Contract::find($contract_id);
            if($status == 'rejected'):
                $contract->status = 'Rejected';
            else:
                $contract->status = 'Active';
            endif;
            $contract->save();
            insert_meta('contract_metas', 'contract_id', $contract->id, 'accetped_date', date('Y-m-d H:i:s'));



            $email_data = array(
                'status' => $status,
                'email' => $contract->customer->email,
                'name' => $contract->customer->name,
                'trainer_name' => $contract->trainer->name,
                'title' => 'Contract offer '.$status.' by '.$contract->trainer->name,
                'address' => 'Hello '.$contract->customer->name.',',
                'link' => array(url('contract/view/'.$contract->id), 'View your offer'),
                'body' => 'Your contract request has been '.$status.' by '.$contract->trainer->name.'.'
                );

            Mail::send('frontend.emails.template', $email_data, function($message) use ($email_data) {
                $message->from('mail@clutchhealth.com' , 'Clutch Health Team' );
                $message->to($email_data['email'], $email_data['name'])->subject('Contract offer '.$email_data['status'].' by '.$email_data['trainer_name']);
            });

            $notify = (object) array();
            $notify->title = 'Success';
            $notify->type = 'success';
            $notify->text = 'Contract successfully '.$status.'.';

            return redirect('contracts')->with('notify', $notify);
        else:
            abort(404);
        endif;
    }

    public function history() {
        if(Auth::user()->role_id == 2):
            $type = 'trainer_id';
        else:
            $type = 'customer_id';
        endif;
        $data['contracts'] = Contract::where($type, Auth::user()->id)
                            ->where('status', 'Completed')
                            ->orWhere('status', 'Cancled')
                            ->get();
        return view('frontend.contract.list', $data);
    }

    public function my_trainer() {
        if(Auth::user()->role_id == 3):
            $query = Contract::where('customer_id', Auth::user()->id)
                            ->where('status', 'Active');
            if($query->exists()):
                $contract = $data['contract'] = $query->first();
                $data['user'] = User::findOrFail($contract->trainer_id);
                return view('frontend.contract.my-trainer', $data);
            endif;
        else:
            $notify = (object) array();
            $notify->title = 'Error';
            $notify->type = 'info';
            $notify->text = 'You have not hired any trainer. You can hire them from here';

            return redirect('dashboard')->with('notify', $notify);
        endif;
    }


    public function compose($user_id = FALSE) {
        $data['nav'] = 'compose';
        $data['user'] = Auth::user();
        if($user_id):
        $data['trainer'] = User::findOrFail($user_id);
        endif;
        $data['payment_modes'] = PaymentMode::all();
        return view('frontend.contract.compose', $data);
    }


    public function view($contract_id) {
        $data['contract'] = Contract::findOrFail($contract_id);
        $data['payment_modes'] = PaymentMode::all();
        return view('frontend.contract.view', $data);
    }

    public function submitContract(Request $request) {
        $messages = [
            'description.required' => 'The details field is required.',
            'start_date.required' => 'Please specify training start date.',
            'end_date.required' => 'Please specify training end date.'
        ];

        $validation =  [
            'title' => 'required|max:255',
            'description' => 'required',
            'start_date' => 'required|max:255',
            'end_date' => 'required|max:255',
            'payment_mode_id' => 'required|max:255',
            'budget' => 'required|max:255',
        ];


        $validator = Validator::make($request->all(),$validation,$messages);

        if($validator->fails()):
            return redirect()->intended('contract/compose')->with('errors', $validator->errors());
        else:
            $offer = new Contract();
            $offer->title = $request->title;
            $offer->description = $request->description;
            $offer->start_date = date('Y-m-d', strtotime($request->start_date));
            $offer->end_date = date('Y-m-d', strtotime($request->end_date));
            $offer->payment_mode_id = $request->payment_mode_id;
            $offer->budget = $request->budget;
            $offer->customer_id = Auth::user()->id;
            $offer->trainer_id = $request->trainer_id;
            $offer->save();
            
            $notify = (object) array();
            $notify->title = 'Success';
            $notify->type = 'success';
            $notify->text = 'Contract successfully requested to trainer.';

            $contract = Contract::findOrFail($offer->id);


            $email_data = array(
                'email' => $contract->trainer->email,
                'name' => $contract->trainer->name,
                'title' => 'You have received contract offer',
                'address' => 'Hello '.$contract->trainer->name.',',
                'link' => array(url('contract/view/'.$contract->id), 'View offer'),
                'body' => 'You have received a contract proposal from '.$contract->customer->name.'.'
                );

            Mail::send('frontend.emails.template', $email_data, function($message) use ($email_data) {
                $message->from('mail@clutchhealth.com' , 'Clutch Health Team' );
                $message->to($email_data['email'], $email_data['name'])->subject('You have received contract offer');
            });

            return redirect()->intended('dashboard')->with('notify', $notify);
        endif;
    }
}
