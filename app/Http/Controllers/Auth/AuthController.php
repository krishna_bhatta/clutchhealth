<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\AllowedLogin;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Auth;
use Mail;
use Intervention\Image\ImageManager;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/user/profile';

    protected $request;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
            'terms_and_condition.required' => 'You must accept terms and conditions for registration.',
            'role.between' => 'You must select your account type.',
        ];

        return Validator::make($data, [
            'terms_and_condition' => 'required',
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:5',
            'role' => 'required|between:1,4',
        ],$messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = new User;
        $user->name = $data['name'];
        if($data['role'] == 2):
            $uesrname_array = $this->generateUserName($data['name']);
            $user->display = $uesrname_array['display'];
            $user->username = $uesrname_array['username'];
        endif;
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);
        $user->confirmation_token = md5(bcrypt($data['password']));
        $user->role_id = $data['role'];
        $user->longitude = $data['longitude'];
        $user->latitude = $data['latitude'];
        $user->address = $data['map-address'];
        $user->budget = $data['budget'];
        $user->payment_mode_id = $data['payment_mode_id'];
        $user->save();

        insert_meta('user_metas', 'user_id', $user->id, 'description', $data['about']);

        if ($this->request->hasFile('profile_photo')):
            $destinationPath = 'uploads/profile'; // upload path
            $extension = $this->request->file('profile_photo')->getClientOriginalExtension(); // getting image extension
            $fileName = md5(time().rand(11111,99999)).'.'.$extension; // renameing image
            // $request->file('profile_photo')->move($destinationPath, $fileName);

            $image_manager = new ImageManager;

            $img = $image_manager->make($_FILES['profile_photo']['tmp_name']);

            // resize image
            $img->fit(200, 200);

            // save image
            $img->save($destinationPath.'/'.$fileName);


            insert_meta('user_metas', 'user_id', $user->id, 'profile_photo', $fileName);
        endif;

        $allowedLogins = new AllowedLogin;
        $allowedLogins->user_id = $user->id;
        $allowedLogins->login_type = "Manual";
        $allowedLogins->save();

        $data['link'] = 'confirm-registration?token='.$user->confirmation_token.'&cId='.$user->id;

        Mail::send('frontend.emails.registration-email', $data, function($message) use ($data) {
            $message->from('mail@clutchhealth.com' , 'Clutch Health Team' );
            $message->to($data['email'], $data['name'])->subject('Activate Your Clutch Health Account');
        });

        return $user;
    }

    private function generateUserName($full_name) {

        $display = str_replace(' ', '', $full_name);
        $username = strtolower($display);

        if(!User::where('username',$username)->exists()):
            return ['display' => $display, 'username' => $username];
        endif;

        $display = $display.rand(0,1000);
        $username = strtolower($display);

        while (!User::where('username',$username)->exists()) {            
            return ['display' => $display, 'username' => $username];

        }
    }
 
    public function login(Request $request) {
        $rules = array('email' => 'unique:users,email');

        $validator = Validator::make($request->all(), $rules);

        if (!$validator->fails()) {
            return redirect('login')->with('error', 'This email is not registered on our system.');
        }

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'status' => 'Active'])) {
            if(Auth::user()->role_id == 1) {
                return redirect()->intended('admin/home');
            }
            return redirect()->intended('dashboard');
        }

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            Auth::logout();
            return redirect('login')->with('error', 'This account is not activated.');
        }

        return redirect('login')->with('error', 'Invalid email and/or password');
    }
}
