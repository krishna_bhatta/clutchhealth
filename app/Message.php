<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

   /**
    * Get the meta value for the message.
    */
   public function meta()
   {
       return $this->hasMany('App\MessageMeta');
   }
}
