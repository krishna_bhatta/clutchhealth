<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{   

   /**
    * Get the meta value for the article.
    */
   public function meta()
   {
       return $this->hasMany('App\ArticleMeta');
   }
}
