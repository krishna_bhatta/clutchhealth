<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\Stripe::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    //Socialite
    'facebook' => [
        'client_id'     => '1042174819184566',
        'client_secret' => '3d3fa9e367cd9c3d5b4fbe0f8fcdebe7',
        'redirect'      => 'https://clutchhealth.com/fb-callback',
    ],

    'twitter' => [
        'client_id' => 'rieuZzP36seX2HyVTSswNAhPY',
        'client_secret' => 'j8j4KftwBP3BH9wA6OO3l2fbqU6gF8Dnxgw8kXabeECVpr31v4',
        'redirect' => 'https://clutchhealth.com/twitter-callback',
     ],

     'google' => [
         'client_id' => '650360771569-g8b8kv9l4p75thc5jckd1udj9ds3jido.apps.googleusercontent.com',
         'client_secret' => 'DHmKfZ5FW9Uq7269pgkhaaFJ',
         'redirect' => 'https://clutchhealth.com/google-callback',
     ],

     'linkedin' => [
         'client_id' => '78s9hkpy3mg9bd',
         'client_secret' => '37nod1IeVB54Ensu',
         'redirect' => 'https://clutchhealth.com/linkedin-callback',
     ],

];
